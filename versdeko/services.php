<?php

// let's create the function for the custom type
function services_cpt() {
    // creating (registering) the custom type
    register_post_type('services', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
            // let's now add all the options for this post type
            [
        'labels' => [
            'name' => trans('services_tab'),
            /* This is the Title of the Group */
            'singular_name' => __('service', 'ifconcept'),
            /* This is the individual type */
            'all_items' => __('All services', 'ifconcept'),
            /* the all items menu item */
            'add_new' => __('Add New', 'ifconcept'),
            /* The add new menu item */
            'add_new_item' => __('Add New project', 'ifconcept'),
            /* Add New Display Title */
            'edit' => __('Edit', 'ifconcept'),
            /* Edit Dialog */
            'edit_item' => __('Edit Post Types', 'ifconcept'),
            /* Edit Display Title */
            'new_item' => __('New Post Type', 'ifconcept'),
            /* New Display Title */
            'view_item' => __('View Post Type', 'ifconcept'),
            /* View Display Title */
            'search_items' => __('Search Post Type', 'ifconcept'),
            /* Search Custom Type Title */
            'not_found' => __('Nothing found in the Database.', 'ifconcept'),
            /* This displays if there are no entries yet */
            'not_found_in_trash' => __('Nothing found in Trash', 'ifconcept'),
            /* This displays if there is nothing in the trash */
            'parent_item_colon' => ''
        ],
        /* end of arrays */
        'description' => __('services', 'ifconcept'),
        /* Custom Type Description */
        'public' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'show_ui' => true,
        'query_var' => true,
        'menu_position' => 7,
        /* this is what order you want it to appear in on the left hand side menu */
        'menu_icon' => 'dashicons-analytics',
        /* the icon for the Game type menu */
        'rewrite' => ['slug' => 'services', 'with_front' => true],
        /* you can specify its url slug */
        'has_archive' => 'services',
        /* you can rename the slug here */
        'capability_type' => 'post',
        'hierarchical' => true,
        /* the next one is important, it tells what's enabled in the post editor */
        'supports' => ['title', 'editor', 'thumbnail', 'post_tag'],
        'taxonomies' => ['service_categories', 'post_tag'],
            ] /* end of options */
    ); /* end of register post type */
}

// adding the function to the Wordpress init
add_action('init', 'services_cpt');

//function service_categories_init() {
//    // create a new taxonomy
//    register_taxonomy(
//            'service_categories', 'services', array(
//        'label' => __('Categories'),
//        'rewrite' => array('slug' => 'service_categories'),
//        'public' => true,
//        'show_admin_column' => true,
//        'show_ui' => true,
//        'hierarchical' => true,
//            )
//    );
//}
//
//add_action('init', 'service_categories_init');
?>
