</main>

<footer class="footer">
    <div class="container">
        <div class="row">
            
            <div class="col-md-1"></div>
            <div class="col-md-11 line"></div>
            <div class="col-md-1"></div>
            <?php $footer = get_field('footer', 'option'); ?>
            <?php if ($footer['first']): ?>
                <div class="col-md-5">
                    <div class="h-foot">
                        <?php echo $footer['first']; ?>
                    </div>
                </div>
            <?php endif; ?>
            <?php if ($footer['second']): ?>
                <div class="col-md-6">
                    <div class="h-foot">
                        <?php echo $footer['second']; ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</footer>
<!-- Wordpress footer -->
<?php wp_footer(); ?>
<!-- main JS -->
<script type="text/javascript">
    var templateDir = '<?php echo get_theme_url(); ?>';
</script>
<script src="<?php echo get_theme_url('/assets/js/script.js').'?v='.time(); ?>"></script>
<?php if (is_home() && !empty(get_map_coord('lat'))) { ?>
    <script>
        var map_coords = {
            lat: <?php echo get_map_coord('lat'); ?>,
            lng: <?php echo get_map_coord('lng'); ?>
        };
    </script>
    <script src="<?php echo get_theme_url('/assets/js/contact.js'); ?>"></script>
<?php } ?>
</body>
</html>