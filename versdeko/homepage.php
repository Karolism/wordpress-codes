<?php
/* Template Name: Homepage */
get_header();
?>
<?php
$blocks = get_field('blokai');
?>
<div id="fullpage">
    <?php
    if ($blocks):
        $numItems = count($blocks);
        $i = 0;
        foreach ($blocks as $block):
            ?>
            <div class="section" style="background-image: url(<?php echo $block['bg-img']; ?>)">
                <div class="section-content">
                    <div class="block-content">
                        <?php echo $block['content']; ?>
                    </div>
                    <?php if ($block['link']): ?>
                        <div class="home-btn">
                            <a class="btn" href="<?php echo $block['link'] ?>"><?php echo trans('more'); ?></a>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="bg-filter"></div>
                <?php
                if (++$i === $numItems):
                    $footer = get_field('footer', 'option');
                    ?>
                    <div class="home-footer">
                        <div class="container">
                            <div class="row">
                                <?php if ($footer['first']): ?>
                                    <div class="col-md-6">
                                        <div class="h-foot">
                                            <?php echo $footer['first']; ?>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <?php if ($footer['second']): ?>
                                    <div class="col-md-6">
                                        <div class="h-foot">
                                            <?php echo $footer['second']; ?>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
            <?php
        endforeach;
    endif;
    ?>
</div>
<?php
get_footer();
