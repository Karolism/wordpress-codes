<?php
/* Template Name: Contacts Page  */
get_header();
?>
<?php
$location = get_field('map');
$phone = get_field('phone');
$email = get_field('email');
?>

<section>
    <div id="google-map" class="google-map-style2" data-ln="<?php echo $location['lng'] ?>" data-lt="<?php echo $location['lat'] ?>" data-img="<?php echo get_theme_url('/assets/images/marker.png') ?>">
    </div>  
</section>
<div class="container">
    <div class="row">
        <div class="col-md-5 col-md-offset-1 px-0">
            <div class="contacts">
                <div class="label">
                    <?php echo trans("rekvizitai") ?>
                </div>
                <div class="blocks">
                    <?php
                    $blocks = get_field('rekvizitai');
                    ?>
                    <div class="row">
                        <div class="block col-md-7 ">
                            <?php echo $blocks['pirmas_blokas'] ?>
                        </div>
                        <div class="block col-md-5 pl-0">
                            <?php echo $blocks['antras_blokas'] ?>
                        </div>
                    </div>
                </div>
                <div class="label">
                    <?php echo trans('contacts_form'); ?>
                    <span><?php echo trans('form_sublabel') ?>:</span>
                </div>
                 <?php get_part('contact-form'); ?>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>

