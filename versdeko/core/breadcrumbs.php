<?php
// Custom post type translation
function trans_cpt($post_type) {
	$post_type_name = trans('cpt_' . $post_type);

	if (strpos($post_type_name, 'No') == true) {
		$cpt = get_post_type_object($post_type);
		$post_type_name = $cpt -> labels -> name;
	}

	return $post_type_name;
}
// Breadcrumbs
function breadcrumbs() {
	// Settings
	$separator = '&raquo;';
	$bread_classname = 'breadcrumbs';
	$home_title = trans('breadcrumbs_home');
	$cat_term = true;
	// Get the query & post information
	global $post, $wp_query;
	// Do not display on the homepage
	$html = '';
	if (!is_front_page()) {
			// Build the breadcrums
			$html.= '<ul class="' . $bread_classname . '">';
			// Home page
			$html.= '<li class="item-home"><a class="bread-link bread-home" href="' . get_home_url() . '" title="' . $home_title . '">' . $home_title . '</a></li>';
			$html.= '<li class="separator separator-home"> ' . $separator . ' </li>';

			if (is_archive() && !is_tax() && !is_category()) {
				if (get_queried_object()) {
					$post_type = get_queried_object() -> name;
				} else {
					$post_type = get_post_type();
				}
				$html.= '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . trans_cpt($post_type) . '</strong></li>';
			} else if (is_archive() && is_tax() && !is_category()) {
				// If post is a custom post type
				if (get_queried_object()) {
					$post_type = get_taxonomy( get_queried_object() -> taxonomy ) -> object_type[0];
				} else {
					$post_type = get_post_type();
				}
				// If it is a custom post type display name and link
				if ($post_type && $post_type != 'post') {
					$html.= '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . get_post_type_archive_link($post_type) . '" title="' . trans_cpt($post_type) . '">' . trans_cpt($post_type) . '</a></li>';
					$html.= '<li class="separator"> ' . $separator . ' </li>';
				}
				
				$html.= '<li class="item-current item-archive"><strong class="bread-current bread-archive">' . get_queried_object() -> name . '</strong></li>';
			} else if (is_single()) {
				// If post is a custom post type
				$post_type = get_post_type();
				// If it is a custom post type display name and link
				if ($post_type != 'post') {
						
						$post_type_archive = get_post_type_archive_link($post_type);
						
						$html.= '<li class="item-cat item-custom-post-type-' . $post_type . '"><a class="bread-cat bread-custom-post-type-' . $post_type . '" href="' . $post_type_archive . '" title="' . trans_cpt($post_type) . '">' . trans_cpt($post_type) . '</a></li>';
						$html.= '<li class="separator"> ' . $separator . ' </li>';
				}
				// Get post category info
				$category = get_the_category();
				// If it's a custom post type within a custom taxonomy
				if (empty($category)) {
					$tax_obj = get_queried_object();
					$taxonomy_names = get_object_taxonomies($post);
					if (!empty($taxonomy_names) && $taxonomy_names[0]) {
						$category = get_the_terms($post -> ID, $taxonomy_names[0]);
					} else {
						$cat_term = false;
					}
				}
				// if everything great - show cat
				if($cat_term) {
					// Get last category post is in
					$last_category = end($category);
					// Get parent any categories and create array
					$get_cat_parents = rtrim(get_category_parents($last_category -> term_id, true, ',') , ',');
					$cat_parents = explode(',', $get_cat_parents);
					// Loop through parent categories and store in variable $cat_display
					$cat_display = '';
					foreach ($cat_parents as $parents) {
						$cat_display .= '<li class="item-cat">' . $parents . '</li>';
						$cat_display .= '<li class="separator"> ' . $separator . ' </li>';
					}
					// Check if the post is in a category
					if (!empty($last_category)) {
						$html .= $cat_display;
					}
				}

				$html.= '<li class="item-current item-' . $post -> ID . '"><strong class="bread-current bread-' . $post -> ID . '" title="' . get_the_title() . '">' . get_the_title() . '</strong></li>';
			} else if (is_category()) {
					
				// Category page
				$html.= '<li class="item-current item-cat"><strong class="bread-current bread-cat">' . single_cat_title('', false) . '</strong></li>';
			} else if (is_page()) {
				// Standard page
				if ($post -> post_parent) {
					// If child page, get parents
					$anc = get_post_ancestors($post -> ID);
					// Get parents in the right order
					$anc = array_reverse($anc);
					//little check if setED
					if (!isset($parents)) {
							$parents = '';
					}
					// Parent page loop
					foreach ($anc as $ancestor) {
						$parents.= '<li class="item-parent item-parent-' . $ancestor . '"><a class="bread-parent bread-parent-' . $ancestor . '" href="' . get_permalink($ancestor) . '" title="' . get_the_title($ancestor) . '">' . get_the_title($ancestor) . '</a></li>';
						$parents.= '<li class="separator separator-' . $ancestor . '"> ' . $separator . ' </li>';
					}
					// Display parent pages
					$html.= $parents;
					// Current page
					$html.= '<li class="item-current item-' . $post -> ID . '"><strong title="' . get_the_title() . '"> ' . get_the_title() . '</strong></li>';
				} else {
					// Just display current page if not parents
					$html.= '<li class="item-current item-' . $post -> ID . '"><strong class="bread-current bread-' . $post -> ID . '"> ' . get_the_title() . '</strong></li>';
				}
			} else if (is_tag()) {
				// Tag page
				// Get tag information
				$term_id = get_query_var('tag_id');
				$taxonomy = 'post_tag';
				$args = 'include=' . $term_id;
				$terms = get_terms($taxonomy, $args);
				
				// Display the tag name
				$html.= '<li class="item-current item-tag-' . $terms -> term_id . ' item-tag-' . $terms[0] -> slug . '"><strong class="bread-current bread-tag-' . $terms -> term_id . ' bread-tag-' . $terms[0] -> slug . '">' . $terms[0] -> name . '</strong></li>';
			} elseif (is_day()) {
					
				// Day archive
				// Year link
				$html.= '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link(get_the_time('Y')) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
				$html.= '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
				// Month link
				$html.= '<li class="item-month item-month-' . get_the_time('m') . '"><a class="bread-month bread-month-' . get_the_time('m') . '" href="' . get_month_link(get_the_time('Y') , get_the_time('m')) . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</a></li>';
				$html.= '<li class="separator separator-' . get_the_time('m') . '"> ' . $separator . ' </li>';
				// Day display
				$html.= '<li class="item-current item-' . get_the_time('j') . '"><strong class="bread-current bread-' . get_the_time('j') . '"> ' . get_the_time('jS') . ' ' . get_the_time('M') . ' Archives</strong></li>';
			} else if (is_month()) {
				// Month Archive
				// Year link
				$html.= '<li class="item-year item-year-' . get_the_time('Y') . '"><a class="bread-year bread-year-' . get_the_time('Y') . '" href="' . get_year_link(get_the_time('Y')) . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</a></li>';
				$html.= '<li class="separator separator-' . get_the_time('Y') . '"> ' . $separator . ' </li>';
				// Month display
				$html.= '<li class="item-month item-month-' . get_the_time('m') . '"><strong class="bread-month bread-month-' . get_the_time('m') . '" title="' . get_the_time('M') . '">' . get_the_time('M') . ' Archives</strong></li>';
			} else if (is_year()) {
				// Display year archive
				$html.= '<li class="item-current item-current-' . get_the_time('Y') . '"><strong class="bread-current bread-current-' . get_the_time('Y') . '" title="' . get_the_time('Y') . '">' . get_the_time('Y') . ' Archives</strong></li>';
			} else if (is_author()) {
				// Author archive
				// Get the author information
				global $author;
				$userdata = get_userdata($author);
				// Display author name
				$html.= '<li class="item-current item-current-' . $userdata -> user_nicename . '"><strong class="bread-current bread-current-' . $userdata -> user_nicename . '" title="' . $userdata -> display_name . '">' . 'Author: ' . $userdata -> display_name . '</strong></li>';
			} else if (get_query_var('paged')) {
				// Paginated archives
				$html.= '<li class="item-current item-current-' . get_query_var('paged') . '"><strong class="bread-current bread-current-' . get_query_var('paged') . '" title="Page ' . get_query_var('paged') . '">' . __('Page') . ' ' . get_query_var('paged') . '</strong></li>';
			} else if (is_search()) {
				// Search results page
				$html.= '<li class="item-current item-current-' . get_search_query() . '"><strong class="bread-current bread-current-' . get_search_query() . '" title="Search results for: ' . get_search_query() . '">Search results for: ' . get_search_query() . '</strong></li>';
			} elseif (is_404()) {
				// 404 page
				$html.= '<li>' . 'Error 404' . '</li>';
			}
			
			$html.= '</ul>';
	}

	//If breadcrumbs turned ON
	if (BREADCRUMBS) {
		return $html;
	}
}