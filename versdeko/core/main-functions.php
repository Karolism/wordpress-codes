<?php
/**
 * @Author: Dewdrop | Aivaras Čenkus
 * @Date:   2016-03-15 16:07:54
 * @Last Modified by:   Dewdrop | Aivaras Čenkus
 * @Last Modified time: 2016-10-19 15:42:02
 */

function ms_to_days ($iDay) {
	return ($iDay * 24 * 60 * 60);
}
//Set cookie for user id 1 at login for WP DEBUG ON
function wp_debug_admin_login( $user_login, $user ) {
	if ($user -> ID === 1) {
		setcookie( 'wp_debug', 1, time() + ms_to_days(365));
	} elseif (isset($_COOKIE['wp_debug'])) {
		setcookie('wp_debug', '', time() - ms_to_days(365));
	}
}
add_action( 'wp_login', 'wp_debug_admin_login', 10, 2 );
//remove cookie for wp debug
function wp_debug_admin_logout() {
	if (isset($_COOKIE['wp_debug'])) {
		setcookie('wp_debug', '', time() - ms_to_days(365));
	}
}
add_action( 'wp_logout', 'wp_debug_admin_logout');
//metaabox translate
add_action('admin_footer', 'metabox_trans');
function metabox_trans () {
	
	$script = '<script type="text/javascript">
		if (jQuery(".acf_postbox h2 span").length) {
			jQuery(".acf_postbox h2 span").addClass("i18n-multilingual-display");
		}
	</script>';

	echo $script;
}
//triming text
function cut_text ($str, $limit = false, $trim_symbol = false) {
	if($limit && strlen($str) > $limit) {
		$str = mb_substr($str, 0, $limit);

		if (!$trim_symbol) {
			$str_arr = explode(' ', $str);

			array_pop($str_arr);
			$str = implode(' ', $str_arr) . '...';
		}
		
	}

	return $str;
}
//Title cut or by id
function title($id = false, $limit = false, $trim_symbol = false) {
	if ($id) {
		$str = __(get_post($id) -> post_title);
	} else {
		$str = get_the_title();
	}

	$str = cut_text($str, $limit, $trim_symbol);

	$str = apply_filters( 'the_title', $str );

	return $str;
}
// Excerpt and content triming or geting by id
function excerpt($id = false, $limit = false, $trim_symbol = false) {
	if ($id) {
		$post = get_post($id);

		if ($post -> post_excerpt) {
			$str = $post -> post_excerpt;
		} else {
			$str = $post -> post_content;
		}

		$str = __($str);

	} else {
		$str = get_the_excerpt();
	}

	$str = cut_text($str, $limit, $trim_symbol);

	$str = preg_replace('/\[.+\]/', '', $str);
	$str = apply_filters( 'the_excerpt', $str );
	$str = str_replace(']]>', ']]&gt;', $str);

	return $str;
}

function content($id = false, $limit = false, $trim_symbol = false) {
	if ($id) {
		$post = get_post($id);
		$str = __($post -> post_content);
	} else {
		$str = get_the_content();
	}

	$str = cut_text($str, $limit, $trim_symbol);

	$str = preg_replace('/\[.+\]/', '', $str);
	$str = apply_filters('the_content', $str);
	$str = str_replace(']]>', ']]&gt;', $str);

	return $str;
}

function current_lang() {
	/* ***
	* check if qTranslate plugin is installed/actived
	* if not use default wp language 
	*** */
	if(function_exists('qtrans_getLanguage')){
		$current = qtrans_getLanguage();
	} else {
		$current = get_locale();
		$current = substr($current, 0, 2);
	}

	return $current;
}
// using curl, because some servers dont configuredt for file_get_contents()  (or reverse...)
function url_get_contents($url) {
	if (function_exists('file_get_contents')){
		return file_get_contents($url);
	} 
	if (function_exists('curl_init')){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$output = curl_exec($ch);
		curl_close($ch);

		return $output;
	} else {
		die('URL can not be open!');
	}
}

/* ***
* This function written for that all translations would be in one place
* and whould not need to search difference files to add new or modify old translation
* -=TasKurPavaro=- 
*** */
$translations_json = get_theme_url('/langs/' . current_lang() . '.json');
$translations = json_decode(url_get_contents($translations_json), true);

function trans($key = null) {
	global $translations;

	if(!$key){
		return '{No key given}';
	}

	if(!isset($translations[$key]) OR empty($translations[$key])){
		return '{No lang[' . current_lang() . '] found for key "'.$key.'"}';        
	}

	return $translations[$key];
}

// functions to get client socials
function get_socials() {
	return unserialize(get_option('client_socials'));
}

function get_social($field) {
	$social_array = unserialize(get_option('client_socials'));
	if (empty($social_array[$field])) {
		return '';
	}
	return $social_array[$field];
}

function get_theme_url ($url = false) {
	return get_template_directory_uri() . $url;
}

function get_theme_image ($image, $title = false, $alt = false) {
	$image_url = get_theme_url('/assets/images/' . $image);

	if (strpos($image, '/') !== false) {
		$image = explode('/', $image);
		$image = $image[1];
	}

	$image_class = explode('.', $image);
	$image_class = $image_class[0];
	$image = '<img class="theme-image ' . $image_class . '" src="' . $image_url . '" alt="'. ( $alt ? $alt : $image_class ) .'"'. ( $title ? ' title="' . $title . '"' : '' ) .' />';

	return $image;
}

function get_part($slug, $name = null) {
	if ($name !== '') {
		get_template_part('parts/' . $slug, $name);
	} else {
		get_template_part('parts/' . $slug);
	}
}

function get_thumb_url ($id = false, $size = 'full') {
	$thumb_id = get_post_thumbnail_id($id);
	//if thumb id exsist
	if ($thumb_id == null) {
		$thumb_id = $id;
	}
	$image_url = wp_get_attachment_image_src( $thumb_id, $size );

	return $image_url[0];
}

function get_thumb_info ($iID = false) {
	return get_post(get_post_thumbnail_id($iID));
}

function get_thumb_alt ($iID = false) {
	return get_post_meta(get_post_thumbnail_id($iID), '_wp_attachment_image_alt', true);
}

function parse_map_info ($address, $key = false) {
	$google_json = 'http://maps.google.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false';
	$json = json_decode(file_get_contents($google_json));
	$result = $json -> results[0];

	if ($key == 'address') {
		return $result -> formatted_address;
	}

	$coord = $result -> geometry -> location;

	if ($key) {
		return $coord -> $key;
	}

	return $coord;
}

function get_map_coord ($what) {
	$coord = explode(', ', get_option('client_map_coord'));

	if ($what == 'lat') {
		return $coord[0];
	} else if ($what == 'lng') {
		return $coord[1];
	}

	return null;
}

// Home title
add_filter('wp_title', 'title_for_site');

function title_for_site ($title) {
	$title_part = get_bloginfo('name');
	if ((is_home() || is_front_page()) && !empty($title)) {
		return $title_part . ' | ' . get_bloginfo('description');
	} else {
		if (get_queried_object() && property_exists(get_queried_object(), 'name')) {
			if (property_exists(get_queried_object(), 'term_id')) {
				return $title_part . ' | ' . get_queried_object() -> name;
			} else {
				return $title_part . ' | ' . trans('cpt_' . get_queried_object() -> name);
			}
		} elseif (get_the_title()) {
			return $title_part . ' | ' . get_the_title();
		}
	}

	return $title_part;
}
//Pagination
function pagination ($limit) {
		if ($limit <= 1) {
			return;
		}
		// need an unlikely integer
		$big = 999999999;
		$pagination_obj = paginate_links(array(
			'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))) ,
			'format' => '?paged=%#%',
			'prev_text' => __('<i class="fa fa-angle-left"></i>') ,
			'next_text' => __('<i class="fa fa-angle-right"></i>') ,
			'current' => max(1, get_query_var('paged')) ,
			'total' => $limit
		));
		
		return '<div class="pagination">' . $pagination_obj . '</div>';
}
// Lang chooser widget
function get_lang_menu () {
	if(function_exists('qtranxf_getSortedLanguages')){
		$langs = qtranxf_getSortedLanguages();
		$current = qtrans_getLanguage();
		$url = explode('/' . $current . '/', $_SERVER['REQUEST_URI']);

		if (!isset($url[1])) {
			$url[1] = ltrim($url[0], '/');
		}

		$text = '<ul class="lang-menu">';
		foreach ($langs as $lang) {
			$text .= '<li>';
			if ($current == $lang) {
				$text .= '<span class="lang-current">' . strtoupper($lang) . '</span>';
			} else {
				$text .= '<a href="' . site_url('/' . $lang . '/' . $url[1]) . '">' . strtoupper($lang) . '</a>';
			}
			$text .= '</li>';
		}
		$text .= '</ul>';

		return $text;
	}
}
//Get menu
function get_menu ($slug) {
	$html = '<nav class="' . $slug . '">';
	$html .= wp_nav_menu(array(
		'container'         => false,
		'menu'              => $slug,
		'theme_location'    => $slug,
		'menu_class'        => $slug . '-list list-unstyled clearfix',
		'echo'              => false
	));
	$html .= '</nav>';

	return $html;
}
// hs(1254,584741) => 1254.58
function nb($str, $tailLength = 2){
 return number_format((float)(str_replace(',', '.', $str)), $tailLength, '.', '');
}

function dates_diff($date1, $date2){
 if(!$date1 OR !$date2){
	return false;
 }
 $date_1 = new DateTime($date1);
 $date_2 = new DateTime($date2);
 
 $diff = $date_2 -> diff($date_1) -> format("%a");
 
 return $diff;
}

function format_bytes($bytes, $precision = 2){ 
 $units = array('B', 'KB', 'MB', 'GB', 'TB'); 
	 
 $bytes = max($bytes, 0); 
 $pow = floor(($bytes ? log($bytes) : 0) / log(1024)); 
 $pow = min($pow, count($units) - 1); 
	 
 $bytes /= pow(1024, $pow); 
	 
 return round($bytes, $precision) . ' ' . $units[$pow]; 
}

function array_sort($array, $on, $order = SORT_ASC){
	$new_array = array();
	$sortable_array = array();

	if(count($array) > 0){
		foreach ($array as $k => $v){
			if (is_array($v)){
				foreach ($v as $k2 => $v2){
					if($k2 == $on){
						$sortable_array[$k] = $v2;
					}
				}
			} else {
				$sortable_array[$k] = $v;
			}
		}

		switch($order){
			case SORT_ASC:
				asort($sortable_array);
				break;
			case SORT_DESC:
				arsort($sortable_array);
				break;
		}

		foreach($sortable_array as $k => $v){
			$new_array[$k] = $array[$k];
		}
	}

	return $new_array;
}

function to_permalink($sName) {
	if($sName !== mb_convert_encoding( mb_convert_encoding($sName, 'UTF-32', 'UTF-8'), 'UTF-8', 'UTF-32') ) {
		$sName = mb_convert_encoding($sName, 'UTF-8', mb_detect_encoding($sName));
	}

	$sName = htmlentities($sName, ENT_NOQUOTES, 'UTF-8');
	$sName = preg_replace('`&([a-z]{1,2})(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig);`i', '\\1', $sName);
	$sName = html_entity_decode($sName, ENT_NOQUOTES, 'UTF-8');
	$sName = preg_replace(array('`[^a-z0-9]`i','`[-]+`'), '-', $sName);
	$sName = sNametolower( trim($sName, '-') );

	return $sName;
}

function get_current_url ($url_after = false) {
	$url  = @( $_SERVER["HTTPS"] != 'on' ) ? 'http://'.$_SERVER["SERVER_NAME"] :  'https://'.$_SERVER["SERVER_NAME"];
	$url .= ( $_SERVER["SERVER_PORT"] !== 80 ) ? ":".$_SERVER["SERVER_PORT"] : "";
	$url .= $_SERVER["REQUEST_URI"];
	return $url.$url_after;
}
// youtube functions
function get_youtube_id($url) {
	parse_str( parse_url( $url, PHP_URL_QUERY ), $vars );
	$id = $vars['v'];    

	return $id;
}

function get_youtube_img_url($url) {
	$resolutions = array(
		'maxresdefault', 
		'hqdefault', 
		'mqdefault'
	);

	foreach($resolutions as $res) {
		$img_url = 'http://img.youtube.com/vi/' . get_youtube_id($url) . '/' . $res . '.jpg';
		if(@getimagesize(($img_url))){
			return $img_url;
		}
	}

	return 'http://img.youtube.com/vi/' . get_youtube_id($url) . '/default.jpg';
}

function get_youtube_img($url) { 
	return '<img class="youtube-thumb" src="' . get_youtube_img_url($url) . '" alt="Thumbnail" />';
}

function get_file_icon($url) {
	$extension = substr($url, strrpos($url, ".") + 1, strlen($url));
	$extensions['excel'] = ['xls', 'xlt', 'xlm', 'xlsx', 'xlsm', 'xltx', 'xltm', 'xlsb', 'xla', 'xlam', 'xll', 'xlw', 'ods'];
	$extensions['word'] = ['doc', 'dot', 'wbk', 'docx', 'docm', 'dotx', 'dotm', 'docb', 'odt'];
	$extensions['pdf'] = ['pdf'];
	$found_icon = false;
	$default_icon = get_theme_url('/assets/images/icons/excel_icon.png');

	$str = "<img width='30' height='31' src='";
	foreach ($extensions as $key => $value) {
		if (in_array($extension, $value)) {
			$str .= get_theme_url('/assets/images/icons/'.$key.'_icon.png');
			$found_icon = true;
			break;
		}
	}
	if ($found_icon == false) {
		$str = get_theme_url('/assets/images/icons/excel_icon.png');
	}
	$str .= "'>";
	return $str;
}

function dd($smthg) {
	echo "<pre style='color: black'>";
	var_dump($what);
	echo "</pre>";
	die();
}

function dump($smthg) {
	echo "<pre style='color: black'>";
	var_dump($smthg);
	echo "</pre>";
}

// sometimes needed for pagination
// function my_post_count_queries( $query ) {
//   if (!is_admin() && $query->is_main_query()){
//     if(is_home()){
//        $query->set('posts_per_page', 1);
//     }
//   }
// }
// add_action( 'pre_get_posts', 'my_post_count_queries' );