<?php
/**
 * @Author: Dewdrop | Aivaras Čenkus, TasKurPavaro
 * @Date:   2016-03-15 16:07:54
 * @Last Modified by:   Dewdrop | Aivaras Čenkus
 * @Last Modified time: 2016-07-05 09:42:26
 */
function json_array_filter(&$value) {
  $value = htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
  $value = stripslashes($value);
}

function create_json ($array, $filename) {
  array_walk_recursive($array, "json_array_filter");
  $fp = fopen(LANG_DIR . $filename . '.json', 'w');
  fwrite($fp, json_encode($array, JSON_UNESCAPED_UNICODE));
  fclose($fp);
}

add_action('admin_head', 'style_trans');
function style_trans () {
  
  $style = '<style>
    .client-translations .delete-btn {
      cursor: pointer;
      border: none;
      background: none;
      color: red;
    }
    .client-translations .delete-btn:hover {
      color: blue;
    }
    .client-translations .th-columns {
      background: #F1F1F1;
      border-radius: 4px;
      overflow: hidden;
    }
    .client-translations .th-columns th {
      width: auto;
      min-width: 260px;
      padding: 9px 10px;
      text-align: center;
      text-transform: uppercase;
    }
    .client-translations .th-columns th:last-child {
      min-width: 50px;
    }
    .client-translations .th-columns th:first-child {
      text-align: left;
    }
    .client-translations .col input.regular-text {
      min-width: 260px;
      width: auto;
    }
    .client-translations th.col {
      min-width: 260px;
      width: auto;
    }
    .client-translations thead, .client-translations tbody {
      display: block;
    }
    .client-translations tbody {
      max-height: 600px;
      overflow-y: auto;
      overflow-x: hidden;
    }
  </style>';

  echo $style;
}

add_action( 'admin_menu', 'client_translations_menu' );
function client_translations_menu() {
  add_menu_page( trans('client_translations_title'), trans('client_translations_title'), 'manage_options', 'client_translations', 'client_translations_page', 'dashicons-translation' );
}

function client_translations_page() {
  global $aAdminCustomFields, $translations;

  if ( !current_user_can( 'manage_options' ) )  {
    wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
  }

  $default_lang = get_option('qtranslate_default_language');
  $sUrlCurrent = get_theme_url('/langs/' . $default_lang . '.json');
  $aCurrentLangs = json_decode(url_get_contents($sUrlCurrent));
  $hidden_field_name = 'client_translations';
  define('LANG_DIR', get_template_directory().'/langs/');

  if(function_exists('qtranxf_getSortedLanguages')){
    $aJsons = array();
    $aLangs = qtranxf_getSortedLanguages();
    $sText = '<thead>';
    $sText .= '<tr class="th-columns">';
    $sText .= '<th>';
    $sText .= 'KEY';
    $sText .= '</th>';
    foreach ($aLangs as $aLang) {
      //if lang json not exsist
      if(!file_exists(LANG_DIR . $aLang . '.json') && !empty($aLang)){
        $empty_array = array();
        create_json($empty_array, $aLang);
      }

      $this_url = get_theme_url('/langs/' . $aLang . '.json');
      $aNewLangs = json_decode(url_get_contents($this_url), true);
      
      $aJsons[$aLang] = $aNewLangs;
      $sText .= '<th>';
      $sText .= $aLang;
      $sText .= '</th>';
    }
    $sText .= '<th>';
    $sText .= '</th>';
    $sText .= '</tr>';
    $sText .= '</thead>';
    $sText .= '<tbody>';
    foreach ($aJsons[$default_lang] as $key => $value) {
      $sText .= '<tr class="translation-line">';
      $sText .= '<th class="col">';
      $sText .= '<label>';
      $sText .= '<span>'.$key.'</span>';
      $sText .= '</label>';
      $sText .= '</th>';
      $sText .= '<td class="col">';
      $sText .= '<input type="text" data-lang="'.$default_lang.'" name="'.$default_lang.'['.$key.']" id="'.$default_lang.'_'.$key.'" value="'.$value.'" class="regular-text">';
      $sText .= '</td>';
      unset($aJsons[$default_lang]);
      foreach ($aJsons as $lang => $val) {
        $sText .= '<td class="col">';
        $sText .= '<input type="text" data-lang="'.$lang.'" name="'.$lang.'['.$key.']" id="'.$lang.'_'.$key.'" value="'. ( isset($val[$key]) ? $val[$key] : '' ) .'" class="regular-text">';
        $sText .= '</td>';
      }
      $sText .= '<td class="col">';
      $sText .= '<button onclick="return confirm(\''. trans('delete_msg') .'\')" class="delete-btn" type="submit" name="delete" value="'.$key.'"><span class="dashicons dashicons-no"></span></button>';
      $sText .= '</td>';
      $sText .= '</tr>';
    }
    $sText .= '</tbody>';
  }
  ?>
    <?php if( isset($_POST[ $hidden_field_name ]) && $_POST[ $hidden_field_name ] == 'Y' ) { ?>
      <?php
      $error = '';
      unset($_POST[ $hidden_field_name ]);
      unset($_POST[ 'submit' ]);
      // create jsons
      foreach ($_POST as $key => $value) {

        if (isset($_POST['delete'])) {
          $del_key = strip_tags($_POST['delete']);
          unset($value[$del_key]);
          unset($_POST['delete']);
        }

        if($value != null && is_array($value) && $key != 'new_key') {
          create_json($value, $key);
        }
      }
      ?>
      <?php if(!empty($error)) { ?>
        <div class="error">
          <p>
            <strong>
              <?php echo $error; ?>
            </strong>
          </p>
        </div>
      <?php } else { ?>
        <div class="updated">
          <p>
            <strong>
              <?php _e( 'Settings saved.'); ?>
            </strong>
          </p>
        </div>
      <?php } ?>
      <script>
        window.location.href = '<?php echo get_current_url(); ?>';
      </script>
    <?php } ?>
  <div class="wrap client-translations">
    <h2>
      <span class="dashicons dashicons-translation"></span>
      <?php echo trans('client_translations_title'); ?>
    </h2>
    <form name="client_translations" method="post" action="">
      <div id="poststuff" class="postbox">
        <h2 class="hndle">
          <span>
            <?php echo trans('client_translations_main'); ?>
          </span>
        </h2>
        <div class="inside">
            <table class="form-table">
              <?php echo $sText; ?>
            </table>
            <table class="added-translations form-table">
              <tbody>
              </tbody>
            </table>
        </div>
      </div>

      <input type="hidden" name="<?php echo $hidden_field_name; ?>" value="Y">
      <p class="submit">
        <a href="#" class="button add-trans"><?php echo trans('client_translation_add'); ?></a>
        <input type="submit" name="submit" class="button-primary" value="<?php esc_attr_e('Save Changes') ?>" />
      </p>
    </form>
  </div>
  <script type="text/javascript">
    var click = 0;

    var add_trans_new = function(e) {
      e.preventDefault();

      click += 1;
      var all_cols = document.querySelectorAll('.translation-line tbody .col');
      var first_tr = document.querySelectorAll('.translation-line');
      first_tr = first_tr[0];
      var first_cols = first_tr.querySelectorAll('.col');
      var form_table = document.querySelector('.added-translations tbody');
      var all_trans_lines = form_table.querySelectorAll('.added-translations tbody');
      var add_new_html = '';
      add_new_html += '<tr class="translation-line" id="trans_' + click + '">';
       for (var i = 0; i < first_cols.length - 1; i++) {
        //id  name
        var this_input = first_cols[i].children[0];
        var input_lang = this_input.getAttribute('data-lang');
        add_new_html += '<td>';
        if (i == 0) {
          add_new_html += '<input type="text" name="new_key[]" placeholder="KEY" class="regular-text added-to added-to-key">';
        } else {
          add_new_html += '<input type="text" data-lang="' + input_lang + '" placeholder="' + input_lang + '" class="regular-text added-to added-to-val" disabled="disabled">';
        }
        add_new_html += '</td>';
      }
      //append
      form_table.insertAdjacentHTML('beforeEnd', add_new_html);

      var added_inputs = document.querySelectorAll('.added-to');
      for (var i = 0; i < added_inputs.length; i++) {
        if (added_inputs[i].classList.contains('added-to-key')) {
          added_inputs[i].addEventListener('keyup', function(e) {
            var el = e.target;
            el.value = el.value.replace(' ', '');
            var parent_element = el.parentElement.parentElement;
            var added_to_val = parent_element.querySelectorAll('.added-to-val');

            for (var iv = 0; iv < added_to_val.length; iv++) {
              if (el.value.length >= 2) {
                added_to_val[iv].removeAttribute('disabled');
                var this_lang = added_to_val[iv].getAttribute('data-lang');
                added_to_val[iv].name = ''+this_lang + '[' + el.value + ']';
              } else {
                added_to_val[iv].setAttribute('disabled', 'disabled');
              }
            }

          });

        }
      }
    };

    var first_tr = document.querySelectorAll('.client-translations .form-table thead tr');
    var all_tds = first_tr[0].querySelectorAll('th');
    var overfl_el = document.querySelector('.client-translations .postbox .inside');

    if (all_tds.length >= 6) {
      var new_w = 250 * (all_tds.length - 2);
      overfl_el.style.overflow = 'auto';
      overfl_el.children[0].style.width = new_w + 'px';
    }

    var add_trans = document.querySelector('.add-trans');
    add_trans.addEventListener('click', add_trans_new, false );

    var confirm_dialog = function() {
      if (confirm("Press a button!") == true) {
        return true;
      } else {
        return false;
      }
    };
  </script>
  <?php
}