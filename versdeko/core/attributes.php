<?php
/**
 * @Author: Dewdrop | Aivaras Čenkus, TasKurPavaro
 * @Date:   2016-03-15 16:07:54
 * @Last Modified by:   Dewdrop | Aivaras Čenkus
 * @Last Modified time: 2016-07-08 15:00:11
 */
// function json_array_filter(&$value) {
//   $value = htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
//   $value = stripslashes($value);
// }

// function create_json ($array, $filename) {
//   array_walk_recursive($array, "json_array_filter");
//   $fp = fopen(LANG_DIR . $filename . '.json', 'w');
//   fwrite($fp, json_encode($array, JSON_UNESCAPED_UNICODE));
//   fclose($fp);
// }

$attributes_fields = array('Plotis', 'Ilgis');

add_action('admin_head', 'style_attributes');
function style_attributes () {
	
	$style = '<style>
		.attributes .delete-btn {
			cursor: pointer;
			border: none;
			background: none;
			color: red;
		}
		.attributes .delete-btn:hover {
			color: blue;
		}
		.attributes .th-columns {
			background: #F1F1F1;
			border-radius: 4px;
			overflow: hidden;
		}
		.attributes .th-columns th {
			width: auto;
			min-width: 260px;
			padding: 9px 10px;
			text-align: center;
			text-transform: uppercase;
		}
		.attributes .th-columns th:last-child {
			min-width: 50px;
		}
		.attributes .th-columns th:first-child {
			text-align: left;
		}
		.attributes .col input.regular-text {
			min-width: 260px;
			width: auto;
		}
		.attributes th.col {
			min-width: 260px;
			width: auto;
		}
		.attributes thead, .attributes tbody {
			display: block;
		}
    .group-line {
      padding: 0 10px;
      border-top: 1px solid #ddd;
    }
    .group-line:first-child {
      border-top: 0;
    }
	</style>';

	echo $style;
}

add_action( 'add_meta_boxes', 'cd_meta_box_add' );

function cd_meta_box_add() {
		add_meta_box( 'my-meta-box-id', 'My First Meta Box', 'attributes_content', 'post', 'normal', 'high' );
}

add_action( 'save_post', 'cd_meta_box_save' );

function cd_meta_box_save( $post_id ) {
		// Bail if we're doing an auto save
		if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;
		 
		// if our nonce isn't there, or we can't verify it, bail
		if( !isset( $_POST['attributes_group'] ) || !wp_verify_nonce( $_POST['attributes_group'], 'attributes' ) ) return;
		 
		// if our current user can't edit this post, bail
		if( !current_user_can( 'edit_post' ) ) return;

    if(isset($_POST['attributes_group'])){
      // var_dump($_POST['attributes_group']);
      // $attributes_fields
      $attributes_serial = serialize($_POST['attributes_group']);
      // var_dump($attributes_serial);
      update_post_meta( $post_id, 'attributes', $attributes_serial );
    }
    // exit;
		// if( isset($_POST[ $hidden_field_name ]) && $_POST[ $hidden_field_name ] == 'Y' ) {
		
		// 	$error = '';

		// 	foreach ($_POST as $key => $value) {

		// 		if (isset($_POST['delete'])) {
		// 			$del_key = strip_tags($_POST['delete']);
		// 			unset($value[$del_key]);
		// 			unset($_POST['delete']);
		// 		}

		// 		if($value != null && is_array($value) && $key != 'new_key') {
		// 			// create_json($value, $key);
		// 			// update_post_meta( $post -> ID, 'attributes', wp_kses( $_POST['my_meta_box_text'], $allowed ) );
				
		// 		}
		// 	}
		// }
}
// add_action( 'admin_menu', 'client_translations_menu' );
// function client_translations_menu() {
//   add_menu_page( trans('client_translations_title'), trans('client_translations_title'), 'manage_options', 'client_translations', 'attributes_content', 'dashicons-translation' );
// }

function attributes_content() {
	global $aAdminCustomFields, $translations, $post, $attributes_fields;

	// if ( !current_user_can( 'manage_options' ) )  {
	//   wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	// }

	$default_lang = get_option('qtranslate_default_language');
	// $aCurrentLangs = json_decode(url_get_contents($sUrlCurrent));
	$hidden_field_name = 'client_translations';

	// if(function_exists('qtranxf_getSortedLanguages')){
		// $aJsons = array();
		$sText = '<div class="groups">';
		$aJsons = get_post_meta($post -> ID, 'attributes');
		if ($aJsons) {
      $aJsons = unserialize($aJsons[0]);
      // var_dump($aJsons);
      foreach ($aJsons as $group) {
        $sText .= '<div class="group-line">';
        $sText .= '<table class="form-table">';
        $sText .= '<tbody>';
        foreach ($group as $key => $value) {
          $sText .= '<tr class="input-line">';
          $sText .= '<th class="label">';
          $sText .= $attributes_fields[$key];
          $sText .= '</th>';
          $sText .= '<td>';
          $sText .= '<input type="text" name="attributes_group[' . $key . '][]" value="' . $value . '" class="widefat"/>';
          $sText .= '</td>';
          $sText .= '</tr>';
        }
        $sText .= '</tbody>';
        $sText .= '</table>';
        $sText .= '</div>';
      }
		} else {
			$sText .= '<div class="group-line">';
			$sText .= '<table class="form-table">';
			$sText .= '<tbody>';
			$sText .= '<tr class="input-line">';
			$sText .= '<th class="label">';
			$sText .= 'Plotis ';
			$sText .= '</th>';
			$sText .= '<td>';
      // smart-custom-fields[plotis_x_ilgis][0]
      // i18n-multilingual
			$sText .= '<input type="text" name="attributes_group[0][]" value="aa" class="widefat"/>';
			$sText .= '</td>';
			$sText .= '</tr>';
      $sText .= '<tr class="input-line">';
      $sText .= '<th class="label">';
      $sText .= 'Ilgis';
      $sText .= '</th>';
      $sText .= '<td>';
      $sText .= '<input type="text" name="attributes_group[0][]" value="aa" class="widefat"/>';
      $sText .= '</td>';
      $sText .= '</tr>';
			$sText .= '</tbody>';
			$sText .= '</table>';
			$sText .= '</div>';
		}
		$sText .= '</div>';
	// }
	?>

		<form name="client_translations" method="post" action="">
			<?php if(!empty($sText)){ ?>
				<?php echo $sText; ?>
			<?php } ?>
			<div id="major-publishing-actions">
				<div id="duplicate-action">
					<div id="publishing-action">
  					<span class="spinner"></span>
  					<div class="submit">
    					<a href="#" class="button add-trans"><?php echo trans('client_translation_add'); ?></a>
    					<input type="submit" name="submit" class="button-primary" value="<?php esc_attr_e('Save Changes') ?>" />
  				  </div>
					</div>
					<div class="clear"></div>
        </div>
			</div>
		</form>

	<script type="text/javascript">
		var click = 0;
    // console.log(group_line.innerHTML);
    var add_trans_new = function(e) {
      e.preventDefault();

      click += 1;

      var group_lines = document.querySelectorAll('.group-line'),
      group_line = group_lines[group_lines.length - 1],
      group_inputs = group_line.querySelectorAll('.group-line .input-line');
			// var all_cols = document.querySelectorAll('.translation-line tbody .col');
			// var first_tr = document.querySelectorAll('.translation-line');
			// first_tr = first_tr[0];
			// var first_cols = first_tr.querySelectorAll('.col');
			// var form_table = document.querySelector('.added-translations tbody');
			// var all_trans_lines = form_table.querySelectorAll('.added-translations tbody');

			//appendin html
      var add_new_html = '<div class="group-line"><table class="form-table"><tbody>';
      for (var i = 0; i < group_inputs.length; i++) {
        var label = group_inputs[i].querySelector('.label').innerText,
        input_name = group_inputs[i].querySelector('input').name,
        input_name = input_name.split('['),
        input_name = input_name[0];
        console.log(input_name);
        
        for (var inp = 0; inp < group_lines.length; inp++) {
          var new_input_name = input_name + '[' + (inp + 1) + '][]';
        }
        console.log(new_input_name);

        add_new_html += '<tr class="input-line"><th class="label">';
        add_new_html += label;
        add_new_html += '</th><td>';
        add_new_html += '<input type="text" name="' + new_input_name + '" value="" class="widefat"/>';
        add_new_html += '</td></tr></div>';
        // group_inputs[i]
      }
      add_new_html += '</tbody></table></div>';
   //    var add_new_html = '<div class="group-line">';
   // //    var add_new_html = '<div class="group-line"><table class="form-table"><tbody><tr><th>';
   // //    add_new_html += 'Label';
   // //    add_new_html += '</th><td>';
			// // add_new_html += '<input type="text" name="smart-custom-fields[plotis_x_ilgis][0]" value="" class="widefat"/>';
   // //    add_new_html += '</td></tr></tbody></table></div>';
   //    add_new_html += group_line.innerHTML;
			// add_new_html += '</div>';

			//append
			group_line.parentElement.insertAdjacentHTML('beforeEnd', add_new_html);

			// var added_inputs = document.querySelectorAll('.added-to');
			// for (var i = 0; i < added_inputs.length; i++) {
			// 	if (added_inputs[i].classList.contains('added-to-key')) {
			// 		added_inputs[i].addEventListener('keyup', function(e) {
			// 			var el = e.target;
			// 			el.value = el.value.replace(' ', '');
			// 			var parent_element = el.parentElement.parentElement;
			// 			var added_to_val = parent_element.querySelectorAll('.added-to-val');

			// 			for (var iv = 0; iv < added_to_val.length; iv++) {
			// 				if (el.value.length >= 2) {
			// 					added_to_val[iv].removeAttribute('disabled');
			// 					var this_lang = added_to_val[iv].getAttribute('data-lang');
			// 					added_to_val[iv].name = ''+this_lang + '[' + el.value + ']';
			// 				} else {
			// 					added_to_val[iv].setAttribute('disabled', 'disabled');
			// 				}
			// 			}

			// 		});

			// 	}
			// }
		};

		// var first_tr = document.querySelectorAll('.attributes .form-table thead tr');
		// var all_tds = first_tr[0].querySelectorAll('th');
		// var overfl_el = document.querySelector('.attributes .postbox .inside');

		// if (all_tds.length >= 6) {
		//   var new_w = 250 * (all_tds.length - 2);
		//   overfl_el.style.overflow = 'auto';
		//   overfl_el.children[0].style.width = new_w + 'px';
		// }

		var add_trans = document.querySelector('.add-trans');
		add_trans.addEventListener('click', add_trans_new, false );

		// var confirm_dialog = function() {
		// 	if (confirm("Press a button!") == true) {
		// 		return true;
		// 	} else {
		// 		return false;
		// 	}
		// };
	</script>
	<?php
}