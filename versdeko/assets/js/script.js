$(document).ready(function () {
    var fullPage = $('#fullpage');
    fullPage.fullpage();

    // ajax functions
    var page = 1;
    var loading = true;
    var $window = $(window);
    var $content = $('body.page-template-newspage #content');
    var more_txt = $('.ajax-btn').text();
    var mobile_html = $('.mobile-lang')[0].outerHTML;
    $('.mobile-lang').remove();
    var lang = function () {
        if ($(window).width() <= 992 && !$('#menu-top-menu span').hasClass('mobile-lang')) {
            $('#menu-top-menu').append(mobile_html);
        } else {
            if ($(window).width() > 992)
                $('.mobile-lang').remove();
        }
    };
    var load_posts = function () {

        $.ajax({
            type: "GET",
            data: {numPosts: 3, pageNumber: page},
            dataType: "html",
            url: templateDir + '/parts/ajaxhandler.php',
            beforeSend: function () {
                if (page != 1) {
                    $('.ajax-btn').append('<img class="loader" src="' + templateDir + '/assets/images/download.svg' + '" />');
                }

            },
            success: function (data) {
                $data = $(data);
                $data.hide();
                $content.append(data);
                $data.show('slow');
                $('.ajax-btn').find('.loader').remove();
                loading = false;
                if (data === '') {
                    $('#loadmore').hide();
                }


            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#loadmore').hide();
            }
        });
    };

    $('#loadmore').on('click', function (e) {
        e.preventDefault();
        var content_offset = $content.offset();
        if (!loading) {
            loading = true;
            page++;
            checkit(page);
            load_posts();
        }
    });
    var postID = $('#postID').val();

    var load_galleries = function () {

        $.ajax({
            type: "POST",
            data: {numPosts: 9, pageNumber: page, dataID: postID},
            dataType: "html",
            url: templateDir + '/parts/ajaxGalleries.php',
            beforeSend: function () {
                if (page != 1) {
                    $('.ajax-btn').append('<img class="loader" src="' + templateDir + '/assets/images/download.svg' + '" />');
                }

            },
            success: function (data) {
                $data = $(data);
                $data.hide();
                $('.galleries').not('.single').append(data);
                $data.show('slow');
                $('.ajax-btn').find('.loader').remove();
                loading = false;
                if (data === '') {
                    $('#loadGalleries').hide();
                }

            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#loadGalleries').hide();
            }
        });
    };

    $('#loadGalleries').on('click', function (e) {
        e.preventDefault();
        var content_offset = $content.offset();
        if (!loading) {
            loading = true;
            page++;
            checkit(page);
            load_galleries();
        }
    });
    load_posts();
    load_galleries();



    if ($('#google-map').length > 0) {



        var pos = {

            lat: $('#google-map').data('lt'),

            lng: $('#google-map').data('ln')

        };

        var center = {

            lat: pos.lat,

            lng: pos.lng

        }

        var map = new google.maps.Map(document.getElementById('google-map'), {

            zoom: 16,

            center: center,

            scrollwheel: false,

            mapTypeControl: false

        });



        // InfoWindow content

        var image = $('#google-map').data('img');

        var beachMarker = new google.maps.Marker({

            position: pos,

            map: map,

            icon: image,

            title: 'Mus rasite'

        });

        map.set('styles', [
            {
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#faf9f2"
                    }
                ]
            },
            {
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#b4b4b4"
                    }
                ]
            },
            {
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "color": "#ffffff"
                    },
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#c9b2a6"
                    }
                ]
            },
            {
                "featureType": "administrative.land_parcel",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#dcd2be"
                    },
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "administrative.land_parcel",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#ae9e90"
                    },
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "landscape.natural",
                "elementType": "geometry",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "labels",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#447530"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#ffffff"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#f8c967"
                    }
                ]
            },
            {
                "featureType": "road.highway.controlled_access",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#e98d58"
                    }
                ]
            },
            {
                "featureType": "transit.line",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#dfd2ae"
                    }
                ]
            },
            {
                "featureType": "transit.line",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#8f7d77"
                    }
                ]
            },
            {
                "featureType": "transit.line",
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "color": "#ebe3cd"
                    }
                ]
            },
            {
                "featureType": "transit.station",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#dfd2ae"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#dcd9bc"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#92998d"
                    }
                ]
            }
        ]);

    }


    $(window).on('scroll', function () {
        var scrollPosition = $(window).scrollTop();
        if (scrollPosition > 10) {
            $('.header').addClass('scrolled');
        } else {
            $('.header').removeClass('scrolled');
        }

    });

    lang();
    $(window).resize(function () {
        lang();
    });

    if($(window).height() <= 640){
        var texts = $('.block-content').find('p');
        $.each($('.fp-tableCell'),function(key){
           var text =  $(this).find('.section-content').find('.block-content').find('p').text();
           $(this).find('.section-content').find('.block-content').find('p').text($.trim(text).substring(0, 200)
    .split(" ").slice(0, -1).join(" ") + "...");
            text = '';
        });
        
    }

});//end of document ready





// functions
function checkit(page) {
    console.log(page);
    console.log($('#totalPages').val());
    if (Number($('#totalPages').val()) === page) {
        $('.ajax-btn').fadeOut();
    }
}
function create_cookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    } else {
        var expires = "";
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

function read_cookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ')
            c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0)
            return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function erase_cookie(name) {
    createCookie(name, "", -1);
}
//check feuture
function check_feature() {
//historyAPI, placeholder (from IE10)
//webGL (from IE11)
    if (!feature.historyAPI || !feature.placeholder) {
        $('.check_feature-container').fadeIn();
    }
}
//document
//check feature
check_feature();

$('.check_feature-close').on('click', function (e) {
    e.preventDefault();

    create_cookie('check_feature', 1, 7);
    $('.check_feature-container').fadeOut();
})
// moblie menu
$('.header-menu-hamburger').on('click', function () {
    $(this).toggleClass('open');
    $('body').toggleClass('no-scroll');
    $('.header-menu').toggleClass('open');
});
//usecookie
var check_usecookie = function () {
    var exsisting_cookie = read_cookie('usecookie');

    if (exsisting_cookie === '1') {
        return true;
    }
    return false;
};

$('a.set-usecookie').on('click', function (e) {
    e.preventDefault();

    create_cookie('usecookie', 1, 365);

    if (check_usecookie()) {
        $('.usecookie-container').fadeOut('slow');
    }
});