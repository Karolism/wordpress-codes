if ($('table').length > 0) {
    var columnCount = 0;
    //check if thead exsist
    if ($('table').find('thead').length <= 0) {
        var firstTr = $('table tr:first-child');
        $('table').prepend('<thead><tr></tr></thead>');
        firstTr.contents().appendTo('thead tr');
        firstTr.remove();
        if ($('table').find('thead th').length <= 0) {
            $('thead td').each(function() {
                $('table thead tr').prepend('<th></th>');
                $(this).contents().appendTo('thead th');
                $(this).remove();
            });
        }
    }
    $('table tr:first-child th').each(function() {
        columnCount++;
        $(this).attr('data-col', columnCount);
        var thead = $('[data-col="' + columnCount + '"').text();
        $('td:nth-child(' + columnCount + ')').addClass('column-' + columnCount);
        $('.column-' + columnCount).attr('data-title', thead);
    });
}