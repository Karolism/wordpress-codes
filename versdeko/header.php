<!DOCTYPE HTML>
<html <?php language_attributes(); ?><?php echo (SOCIAL_META ? ' itemscope itemtype="http://schema.org/Article"' : ''); ?>>

<head>
  <?php get_part('header', 'head'); ?>
  <!-- wordpress head -->
  <?php wp_head(); ?>
</head>
<body <?php body_class() ?> >
<!-- header -->
<header class="header">
  <div class="container">
    <div class="row">
      <div class="col-md-2 col-xs-9">
        <a href="<?php echo site_url(); ?>" class="logo">
            <img src="<?php echo (is_front_page())?get_field('logo', 'option'):get_field('logotipas', 'option'); ?>" alt="logo">
        </a>
          <span class="mobile-lang">
                <?php echo the_widget('qTranslateXWidget', array('type' => 'both', 'hide-title' => true) ); ?>
          </span>
      </div>
      <div class="col-md-9 col-xs-3 static">
        <div class="header-menu-hamburger">
          <span></span>
          <span></span>
          <span></span>
          <span></span>
        </div>
        <?php echo get_menu('header-menu'); ?>
      </div>
        <div class="col-sm-1 static dekstop-lang">
            <?php echo the_widget('qTranslateXWidget', array('type' => 'both', 'hide-title' => true) ); ?>
        </div>
    </div>
  </div>
</header>
<!-- main container begin -->
<main class="main-container">