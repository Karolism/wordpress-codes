<?php get_header(); ?>
<div class="page-container container">
    <?php echo breadcrumbs(); ?>
    <?php if (have_posts()) { ?>
        <?php while (have_posts()) { ?>
            <?php the_post(); ?>
            <div class="row">
                <?php if(get_the_post_thumbnail_url(get_the_ID())): ?>
                <div class="col-md-11 col-md-offset-1 px-0 over-image">
                    <img  src="<?php echo get_the_post_thumbnail_url(get_the_ID()) ?>" alt=" <?php echo get_the_title(); ?>">
                    <div class="title-holder">
                        <h1 class="news-title"><?php echo get_the_title(); ?></h1>
                    </div>
                </div>
                <?php endif; ?>
            </div>
            <div class="row">
                <div class="col-md-11 col-md-offset-1">
                    <article <?php post_class(); ?>>
                        <div class="news-info">
                            <div class="date">
                                <?php echo get_the_date('F j - Y'); ?>
                            </div>
                            <h2><?php echo get_the_title(); ?></h2>
                        </div>
                        <p>
                            <?php echo nl2br(get_the_content()); ?>
                        </p>
                    </article>
                    <?php
                    $gallery = get_field('photos');
                    if (!empty($gallery)):
                        ?>
                        <div class="news-gallery">
                            <div class="label">
                                <?php echo trans('gallery'); ?>
                            </div>
                            <div class="images">
                               
                                <?php foreach ($gallery as $image): ?>
                                    <a data-fancybox="images" href="<?php echo wp_get_attachment_image_url($image['ID'], 'full'); ?>">
                                        <img src="<?php echo wp_get_attachment_image_url($image['ID'], 'mini-gallery'); ?>">
                                    </a>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <hr>
                    <?php
                    $prevpost = get_adjacent_post(false, '', true);
                    $url = get_permalink($prevpost);
                    $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                    if($url != $actual_link):
                    ?>
                    <a class="paginated-link" href="<?php echo $url; ?>">< <?php echo trans('next_article'); ?></a>
                    <?php endif; ?>
                </div>
            </div>
        <?php } ?>
    <?php } ?>
</div>
<?php
get_footer();
