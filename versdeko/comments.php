<?php
// cia comments.php failas
// if ( post_password_required() )
//  return;
?>

<div id="comments" class="comments-area row">
               
<?php
$fields =  array(
    'author'        => '<div class="col-md-4 form-group"><label class="sr-only">Pavardė/Įmonė</label><input id="author" name="author" type="text" class="form-control" placeholder="Vardas, Pavardė/Įmonė" value="" size="30" /></div>',
    'url'           => '<div class="col-md-4 form-group"><label class="sr-only">Paslaugos tipas</label><input id="url" name="url" type="text" class="form-control" placeholder="Paslaugos tipas" value="" size="30" /></div>',
    'email'         => '<div class="col-md-4 form-group"><label class="sr-only">El.pašto adresas</label></label><input id="email" name="email" type="text" class="form-control" placeholder="El.pašto adresas" value="" size="30"/></div>',
    // 'submit_button' => '<div class="form-group btn-side clearfix"><input name="%1$s" type="submit" class="btn" value="Siųsti žinutę" /> <i class="fa fa-angle-right"></i></div>'
);

                $comment_args = array(
                    // 'fields' => apply_filters('comment_form_default_fields', $fields),
                    'fields' =>  $fields,
                    'comment_field' => '<div class="col-md-12 form-group clearfix"><label class="sr-only">Irašykite žinutę</label><textarea id="comment" name="comment" class="form-control" placeholder="Įrašykite žinutę" aria-required="true"></textarea></div><div class="comment-message"></div>',
                    'comment_notes_after' => '',
                    'comment_notes_before' => '',
                    'title_reply' => '',
                    // 'label_submit' => __('[:lt]Siųsti[:en]Send'),
                    'logged_in_as' => '',
                    
                );
                
                comment_form( $comment_args,  8);
            ?>
                
  
                
                

</div>
<!--  
<div class="row">
    <div class="col-md-4 form-group"><label class="sr-only">Pavardė/Įmonė</label>[text* vardas class:form-control placeholder "Vardas, Pavardė/Įmonė"]</div>
    <div class="col-md-4 form-group"><label class="sr-only">Paslaugos tipas</label>[text* paslauga class:form-control placeholder "Paslaugos tipas"]</div>
    <div class="col-md-4 form-group"><label class="sr-only">El.pašto adresas</label>[email* email class:form-control placeholder "El.pašto adresas"]</div>
</div>

<div class="form-group clearfix">
<label class="sr-only">Irašykite žinutę</label>
  [textarea zinute class:col-md-12 class:form-control placeholder "Irašykite žinutę"]
</div>
<div class="form-group btn-side clearfix">
[submit class:btn "Siųsti žinutę"]<i class="fa fa-angle-right"></i>
</div>
-->