<?php
/* Template Name: Newspage */

get_header();
?>

<div class="container">
    <div class="row">
        <div class="col-md-11 col-md-offset-1">
            <div class="label">
                <?php echo trans('news'); ?>
            </div>
            <?php
            $query = new WP_Query(array('post_type' => 'post'));
            $total = $query->found_posts;
            $total_pages = ceil($total / 3); // How many pages we have in total
            ?>
            <input type="hidden" id="totalPages" name="totalPages" value="<?php echo $total_pages; ?>">
            <div id="content">

            </div>
            <div class="btn-holder text-center">
                <a href="#" class="ajax-btn btn" id="loadmore"><?php echo trans('more') ?><img class="loader" src="<?php echo get_theme_url('/assets/images/download.svg') ?>" /></a>
            </div>

        </div>
    </div>
</div>

<?php
wp_reset_query();
get_footer();
?>

