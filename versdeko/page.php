<?php get_header(); ?>
<div class="page-container container">
    <?php echo breadcrumbs(); ?>
    <?php if (have_posts()) { ?>
        <?php while (have_posts()) { ?>
            <?php the_post(); ?>
            <div class="row">
                <?php if(get_the_post_thumbnail_url(get_the_ID())): ?>
                <div class="col-md-11 col-md-offset-1 px-0 over-image">
                    <img  src="<?php echo get_the_post_thumbnail_url(get_the_ID()) ?>" alt=" <?php echo get_the_title(); ?>">
                    <div class="title-holder">
                        <h1 class="news-title"><?php echo get_the_title(); ?></h1>
                    </div>
                </div>
                <?php endif; ?>
            </div>
            <div class="row">
                <div class="col-md-11 col-md-offset-1">
                    <article <?php post_class(); ?>>
                        <div class="news-info">
<!--                            <div class="date">
                                <?php echo get_the_date('F j - Y'); ?>
                            </div>-->
                            <h2><?php echo get_the_title(); ?></h2>
                        </div>
                        <p>
                            <?php echo nl2br(get_the_content()); ?>
                        </p>
                    </article>
                </div>
            </div>
        <?php } ?>
    <?php } ?>
</div>
<?php
get_footer();
