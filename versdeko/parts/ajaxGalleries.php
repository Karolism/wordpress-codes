<?php

define("WP_USE_THEMES", false);
require_once('../../../../wp-load.php');

$gallery = get_field('galleries', $_POST['dataID']); // Get our gallery
$images = array(); // Set images array for current page

$items_per_page  = 9; // How many items we should display on each page
$total_items = count($gallery); // How many items we have in total
$total_pages = ceil($total_items / $items_per_page); // How many pages we have in total
//Get current page
if ( isset($_POST['pageNumber'])) {
	$current_page = $_POST['pageNumber'];
}elseif ( isset($_POST['pageNumber'])) {
	//this is just in case some odd rewrite, but paged should work instead of page here
	$current_page = $_POST['pageNumber'];
}else{
	$current_page = 1;
}
$starting_point = (($current_page-1)*$items_per_page); // Get starting point for current page

// Get elements for current page
if($gallery){
	$images = array_slice($gallery,$starting_point,$items_per_page);
}
?>
<?php foreach ($images as $gallery): ?>
    <div class="gallery-item">
        <img src="<?php echo (get_the_post_thumbnail_url($gallery->ID, 'small-thumb'))?get_the_post_thumbnail_url($gallery->ID, 'small-thumb'): get_theme_url('/assets/images/noimage.png'); ?>" alt="<?php echo $gallery->post_title ?>">
        <div class="gallery-title">
            <?php echo $gallery->post_title ?>
        </div>
        <a class="gallery-btn" href="<?php echo get_permalink($gallery->ID); ?>"><?php echo trans('more'); ?></a>
    </div>
<?php endforeach; ?>