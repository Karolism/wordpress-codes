<a href="<?php echo get_permalink(get_the_ID()); ?>">
    <div class="news-item" style="background-image: url(<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>)">
        <div class="text">
            <div class="title">
                <?php the_title(); ?>
            </div>
            <div class="description">
                <?php wp_trim_words(the_excerpt(), 50); ?>
            </div>
        </div>
    </div>
</a>