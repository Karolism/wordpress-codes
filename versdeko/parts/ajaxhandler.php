<?php

define("WP_USE_THEMES", false);
require_once('../../../../wp-load.php');

$numPosts = (isset($_GET["numPosts"])) ? $_GET['numPosts'] : 0;
$page = (isset($_GET["pageNumber"])) ? $_GET['pageNumber'] : 0;

query_posts(array(
    'posts_per_page' => $numPosts,
    'paged' => $page
));

if (have_posts()) {
    $i = 1;
    while (have_posts()) {
        the_post();
        if ($i == 1) {
            echo '<div class="first">';
            get_part("news-part");
            echo '</div>';
        } elseif ($i == 2) {
            echo '<div class="news">';
            get_part("news-left");
            echo '</div>';
        } elseif ($i == 3) {
            echo '<div class="news right">';
            get_part("news-right");
            echo '</div>';
        } else {
            
        }
        $i++;
    }
}

wp_reset_query();
?>