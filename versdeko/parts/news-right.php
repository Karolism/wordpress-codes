<a href="<?php echo get_permalink(get_the_ID()); ?>">
    <div class="col-md-8">
        <div class="text">
            <div>
                <div class="title">
                    <?php the_title(); ?>
                </div>
                <div class="description">
                    <?php wp_trim_words(the_excerpt(), 50); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4 px-0 text-center">
        <div class="img">
            <img src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'news-list-thumb'); ?>" alt="">
        </div>
    </div>
</a>