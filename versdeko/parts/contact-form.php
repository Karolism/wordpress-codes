<?php
/**
 * @Author: Dewdrop | Aivaras Čenkus
 * @Date:   2016-03-15 16:07:54
 * @Last Modified by:   Dewdrop | Aivaras Čenkus
 * @Last Modified time: 2016-10-31 14:33:14
 */

include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
?>
<?php if (is_plugin_active('dd_form/dd_form.php')) { ?>
	<form method="post" action="<?php echo admin_url("admin-ajax.php"); ?>" class="contact-form">
	  <div class="form-results"></div>
	  <div class="form-fields">
	    <div class="row">
	      <div class="col-md-6">
                  <label for="name"><?php echo trans('form_name'); ?></label>
                  <input id="name" type="text" class="form-field" name="name" placeholder=" " required="true">
	      </div>
	      <div class="col-md-6">
                  <label for="email"><?php echo trans('form_email'); ?></label>
	        <input id="email" type="email" class="form-field field-email" name="email" placeholder=" ">
	      </div>
<!--	      <div class="col-md-6">
	        <input type="email" class="form-field field-email" name="email" placeholder="<?php echo trans('field_email'); ?>" required="true">
	      </div>
	      <div class="col-md-6">
	        <input type="text" class="form-field field-subject" name="subject" placeholder="<?php echo trans('field_subject'); ?>">
	      </div>-->
<!--	      <div class="col-md-12">
	        <input type="file" name="file" class="form-field">
	      </div>-->
	      <div class="col-md-12">
                  <label for="message"><?php echo trans('field_message'); ?></label>
                  <textarea id="message" name="message" class="form-field field-message" required="true"></textarea>
	      </div>
<!--	      <div class="col-md-12">
	        <?php if(RECAPTCHA) { ?>
	          <div class="g-recaptcha" data-sitekey="<?php echo RECAPTCHA_KEY; ?>"></div>
	        <?php } ?>
	      </div>-->
	      <div class="col-md-12">
	        <button type="submit" name="form-submit" value="1" class="submit-form">
	          <?php echo trans('field_submit'); ?>
	        </button>
	      </div>
	    </div>
	  </div>
	</form>
<?php } else { ?>
	Išjungtas kontaktų formos modulis.
<?php } ?>