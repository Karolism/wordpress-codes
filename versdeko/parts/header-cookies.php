<?php
/**
* @Author: Dewdrop
* @E-mail:   hello@dewdrop.eu
* @Author URL: http://dewdrop.eu
* @Date:   2016-03-03 11:27:48
* @Last Modified by:   Dewdrop
* @Last Modified time: 2016-03-03 11:28:19
*/
?>
<?php if (!isset($_COOKIE['usecookie']) || $_COOKIE['usecookie'] != '1') { ?>
  <div class="usecookie-container">
    <div class="container clearfix">
      <div class="usecookie-info">
        <p>
          <?php echo sprintf(trans('usecookie'), get_bloginfo('name')); ?>
          <a href="/cookies">
            <?php echo trans('learnmore_usecookie'); ?>
          </a>
        </p>
      </div>
      <div class="usecookie-agree">
        <a href="#" class="set-usecookie">
          <?php echo trans('set_usecookie'); ?>
        </a>
      </div>
    </div>
  </div>
<?php } ?>