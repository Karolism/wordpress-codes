<?php
/**
 * @Author: Dewdrop | Aivaras Čenkus
 * @Date:   2016-05-02 13:25:28
 * @Last Modified by:   Dewdrop | Aivaras Čenkus
 * @Last Modified time: 2016-10-31 14:36:13
 */
	include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
	define('ES_NAME', false);
?>
<?php if (is_plugin_active('dd_form/dd_form.php')) { ?>
	<div class="news-subscriber-container">
	  <form class="es_shortcode_form">
	    <div class="es_msg"><span id="es_msg_pg"></span></div>
	    <?php if( ES_NAME ) { ?>
	      <div class="es_textbox">
	      <input class="es_textbox_class" placeholder="Name" name="es_txt_name_pg" id="es_txt_name_pg" value="" maxlength="225" type="text">
	      </div>
	    <?php } ?>
	      
	    <div class="es_textbox">
	      <input class="es_textbox_class" name="es_txt_email_pg" id="es_txt_email_pg" onkeypress="if(event.keyCode==13) es_submit_pages('<?php echo home_url(); ?>')" value="" maxlength="225" type="text">
	    </div>
	    <div class="es_button">
	      <input class="es_textbox_button" name="es_txt_button_pg" id="es_txt_button_pg" onClick="return es_submit_pages('<?php echo home_url(); ?>')" value="Subscribe" type="button">
	    </div>
	    <?php if( !ES_NAME ) { ?>
	      <input name="es_txt_name_pg" id="es_txt_name_pg" value="" type="hidden">
	    <?php } ?>
	    <input name="es_txt_group_pg" id="es_txt_group_pg" value="" type="hidden">
	  </form>
	</div>
<?php } else { ?>
	Išjungtas naujienlaiškių modulis.
<?php } ?>