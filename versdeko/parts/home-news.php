<div class="news-items row">
  <?php
    $news_args = array(
      'post_type'       => 'post', 
      'posts_per_page'  => 3
    );
  ?>
  <?php $news = new WP_Query($news_args); ?>
  <?php if ($news -> have_posts()) { ?>
    <?php while ($news -> have_posts()) { ?>
      <?php $news -> the_post(); ?>
      <article class="news-item col-md-4" >
        <?php if (has_post_thumbnail()) { ?>
          <div class="news-image">
            <a href="<?php echo get_the_permalink(); ?>">
              <?php echo get_the_post_thumbnail('small_thumb'); ?>
            </a>
          </div>
        <?php } ?>
        <div class="news-description">
          <h1>
            <a href="<?php echo get_the_permalink(); ?>">
              <?php echo title(); ?>
            </a>
          </h1>
          <p>
            <?php echo excerpt(); ?>
          </p>
        </div>
      </article>
    <?php } ?>
    <?php wp_reset_postdata(); ?>
  <?php } else { ?>
    <?php echo trans('no_items'); ?>
  <?php } ?>
</div>