<?php get_header(); ?>
<div class="container">
<!-- slider plugin -->
  <?php get_part('slider', 'section'); ?>
<!-- home news section -->
  <?php get_part('home', 'news'); ?>
<!-- contact form plugin -->
  <?php get_part('contact', 'form'); ?>
<!-- news letter subscribe plugin -->
  <?php get_part('email', 'subscriber'); ?>
  <!-- Google map -->
  <div class="contact-container">
    <div class="google-map-side">
      <div id="map" class="google-map"></div>
    </div>
  </div>
</div>
<?php
get_footer();