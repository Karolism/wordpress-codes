<?php get_header(); ?>
<?php if (have_posts()): ?>
    <?php
    while (have_posts()):
        the_post();
        ?>
        <div class="content">
            <div class="container">
                <div class="text">
                    <?php echo the_content(); ?> 
                </div>
            </div>
            <?php
            $furnitures = get_field('baldai');
            if (!empty($furnitures)):
                ?>
                <div class="c-separator"></div>
                <div class="container" id="service-furniture">
                    <div class="col-md-8 col-md-offset-2 px-0">
                        <?php foreach ($furnitures as $furniture): ?>
                            <div class="col-md-3 col-sm-6">
                                <div class="service-item">
                                    <div>
                                        <div class="icon-wrap">
                                            <img src="<?php echo $furniture['ikona']['url']; ?>" alt="<?php echo $furniture['pavadinimas'] ?>">
                                        </div>
                                        <div class="icon-label">
                                            <?php echo $furniture['pavadinimas']; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
                <?php
            endif;
            $galleries = get_field('galleries');
            $images = array(); // Set images array for current page

            $items_per_page = 9; // How many items we should display on each page
            $total_items = count($galleries); // How many items we have in total
            $total_pages = ceil($total_items / $items_per_page); // How many pages we have in total
//Get current page
            if (isset($_POST['pageNumber'])) {
                $current_page = $_POST['pageNumber'];
            } elseif (get_query_var('page')) {
                //this is just in case some odd rewrite, but paged should work instead of page here
                $current_page = get_query_var('page');
            } else {
                $current_page = 1;
            }
            $starting_point = (($current_page - 1) * $items_per_page); // Get starting point for current page
// Get elements for current page
            if ($galleries) {
                $images = array_slice($galleries, $starting_point, $items_per_page);
            }

            if (!empty($images)):
                //your gallery loop here
                ?>
                <div class="c-separator"></div>
                <input type="hidden" id="postID" name="postID" value="<?php echo get_the_ID(); ?>">
                <input type="hidden" id="totalPages" name="totalPages" value="<?php echo $total_pages; ?>">
                <div class="container" id="Galleries">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1 px-0">
                            <div class="label text-center"><?php echo trans('gallery') ?></div>
                            <div class="galleries">

                            </div>
                        </div>
                    </div>
                </div>
                <?php
            endif;
            if ($total_pages > 1 && $current_page < $total_pages) {
                echo '<div class="btn-holder text-center">';
                echo '<a class="ajax-btn btn" id="loadGalleries" href="#"><span>' . trans('more') . '</span<img class="loader" src="'. get_theme_url('/assets/images/download.svg').'" />></a>';
                echo '</div>';
            }
            ?>

        </div>
    <?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>

