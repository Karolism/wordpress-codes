<?php
// Core includes
 include 'services.php';
//Translations
// require "core/langs.php";
// Remover Admin menus
require "core/remove-menus.php";
// Template config
require "core/template-config.php";
//Remover WP updates
require "core/remove-updates.php";
//Breadcrumbs
require "core/breadcrumbs.php";
//Post types
require "core/post-types.php";
// Displayng Admin bar
if (!ADMIN_BAR) {
  add_filter('show_admin_bar', '__return_false');
}
// Theme supports
add_theme_support('menus');
add_theme_support('post-thumbnails');
//Remove emoji
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );
//Main functions
require "core/main-functions.php";
//Client information ADMIN page
if (CLIENT_INFO_PAGE) {
 // require "core/client-information.php";
}
//Client translations ADMIN page
if (function_exists('qtranxf_getSortedLanguages') && CLIENT_TRANSLATION) {
  require "core/client-translations.php";
}
add_filter('wp_nav_menu_objects', 'my_wp_nav_menu_objects', 10, 2);

function my_wp_nav_menu_objects($items, $args) {

    foreach ($items as &$item) {

        $icon = get_field('apibraukta', $item);

        if ($icon) {
            $item->title = '<div class="custom-menu-item apibraukta">' . $item->title.'</div>';
        } else {
              $item->title = '<div class="custom-menu-item">' . $item->title.'</div>';
        }
    }

    return $items;
}
if (function_exists('acf_add_options_page')) {

    acf_add_options_page(array(
        'page_title' => 'Temos nustatymai',
        'menu_title' => 'Temos nustatymai',
        'menu_slug' => 'theme-general-settings',
        'capability' => 'edit_posts',
        'redirect' => false
    ));

//    acf_add_options_sub_page(array(
//        'page_title' => 'Theme Header Settings',
//        'menu_title' => 'Header',
//        'parent_slug' => 'theme-general-settings',
//    ));
}
function load_opt_wp_admin_style() {
    if (!is_admin())
        return;

    global $current_screen;

    if ($current_screen->base == "toplevel_page_theme-general-settings") {
        wp_enqueue_script('qtranslate-admin-options', '/wp-content/themes/versdeko/assets/js/qtrans.js', array(), '1.0', false);
        wp_enqueue_script('qtranslate-admin-options', '/wp-content/plugins/qtranslate-x/admin/js/options.min.js', array(), '1.0', false);
        wp_enqueue_script('qtranslate-admin-common', '/wp-content/plugins/qtranslate-x/admin/js/common.min.js', array(), '1.0', true);
    }
}
add_action('admin_enqueue_scripts', 'load_opt_wp_admin_style');

function load_custom_fonts($init) {

    $stylesheet_url = get_theme_url('/assets/libs/css/fonts.css');  // Note #1
     if(empty($init['content_css'])) {
        $init['content_css'] = $stylesheet_url;
    } else {
        $init['content_css'] = $init['content_css'].','.$stylesheet_url;
    }

    $font_formats = isset($init['font_formats']) ? $init['font_formats'] : 'Lato=Lato;Arial=arial,helvetica,sans-serif;Arial Black=arial black,avant garde;Verdana=verdana,geneva;Webdings=webdings;Wingdings=wingdings,zapf dingbats';

    $custom_fonts = ';'.'Bira=Bira;DINPro-light=DINPro-light;DINPro-Bold=DINPro-Bold;DINNextCYR-Heavy=DINNextCYR=Heavy;DINNextCYR-Italic=DINNextCYR-Italic;DINNextCYR-Light=DINNextCYR-Light;DINNextCYR-Medium=DINNextCYR-Medium;DINNextCYR-Regular=DINNextCYR-Regular;Bangla=Bangla;Amatic-Bold=Amatic-Bold;Chalkduster=Chalkduster;';

    $init['font_formats'] = $font_formats . $custom_fonts;

    return $init;
}
add_filter('tiny_mce_before_init', 'load_custom_fonts');

function load_custom_fonts_frontend() {

    echo '<link type="text/css" rel="stylesheet" href="'.get_theme_url('/assets/libs/css/fonts.css').'">';
}
add_action('wp_head', 'load_custom_fonts_frontend');
add_action('admin_head', 'load_custom_fonts_frontend');

// Callback function to filter the MCE settings
function my_mce_before_init_insert_formats( $init_array ) {  
	// Define the style_formats array
	$style_formats = array(  
		// Each array child is a format with it's own settings
		array(  
			'title' => '.translation',  
			'block' => 'blockquote',  
			'classes' => 'translation',
			'wrapper' => true,
			
		),  
		array(  
			'title' => '⇠.rtl',  
			'block' => 'blockquote',  
			'classes' => 'rtl',
			'wrapper' => true,
		),
		array(  
			'title' => '.ltr⇢',  
			'block' => 'blockquote',  
			'classes' => 'ltr',
			'wrapper' => true,
		),
	);  
	// Insert the array, JSON ENCODED, into 'style_formats'
	$init_array['style_formats'] = json_encode( $style_formats );  
	
	return $init_array;  
  
} 
// Attach callback to 'tiny_mce_before_init' 
add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' ); 
add_image_size( 'small-thumb', 386, 235, true );
add_image_size( 'mini-gallery', 212, 157, true );
add_image_size( 'news-list-thumb', 550, 387, true );

// add short description
/**
 * Filter the excerpt length to 50 characters.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
function theme_slug_excerpt_length( $length ) {
        if ( is_admin() ) {
                return $length;
        }
        return 50;
}
add_filter( 'excerpt_length', 'theme_slug_excerpt_length', 999 );

function my_acf_init() {
	
	acf_update_setting('google_api_key', 'AIzaSyBtDnhj67MymfHrNd7fm-8jtrKLNSHn2vk');
}

add_action('acf/init', 'my_acf_init');