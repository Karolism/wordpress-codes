<?php
// Core includes
// projects include
require "projects.php";
// products include
require "products.php";
require "solutions.php";
//require "services.php";
//Translations
// require "core/langs.php";
// Remover Admin menus
require "core/remove-menus.php";
// Template config
require "core/template-config.php";
//Remover WP updates
require "core/remove-updates.php";
//Breadcrumbs
require "core/breadcrumbs.php";
//Post types
require "core/post-types.php";
//bootstrap navbar
require_once('core/wp_bootstrap_navwalker.php');
// Displayng Admin bar
if (!ADMIN_BAR) {
    add_filter('show_admin_bar', '__return_false');
}
// Theme supports
add_theme_support('menus');
add_theme_support('post-thumbnails');
//Remove emoji
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('admin_print_scripts', 'print_emoji_detection_script');
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action('admin_print_styles', 'print_emoji_styles');
//Main functions
require "core/main-functions.php";
//Client information ADMIN page
if (CLIENT_INFO_PAGE) {
    require "core/client-information.php";
}
//Client translations ADMIN page
if (function_exists('qtranxf_getSortedLanguages') && CLIENT_TRANSLATION) {
    require "core/client-translations.php";
}

function is_msie() {
    if (strpos($_SERVER['HTTP_USER_AGENT'], 'Trident/7.0; rv:11.0') == false) {
        return false;
    }
    return true;
}

// Theme support
function wpb_theme_setup() {
    //navv menus
    register_nav_menus(array(
        'primary' => __('Primary Menu')
    ));
    register_nav_menus(array(
        'Bottom' => __('Bottom Menu')
    ));
}

add_action('after_setup_theme', 'wpb_theme_setup');

add_action('admin_init', 'hide_editor');

function hide_editor() {
    // Get the Post ID.
    if (!isset($_GET['post']))
        return;
    $template_name = get_page_template_slug($_GET['post']);
    // Hide the editor on the page titled 'Homepage'
    if ($template_name == 'homepage.php' || $template_name == 'projects_template.php') {
        remove_post_type_support('page', 'editor');
    }
}

function my_admin_enqueue_scripts() {

    wp_enqueue_script('my-admin-js', get_template_directory_uri() . '/assets/js/visual.js', array(), null, true);
}

add_action('acf/input/admin_enqueue_scripts', 'my_admin_enqueue_scripts');

if (function_exists('acf_add_options_page')) {

    acf_add_options_page(array(
        'page_title' => 'Theme General Settings',
        'menu_title' => 'Theme Settings',
        'menu_slug' => 'theme-general-settings',
        'capability' => 'edit_posts',
        'redirect' => false
    ));

//    acf_add_options_sub_page(array(
//        'page_title' => 'Theme Header Settings',
//        'menu_title' => 'Header',
//        'parent_slug' => 'theme-general-settings',
//    ));
}

//SCRIPTS
function print_my_inline_script() {
    ?>
    <script type="text/javascript">
        var trans_fill_field = '<?php echo trans('fill_field'); ?>';
    </script>
    <?php
}

add_action('wp_head', 'print_my_inline_script');

//flush_rewrite_rules( false );

function rd_duplicate_post_as_draft() {
    global $wpdb;
    if (!( isset($_GET['post']) || isset($_POST['post']) || ( isset($_REQUEST['action']) && 'rd_duplicate_post_as_draft' == $_REQUEST['action'] ) )) {
        wp_die('No post to duplicate has been supplied!');
    }

    /*
     * Nonce verification
     */
    if (!isset($_GET['duplicate_nonce']) || !wp_verify_nonce($_GET['duplicate_nonce'], basename(__FILE__)))
        return;

    /*
     * get the original post id
     */
    $post_id = (isset($_GET['post']) ? absint($_GET['post']) : absint($_POST['post']) );
    /*
     * and all the original post data then
     */
    $post = get_post($post_id);

    /*
     * if you don't want current user to be the new post author,
     * then change next couple of lines to this: $new_post_author = $post->post_author;
     */
    $current_user = wp_get_current_user();
    $new_post_author = $current_user->ID;

    /*
     * if post data exists, create the post duplicate
     */
    if (isset($post) && $post != null) {

        /*
         * new post data array
         */
        $args = array(
            'comment_status' => $post->comment_status,
            'ping_status' => $post->ping_status,
            'post_author' => $new_post_author,
            'post_content' => $post->post_content,
            'post_excerpt' => $post->post_excerpt,
            'post_name' => $post->post_name,
            'post_parent' => $post->post_parent,
            'post_password' => $post->post_password,
            'post_status' => 'draft',
            'post_title' => $post->post_title,
            'post_type' => $post->post_type,
            'to_ping' => $post->to_ping,
            'menu_order' => $post->menu_order
        );

        /*
         * insert the post by wp_insert_post() function
         */
        $new_post_id = wp_insert_post($args);

        /*
         * get all current post terms ad set them to the new post draft
         */
        $taxonomies = get_object_taxonomies($post->post_type); // returns array of taxonomy names for post type, ex array("category", "post_tag");
        foreach ($taxonomies as $taxonomy) {
            $post_terms = wp_get_object_terms($post_id, $taxonomy, array('fields' => 'slugs'));
            wp_set_object_terms($new_post_id, $post_terms, $taxonomy, false);
        }

        /*
         * duplicate all post meta just in two SQL queries
         */
        $post_meta_infos = $wpdb->get_results("SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id=$post_id");
        if (count($post_meta_infos) != 0) {
            $sql_query = "INSERT INTO $wpdb->postmeta (post_id, meta_key, meta_value) ";
            foreach ($post_meta_infos as $meta_info) {
                $meta_key = $meta_info->meta_key;
                if ($meta_key == '_wp_old_slug')
                    continue;
                $meta_value = addslashes($meta_info->meta_value);
                $sql_query_sel[] = "SELECT $new_post_id, '$meta_key', '$meta_value'";
            }
            $sql_query .= implode(" UNION ALL ", $sql_query_sel);
            $wpdb->query($sql_query);
        }


        /*
         * finally, redirect to the edit post screen for the new draft
         */
        wp_redirect(admin_url('post.php?action=edit&post=' . $new_post_id));
        exit;
    } else {
        wp_die('Post creation failed, could not find original post: ' . $post_id);
    }
}

add_action('admin_action_rd_duplicate_post_as_draft', 'rd_duplicate_post_as_draft');

/*
 * Add the duplicate link to action list for post_row_actions
 */

function rd_duplicate_post_link($actions, $post) {
    if (current_user_can('edit_posts')) {
        $actions['duplicate'] = '<a href="' . wp_nonce_url('admin.php?action=rd_duplicate_post_as_draft&post=' . $post->ID, basename(__FILE__), 'duplicate_nonce') . '" title="Duplicate this item" rel="permalink">Duplicate</a>';
    }
    return $actions;
}

add_filter('post_row_actions', 'rd_duplicate_post_link', 10, 2);

function get_pages_by_template($template = '', $args = array()) {
    if (empty($template))
        return false;
    if (strpos($template, '.php') !== (strlen($template) - 4))
        $template .= '.php';
    $args['meta_key'] = '_wp_page_template';
    $args['meta_value'] = $template;
    return get_pages($args);
}

remove_filter('the_content', 'wpautop');

remove_filter('the_excerpt', 'wpautop');
function tinymce_remove_root_block_tag( $init ) {
    $init['forced_root_block'] = false; 
    return $init;
}
add_filter( 'tiny_mce_before_init', 'tinymce_remove_root_block_tag' );