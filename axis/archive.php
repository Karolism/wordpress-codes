<?php get_header(); ?>
<div class="page-container container">
  <?php echo breadcrumbs(); ?>
  <?php if (have_posts()) { ?>
    <?php while (have_posts()) { ?>
      <?php the_post(); ?>
      <article <?php post_class(); ?>>
        <h1>
          <?php echo get_the_title(); ?>
        </h1>
        <p>
          <?php echo get_the_content(); ?>
        </p>
      </article>
    <?php } ?>
  <?php } ?>
</div>
<?php
get_footer();