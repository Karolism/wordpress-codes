<?php /* Template Name: Career */ ?>
<?php get_header(); ?>
<?php get_part('menu-section'); ?> 
<?php
$positions = get_field('positions');
$photo = get_field('career_photo');
$person_name = get_field('career_person_name');
$person_position = get_field('career_status');
$phone = get_field('career_phone');
$email = get_field('career_email');
$career_mini = get_field('career_mini');
$cities = [];
$intresting = get_field('sritys');
$career_title = get_field('page_title');
$career_description = get_field('page_description');
?>
<div class="page-container container-fluid" style="background-image: url(<?php echo get_theme_url('/assets/images/bg-career.png'); ?>)">
    <div class="col-md-offset-2 col-md-8">         
        <div class="info-holder">
            <h1><?php echo $career_title; ?></h1>
            <div class="desc">
                <?php echo $career_description; ?>
            </div>
        </div>
        <section class="page-content">
            <?php
            if (!empty($positions)):
                ?>
                <ul id="positions">
                    <?php foreach ($positions as $position): ?>
                        <li>
                            <div class="fl title"><?php echo $position['career_title']; ?></div>
                            <div class="fr">
                                <div class="city"><?php
                                    $position['career_city'] = ucfirst($position['career_city']);

                                    echo $position['career_city'];
                                    if (!in_array($position['career_city'], $cities)) {
                                        array_push($cities, $position['career_city']);
                                    }
                                    ?></div>
                                <div class="arrow-in"></div>
                            </div>
                            <div class="expandable-content">
                                <?php echo $position['career_text']; ?>
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>
            <div class="manager-block">
                <div class="manager-holder">
                    <div class="person-holder">
                        <div class="person-img">
                            <img class="img-responsive" src="<?php echo $photo; ?>" alt="person">
                        </div>
                        <div class="person-info">
                            <div class="person-name"><?php echo $person_name; ?></div>
                            <div class="person-status"><?php echo $person_position; ?></div>
                            <div class="person-contact"><?php echo $phone; ?> <br>
                                <?php echo $email; ?></div>
                        </div>
                    </div>
                    <div class="quote">
                        <?php echo $career_mini; ?>
                    </div>
                    <div class="btn write" data-toggle="modal" data-target=".modal-profile-lg">Siųsti užklausą</div>
                </div>
            </div>
        </section>
    </div>

</div>

<!-- .modal-profile -->
<div class="modal fade modal-profile" tabindex="-1" role="dialog" aria-labelledby="modalProfile" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" type="button" data-dismiss="modal"></button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="col-md-offset-3 col-md-6">
                        <div class="heading mail"><?php echo trans('anketa') ?></div>
                        <?php
                        include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
                        ?>
                        <form method="post" action="<?php echo admin_url("admin-ajax.php"); ?>" class="cv-form">
                            
                            <input type="text" name="name" class="form-field" placeholder="<?php echo trans('form_name'); ?>" required="true">
                            <input type="text" name="lastname" class="form-field" placeholder="<?php echo trans('form_lastname'); ?>">
                            <select name="city" class="form-field" id="s_select">
                                <option value="" selected="" disabled><?php echo trans('city'); ?></option>
                                <?php
                                foreach ($cities as $city) {

                                    echo '<option value="' . $city . '">' . $city . '</option>';
                                }
                                ?>
                            </select>
                            <input type="email" class="form-field field-email" name="email" placeholder="<?php echo trans('client_email'); ?>" required="true">
                            <input type="number" class="form-field field-phone" name="phone" placeholder="<?php echo trans('form_phone'); ?>" required="true">
                            <select name="intresting" id="exp" required="true">
                                <option value="" disabled selected=""><?php echo trans('intrestingarea') ?></option>
                                <?php
                                foreach ($intresting as $area) {
                                    if(isset($area['srities_pavadinimas']) && $area['srities_pavadinimas'] !== '')
                                    echo '<option value="' . $area['srities_pavadinimas'] . '">' . $area['srities_pavadinimas'] . '</option>';
                                }
                                ?>
                            </select>
                            <input type="text" name="education" placeholder="<?php echo trans('education') ?>" required="true">
                            <input type="file" name="cvfile" id="upload-cv" accept=".xls,.xlsx, .pdf, .docx, .doc">
                            <div class="upload" data-id="upload-cv">
                                <div class="icon">
                                    <img src="<?php echo get_theme_url('/assets/images/file.png'); ?>" alt="">
                                </div>
                                <div class="form-label"><span><?php echo trans('upload_cv') ?></span></div>

                            </div>
                             <input type="hidden" name="emailto" value="<?php echo $email; ?>">
                           <div class="form-results"></div>
                                <button type="submit" name="form-submit" value="1" class="submit-form btn write"><?php echo trans('send_cv') ?></button>  
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<script>
    $(document).ready(function () {
        $('#s_select, #exp').niceSelect();
        $('submit-form').on('click', function(){
          var upload_input = $('#upload-cv').val();  
          
        if(!upload_input){
             output = '<div class="form-success"><?php echo trans("no_file") ?></div>';
             $('.cv-form .form-results').html(output).slideDown();
        }
        });
    });
</script>
<!-- Wordpress footer -->
<?php wp_footer(); ?>
<?php get_footer(); ?>