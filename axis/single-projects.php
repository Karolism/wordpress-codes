<?php
/*
  Template Name: Project
  Template Post Type: post, projects, project
 */
?>

<?php get_header(); ?>
<?php get_part('menu-section'); ?> 
<?php
$prevpost = get_adjacent_post(false, '', true, 'category');
$url = get_permalink($prevpost);

?>
<div class="page-container container-fluid" style="background-image: url(<?php echo get_theme_url('/assets/images/bg-project-inner.png'); ?>)">
    <div class="col-md-offset-2 col-md-8">         
        <div class="info-holder">
            <h1 class="preline h1-big"><?php the_title(); ?></h1>
        </div>
        <section class="page-content m53 inner">
            <div class="controls">
                <div class="fl"><div class="all-news"><img src="<?php echo get_theme_url('/assets/images/dots.png') ?>" alt=""></div>
                    <a href="<?php echo get_permalink(166); ?>" class="news-list"><?php echo trans('goProjects') ?></a>
                </div>
                <?php if($url !== get_permalink()): ?>
                <div class="fr">
                    <a href="<?php echo $url; ?>" class="next"><?php echo trans('nextProject') ?></a>
                    <div class="all-news">&#xe60a</div>
                </div>
                <?php endif; ?>
                <div class="clearfix"></div>
            </div>

            <div class="project-inner">
                <div class="fl">

                    <div class="owl-carousel" id="product-slider">
                        <?php
                        if (have_rows('project_photos')):
                            while (have_rows('project_photos')): the_row();
                                ?>
                                <div class="item" style="background-image: url(<?php echo get_sub_field('project_photo'); ?>)"><img class="img-responsive" src="<?php echo get_sub_field('project_photo'); ?>" alt=""></div>
                                <?php
                            endwhile;
                        endif;
                        ?>
                    </div>
                </div>
                <div class="fr">
                    <ul class="project-attr">
                        <?php
                        $spec = get_field('specifikacijos');
                        if (!empty($spec['project_power'])):
                            ?>
                            <li>
                                <div class="pr-item">
                                    <div class="file" style="background-image: url(<?php echo get_theme_url('/assets/images/electric.png') ?>)"></div>
                                    <div class="file-name"><?php echo $spec['project_power']; ?></div>
                                    <div class="download"></div>
                                </div>
                            </li>
                            <?php
                        endif;
                        if (!empty($spec['project_place'])):
                            ?>
                            <li>
                                <div class="pr-item">
                                    <div class="file" style="background-image: url(<?php echo get_theme_url('/assets/images/map.png') ?>)"></div>
                                    <div class="file-name"><?php echo $spec['project_place']; ?></div>
                                    <div class="download"></div>
                                </div>
                            </li>
                            <?php
                        endif;
                        if (!empty($spec['project_date'])):
                            ?>
                            <li>
                                <div class="pr-item">
                                    <div class="file" style="background-image: url(<?php echo get_theme_url('/assets/images/calendar.png') ?>)"></div>
                                    <div class="file-name"><?php echo $spec['project_date']; ?></div>
                                    <div class="download"></div>
                                </div>
                            </li>
                            <?php
                        endif;
                        if (!empty($spec['project_file'])):
                            ?>
                            <li>
                                <a href="<?php echo $spec['project_file']['url']; ?>" class="pr-item" target="_blank" download>
                                    <div class="file" style="background-image: url(<?php echo get_theme_url('/assets/images/pdf.png') ?>)"></div>
                                    <div class="file-name"><?php echo $spec['project_file']['title']; ?></div>
                                    <div class="download"><?php echo trans('download'); ?></div>
                                </a>
                            </li>
                            <?php
                        endif;
                        ?>
                    </ul>
                    <div class="equipment">
                        <?php
                        if (!empty($spec['pagrindine_iranga'])):
                            ?>
                            <div class="e-label">
    <?php echo trans('main_equipment') ?>  
                            </div>
                            <ul>
                               <?php
                               foreach($spec['pagrindine_iranga'] as $iranga){
                                   echo '<li>'.$iranga['iranga'].'</li>';
                               }
                               ?>
                            </ul>
<?php endif; ?>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="project-content">
               <?php
               $desc = get_field('project_description');
               if(!empty($desc)){
                   echo $desc;
               }
               ?>
            </div>
        </section>
    </div>
</div>


<script>
    var owl = $(".owl-carousel");
    owl.owlCarousel({
        loop: false,
        items: 1,
        margin: 0,
        stagePadding: 0,
        autoplay: false,
        callbacks: true,
        dots: true,
        nav: false,
        responsive: {
            0: {
                nav: false,
            },
            768: {
                nav: false,
            }

        }
    });

    dotcount = 1;

    jQuery('.owl-dot').each(function () {
        jQuery(this).addClass('dotnumber' + dotcount);
        jQuery(this).attr('data-info', dotcount);
        dotcount = dotcount + 1;
    });

    slidecount = 1;

    jQuery('.owl-item').not('.cloned').each(function () {
        jQuery(this).addClass('slidenumber' + slidecount);
        slidecount = slidecount + 1;
    });

    jQuery('.owl-dot').each(function () {
        grab = jQuery(this).data('info');
        slidegrab = jQuery('.slidenumber' + grab + ' img').attr('src');
        jQuery(this).css("background-image", "url(" + slidegrab + ")");
    });

    amount = $('.owl-dot').length;
    gotowidth = 77;
    maxscroll = amount * gotowidth - gotowidth;
    $(".godown").on("click", function () {
        $(".owl-dots").animate({
            scrollTop: gotowidth
        });

    });
    $(".goup").on("click", function () {
        $(".owl-dots").animate({
            scrollTop: -gotowidth
        });

    });
    if (amount > 7) {
        $('.owl-carousel .owl-dots').css('display', 'block');
    }
    if (amount <= 7) {
        $(".godown").addClass("disabled");
        $(".goup").addClass("disabled");
    }
</script>


<?php get_footer(); ?>