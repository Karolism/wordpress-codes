<?php
/**
 * @Author: Dewdrop | Aivaras Čenkus
 * @Date:   2016-03-15 16:07:54
 * @Last Modified by:   Dewdrop | Aivaras Čenkus
 * @Last Modified time: 2016-07-27 08:42:32
 */
if(!defined('ABSPATH'))exit;

add_action( 'admin_menu', 'client_options_menu' );

function client_options_menu() {
	add_menu_page( trans('client_admin_title'), trans('client_admin_title'), 'manage_options', 'client_options', 'client_options_page', 'dashicons-id' );
}

if (function_exists('qtranxf_getSortedLanguages') && $pagenow == 'admin.php' && isset($_GET['page']) && $_GET['page'] == 'client_options') {
	function q_Conf () {
		?>
		<script type="text/javascript">
    // <![CDATA[
    var qTranslateConfig= {
    	"default_language":"en",
    	"language":"<?=qtrans_getLanguage();?>",
    	"url_mode":2,
    	"lsb_style_wrap_class":"qtranxs-lang-switch-wrap",
    	"lsb_style_active_class":"active",
    	"hide_default_language":false,
    	"custom_fields":[],
    	"custom_field_classes":[],
    	"homeinfo_path":"\/",
    	"home_url_path":"\/lt\/",
    	"flag_location":"http:\/\/solutus.opo.lt\/wp-content\/plugins\/qtranslate-x\/flags\/",
    	"js":[],
    	"language_config":{
    		"lt":{
    			"flag":"lt.png",
    			"name":"Lithuanian",
    			"locale":"lt_LT",
    			"locale_html":"lt"
    		},
    		"en":{
    			"flag":"gb.png",
    			"name":"English",
    			"locale":"en_US",
    			"locale_html":"en"
    		}
    	},
    	"page_config":{
    		"pages":{
    			"post.php":"",
    			"post-new.php":""
    		},
    		"anchors":{
    			"post-body-content":{
    				"where":"first last"
    			}
    		},
    		"forms":{
    			"post":{
    				"fields":{
    					"title":[],
    					"excerpt":[],
    					"attachment_caption":[],
    					"attachment_alt":[],
    					"view-post-btn":{"encode":"display"},
    					"wp-editor-area":{"jquery":".wp-editor-area"},
    					"wp-caption-text":{
    						"jquery":".wp-caption-text",
    						"encode":"display"
    					}
    				}
    			},
    			"wpbody-content":{
    				"fields":{
    					"wrap-h1":{
    						"jquery":".wrap h1",
    						"encode":"display"
    					},
    					"wrap-h2":{
    						"jquery":".wrap h2",
    						"encode":"display"
    					}
    				}
    			}
    		}
    	},
    	"LSB":true
    };
    function qtranxj_get_cookie(e){for(var n=document.cookie.split(";"),a=0;a<n.length;++a){var t=n[a],i=t.split("=");if(i[0].trim()==e&&!(n.length<2))return i[1].trim()}return""}function qtranxj_ce(e,n,a,t){var i=document.createElement(e);if(n)for(prop in n)i[prop]=n[prop];return a&&(t&&a.firstChild?a.insertBefore(i,a.firstChild):a.appendChild(i)),i}qtranxj_get_split_blocks=function(e){var n=/(<!--:[a-z]{2}-->|<!--:-->|\[:[a-z]{2}\]|\[:\]|\{:[a-z]{2}\}|\{:\})/gi;return e.xsplit(n)},qtranxj_split=function(e){var n=qtranxj_get_split_blocks(e);return qtranxj_split_blocks(n)},qtranxj_split_blocks=function(e){var n=new Object;for(var a in qTranslateConfig.language_config)n[a]="";if(!e||!e.length)return n;if(1==e.length){var t=e[0];for(var a in qTranslateConfig.language_config)n[a]+=t;return n}for(var i,r=/<!--:([a-z]{2})-->/gi,o=/\[:([a-z]{2})\]/gi,s=/\{:([a-z]{2})\}/gi,a=!1,l=0;l<e.length;++l){var t=e[l];if(t.length)if(i=r.exec(t),r.lastIndex=0,null==i)if(i=o.exec(t),o.lastIndex=0,null==i)if(i=s.exec(t),s.lastIndex=0,null==i)if("<!--:-->"!=t&&"[:]"!=t&&"{:}"!=t)if(a)n[a]?n[a]+=t:n[a]=t,a=!1;else for(var c in n)n[c]+=t;else a=!1;else a=i[1];else a=i[1];else a=i[1]}return n},String.prototype.xsplit=function(e){if(3==="a~b".split(/(~)/).length)return this.split(e);e.global||(e=new RegExp(e.source,"g"+(e.ignoreCase?"i":"")));for(var n,a=0,t=[];null!=(n=e.exec(this));)t.push(this.slice(a,n.index)),n.length>1&&t.push(n[1]),a=e.lastIndex;return a<this.length&&t.push(this.slice(a)),a==this.length&&t.push(""),t};var qTranslateX=function(e){var n=this;qTranslateConfig.qtx=this,this.getLanguages=function(){return qTranslateConfig.language_config},this.getFlagLocation=function(){return qTranslateConfig.flag_location},this.isLanguageEnabled=function(e){return!!qTranslateConfig.language_config[e]};var a=function(e){document.cookie="qtrans_edit_language="+e};qTranslateConfig.activeLanguage,qTranslateConfig.LSB?(qTranslateConfig.activeLanguage=qtranxj_get_cookie("qtrans_edit_language"),qTranslateConfig.activeLanguage&&this.isLanguageEnabled(qTranslateConfig.activeLanguage)||(qTranslateConfig.activeLanguage=qTranslateConfig.language,this.isLanguageEnabled(qTranslateConfig.activeLanguage)?a(qTranslateConfig.activeLanguage):qTranslateConfig.LSB=!1)):(qTranslateConfig.activeLanguage=qTranslateConfig.language,a(qTranslateConfig.activeLanguage)),this.getActiveLanguage=function(){return qTranslateConfig.activeLanguage},this.getLanguages=function(){return qTranslateConfig.language_config};var t={};this.hasContentHook=function(e){return t[e]},this.addContentHook=function(e,a,i){if(!e)return!1;switch(e.tagName){case"TEXTAREA":break;case"INPUT":switch(e.type){case"button":case"checkbox":case"password":case"radio":case"submit":return!1}break;default:return!1}if(!i){if(!e.name)return!1;i=e.name}if(e.id){if(t[e.id]){if(jQuery.contains(document,e))return t[e.id];n.removeContentHook(e)}}else if(t[i]){var r=0;do++r,e.id=i+r;while(t[i])}else e.id=i;var o=t[e.id]={};o.name=i,o.contentField=e,o.lang=qTranslateConfig.activeLanguage;var s=qtranxj_split(e.value);e.value=s[o.lang];var l;if(a)switch(a){case"slug":l="qtranslate-slugs[";break;case"term":l="qtranslate-terms[";break;default:l="qtranslate-fields["}else a="[",l="qtranslate-fields[";var c,f,g=o.name.indexOf("[");if(0>g)c=l+o.name+"]";else if(c=l+o.name.substring(0,g)+"]",o.name.lastIndexOf("[]")<0)c+=o.name.substring(g);else{var u=o.name.length-2;u>g&&(c+=o.name.substring(g,u)),f="[]"}o.fields={};for(var d in s){var h=s[d],v=c+"["+d+"]";f&&(v+=f);var C=qtranxj_ce("input",{name:v,type:"hidden",className:"hidden",value:h});o.fields[d]=C,e.parentNode.insertBefore(C,e)}switch(a){case"slug":case"term":o.sepfield=qtranxj_ce("input",{name:c+"[qtranslate-original-value]",type:"hidden",className:"hidden",value:s[qTranslateConfig.default_language]});break;default:o.sepfield=qtranxj_ce("input",{name:c+"[qtranslate-separator]",type:"hidden",className:"hidden",value:a})}return e.parentNode.insertBefore(o.sepfield,e),o.encode=a,e.className+=" qtranxs-translatable",o},this.addContentHookC=function(e){return n.addContentHook(e,"[")},this.addContentHookB=function(e){return n.addContentHook(e,"[")},this.addContentHookById=function(e,a,t){return n.addContentHook(document.getElementById(e),a,t)},this.addContentHookByIdName=function(e){var a;switch(e[0]){case"<":case"[":a=e.substring(0,1),e=e.substring(1)}return n.addContentHookById(e,a)},this.addContentHookByIdC=function(e){return n.addContentHookById(e,"[")},this.addContentHookByIdB=function(e){return n.addContentHookById(e,"[")},this.addContentHooks=function(e,a,t){for(var i=0;i<e.length;++i){var r=e[i];n.addContentHook(r,a,t)}};var i=function(e,a,t){a||(a=document);var i=a.getElementsByClassName(e);n.addContentHooks(i,t)};this.addContentHooksByClass=function(e,n){var a;(0==e.indexOf("<")||0==e.indexOf("["))&&(a=e.substring(0,1),e=e.substring(1)),i(e,n,a)},this.addContentHooksByTagInClass=function(e,a,t){for(var i=t.getElementsByClassName(e),r=0;r<i.length;++r){var o=i[r],s=o.getElementsByTagName(a);n.addContentHooks(s)}};var r=function(e){if(!e)return!1;e.sepfield&&jQuery(e.sepfield).remove();for(var n in e.fields)jQuery(e.fields[n]).remove();return delete t[e.contentField.id],!0};this.removeContentHook=function(e){if(!e)return!1;if(!e.id)return!1;if(!t[e.id])return!1;var n=t[e.id];return r(n),jQuery(e).removeClass("qtranxs-translatable"),!0},this.refreshContentHook=function(e){if(!e)return!1;if(!e.id)return!1;var a=t[e.id];return a&&r(a),n.addContentHook(e)};var o=[],s=function(e){if(!e.nodeValue)return 0;var n=qtranxj_get_split_blocks(e.nodeValue);if(!n||!n.length||1==n.length)return 0;var a={};return a.nd=e,a.contents=qtranxj_split_blocks(n),e.nodeValue=a.contents[qTranslateConfig.activeLanguage],o.push(a),1},l=[],c=function(e){if(!e.value)return 0;var n=qtranxj_get_split_blocks(e.value);if(!n||!n.length||1==n.length)return 0;var a={};return a.nd=e,a.contents=qtranxj_split_blocks(n),e.value=a.contents[qTranslateConfig.activeLanguage],l.push(a),1};this.addDisplayHook=function(e){if(!e||!e.tagName)return 0;switch(e.tagName){case"TEXTAREA":return 0;case"INPUT":switch(e.type){case"submit":if(e.value)return c(e);default:return 0}}var a=0;if(e.childNodes&&e.childNodes.length)for(var t=0;t<e.childNodes.length;++t){var i=e.childNodes[t];switch(i.nodeType){case 1:a+=n.addDisplayHook(i);break;case 2:case 3:a+=s(i)}}return a},this.addDisplayHookById=function(e){return n.addDisplayHook(document.getElementById(e))};var f=function(e){text=e.contentField.value,e.wpautop&&window.switchEditors&&(text=window.switchEditors.wpautop(text)),e.mce.setContent(text,{format:"html"})},g=function(e){a(e);for(var n=o.length;--n>=0;){var i=o[n];i.nd.parentNode?i.nd.nodeValue=i.contents[e]:o.splice(n,1)}for(var n=l.length;--n>=0;){var i=l[n];i.nd.parentNode?i.nd.value=i.contents[e]:l.splice(n,1)}for(var r in t){var i=t[r],s=i.mce&&!i.mce.hidden;s&&i.mce.save({format:"html"}),i.fields[i.lang].value=i.contentField.value,i.lang=e;var c=i.fields[i.lang].value;i.contentField.placeholder&&""!=c&&(i.contentField.placeholder=""),i.contentField.value=c,s&&f(i)}};this.addDisplayHooks=function(e){for(var a=0;a<e.length;++a){var t=e[a];n.addDisplayHook(t)}},this.addDisplayHooksByClass=function(e,a){var t=a.getElementsByClassName(e);n.addDisplayHooks(t)},this.addDisplayHooksByTagInClass=function(e,a,t){for(var i=t.getElementsByClassName(e),r=0;r<i.length;++r){var o=i[r],s=o.getElementsByTagName(a);n.addDisplayHooks(s)}},this.addCustomContentHooks=function(){for(var e=0;e<qTranslateConfig.custom_fields.length;++e){var a=qTranslateConfig.custom_fields[e];n.addContentHookByIdName(a)}for(var e=0;e<qTranslateConfig.custom_field_classes.length;++e){var a=qTranslateConfig.custom_field_classes[e];n.addContentHooksByClass(a)}qTranslateConfig.LSB&&setTinyMceInit()};var u=function(e){e(".i18n-multilingual").each(function(e,a){n.addContentHook(a,"[")}),e(".i18n-multilingual-curly").each(function(e,a){n.addContentHook(a,"{")}),e(".i18n-multilingual-term").each(function(e,a){n.addContentHook(a,"term")}),e(".i18n-multilingual-slug").each(function(e,a){n.addContentHook(a,"slug")}),e(".i18n-multilingual-display").each(function(e,a){n.addDisplayHook(a)})},d=function(e){for(var a in e){var t,i=e[a];if(i.form){if(i.form.id)t=document.getElementById(i.form.id);else if(i.form.jquery)t=$(i.form.jquery);else if(i.form.name){var r=document.getElementsByName(i.form.name);r&&r.length&&(t=r[0])}}else t=document.getElementById(a);t||(t=v(),t||(t=document));for(var o in i.fields){var s=i.fields[o],l=[];if(s.container_id){var c=document.getElementById(s.container_id);c&&l.push(c)}else s.container_jquery?l=$(s.container_jquery):s.container_class?l=document.getElementsByClassName(s.container_class):l.push(t);var f=s.encode;switch(f){case"none":continue;case"display":if(s.jquery)for(var g=0;g<l.length;++g){var c=l[g],u=jQuery(c).find(s.jquery);n.addDisplayHooks(u)}else{var d=s.id?s.id:o;n.addDisplayHook(document.getElementById(d))}break;case"[":case"<":case"{":case"byline":default:if(s.jquery)for(var g=0;g<l.length;++g){var c=l[g],u=jQuery(c).find(s.jquery);n.addContentHooks(u,f,s.name)}else{var d=s.id?s.id:o;n.addContentHookById(d,f,s.name)}}}}},h=function(){function e(e){var n=e.id;if(n){var a=t[n];if(a&&!a.mce){a.mce=e,e.getContainer().className+=" qtranxs-translatable",e.getElement().className+=" qtranxs-translatable";var i=a.updateTinyMCEonInit;if(null==i){var r=e.getContent({format:"html"}).replace(/\s+/g,""),o=a.contentField.value.replace(/\s+/g,"");i=r!=o}return i&&f(a),a}}}setTinyMceInit=function(){if(window.tinyMCE)for(var n in t){var a=t[n];if("TEXTAREA"===a.contentField.tagName&&!a.mce&&!a.mceInit&&tinyMCEPreInit.mceInit[n]){if(a.mceInit=tinyMCEPreInit.mceInit[n],a.mceInit.wpautop){a.wpautop=a.mceInit.wpautop;var i=tinymce.DOM.select("#wp-"+n+"-wrap");i&&i.length&&(a.wrapper=i[0],a.wrapper&&(tinymce.DOM.hasClass(a.wrapper,"tmce-active")&&(a.updateTinyMCEonInit=!0),tinymce.DOM.hasClass(a.wrapper,"html-active")&&(a.updateTinyMCEonInit=!1)))}else a.updateTinyMCEonInit=!1;tinyMCEPreInit.mceInit[n].init_instance_callback=function(n){e(n)}}}},setTinyMceInit(),loadTinyMceHooks=function(){if(window.tinyMCE&&tinyMCE.editors)for(var n=0;n<tinyMCE.editors.length;++n){var a=tinyMCE.editors[n];e(a)}},window.addEventListener("load",loadTinyMceHooks)};qTranslateConfig.onTabSwitchFunctions||(qTranslateConfig.onTabSwitchFunctions=[]),qTranslateConfig.onTabSwitchFunctionsSave||(qTranslateConfig.onTabSwitchFunctionsSave=[]),qTranslateConfig.onTabSwitchFunctionsLoad||(qTranslateConfig.onTabSwitchFunctionsLoad=[]),this.addLanguageSwitchListener=function(e){qTranslateConfig.onTabSwitchFunctions.push(e)},this.addLanguageSwitchBeforeListener=function(e){qTranslateConfig.onTabSwitchFunctionsSave.push(e)},this.delLanguageSwitchBeforeListener=function(e){for(var n=0;n<qTranslateConfig.onTabSwitchFunctionsSave.length;++n){var a=qTranslateConfig.onTabSwitchFunctionsSave[n];if(a==e)return void qTranslateConfig.onTabSwitchFunctionsSave.splice(n,1)}},this.addLanguageSwitchAfterListener=function(e){qTranslateConfig.onTabSwitchFunctionsLoad.push(e)},this.delLanguageSwitchAfterListener=function(e){for(var n=0;n<qTranslateConfig.onTabSwitchFunctionsLoad.length;++n){var a=qTranslateConfig.onTabSwitchFunctionsLoad[n];if(a==e)return void qTranslateConfig.onTabSwitchFunctionsLoad.splice(n,1)}},this.enableLanguageSwitchingButtons=function(e){var n=e?"block":"none";for(var a in qTranslateConfig.tabSwitches){for(var t=qTranslateConfig.tabSwitches[a],i=0;i<t.length;++i){var r=(t[i],t[i].parentElement);r.style.display=n;break}break}};var v=function(){for(var e=document.getElementsByClassName("wrap"),n=0;n<e.length;++n){var a=e[n],t=a.getElementsByTagName("form");if(t.length)return t[0]}var t=document.getElementsByTagName("form");if(1===t.length)return t[0];for(var n=0;n<t.length;++n){var i=t[n];if(e=i.getElementsByClassName("wrap"),e.length)return i}return null};if("function"==typeof e.addContentHooks&&e.addContentHooks(this),qTranslateConfig.page_config&&qTranslateConfig.page_config.forms&&d(qTranslateConfig.page_config.forms),u(jQuery),!o.length&&!l.length){var C=!1;for(var m in t){C=!0;break}if(!C)return}this.switchActiveLanguage=function(){var e=this,n=e.lang;if(!n)return void alert("qTranslate-X: This should not have happened: Please, report this incident to the developers: !lang");if(qTranslateConfig.activeLanguage!==n){if(qTranslateConfig.activeLanguage){for(var a=!0,t=qTranslateConfig.onTabSwitchFunctionsSave,i=0;i<t.length;++i){var r=t[i].call(qTranslateConfig.qtx,qTranslateConfig.activeLanguage,n);r===!1&&(a=!1)}if(!a)return;for(var o=qTranslateConfig.tabSwitches[qTranslateConfig.activeLanguage],i=0;i<o.length;++i)o[i].classList.remove(qTranslateConfig.lsb_style_active_class)}var s=qTranslateConfig.activeLanguage;qTranslateConfig.activeLanguage=n;for(var o=qTranslateConfig.tabSwitches[qTranslateConfig.activeLanguage],i=0;i<o.length;++i)o[i].classList.add(qTranslateConfig.lsb_style_active_class);for(var l=qTranslateConfig.onTabSwitchFunctions,i=0;i<l.length;++i)l[i].call(qTranslateConfig.qtx,n,s);for(var c=qTranslateConfig.onTabSwitchFunctionsLoad,i=0;i<c.length;++i)c[i].call(qTranslateConfig.qtx,n,s)}};var q=function(){var e=qtranxj_ce("ul",{className:qTranslateConfig.lsb_style_wrap_class}),n=qTranslateConfig.language_config;qTranslateConfig.tabSwitches||(qTranslateConfig.tabSwitches={});for(var a in n){var t=n[a],i=qTranslateConfig.flag_location,r=qtranxj_ce("li",{lang:a,className:"qtranxs-lang-switch",onclick:qTranslateConfig.qtx.switchActiveLanguage},e);qtranxj_ce("img",{src:i+t.flag},r),qtranxj_ce("span",{innerHTML:t.name},r),qTranslateConfig.activeLanguage==a&&r.classList.add(qTranslateConfig.lsb_style_active_class),qTranslateConfig.tabSwitches[a]||(qTranslateConfig.tabSwitches[a]=[]),qTranslateConfig.tabSwitches[a].push(r)}return e},T=function(e){var n=document.getElementById("qtranxs-meta-box-lsb");if(n){var a=n.getElementsByClassName("inside");if(a.length){n.className+=" closed",e(n).find(".hndle").remove();var t=document.createElement("span");n.insertBefore(t,a[0]),t.className="hndle ui-sortable-handle";var i=q();t.appendChild(i),e(function(e){e("#qtranxs-meta-box-lsb .hndle").unbind("click.postboxes")})}}};if(qTranslateConfig.LSB){h(),T(jQuery);var p=[];if(qTranslateConfig.page_config&&qTranslateConfig.page_config.anchors)for(var y in qTranslateConfig.page_config.anchors){var w=qTranslateConfig.page_config.anchors[y],b=document.getElementById(y);if(b)p.push({f:b,where:w.where});else if(w.jquery)for(var _=jQuery(w.jquery),k=0;k<_.length;++k){var b=_[k];p.push({f:b,where:w.where})}}if(!p.length){var b=e.langSwitchWrapAnchor;b||(b=v()),b&&p.push({f:b,where:"before"})}for(var k=0;k<p.length;++k){var w=p[k];if(!w.where||w.where.indexOf("before")>=0){var x=q();w.f.parentNode.insertBefore(x,w.f)}if(w.where&&w.where.indexOf("after")>=0){var x=q();w.f.parentNode.insertBefore(x,w.f.nextSibling)}if(w.where&&w.where.indexOf("first")>=0){var x=q();w.f.insertBefore(x,w.f.firstChild)}if(w.where&&w.where.indexOf("last")>=0){var x=q();w.f.insertBefore(x,null)}}this.addLanguageSwitchListener(g),e.onTabSwitch&&this.addLanguageSwitchListener(e.onTabSwitch)}};qTranslateConfig.js.get_qtx=function(){return qTranslateConfig.qtx||new qTranslateX(qTranslateConfig.js),qTranslateConfig.qtx},jQuery(document).ready(qTranslateConfig.js.get_qtx);jQuery(document).ready(function(e){var a,n,t,r,l,i,s,h,d=qTranslateConfig.js.get_qtx(),o=function(e,a){switch(qTranslateConfig.url_mode.toString()){case"1":e.search?e.search+="&lang="+a:e.search="?lang="+a;break;case"2":var n=qTranslateConfig.home_url_path,t=e.pathname;"/"!=t[0]&&(t="/"+t);var r=t.indexOf(n);r>=0&&(e.pathname=qTranslateConfig.homeinfo_path+a+t.substring(r+n.length-1));break;case"3":e.host=a+"."+e.host;break;case"4":e.host=qTranslateConfig.domains[a]}},c=function(d){if(!a){var c=document.getElementById("view-post-btn");if(!c||!c.children.length)return;if(a=c.children[0],"A"!=a.tagName)return;n=a.href,t=qtranxj_ce("a",{}),r=n.search(/\?/)>0}t.href=n,o(t,d),a.href=t.href;var g=document.getElementById("preview-action");if(g&&g.children.length&&(g.children[0].href=t.href),1!=qTranslateConfig.url_mode){if(!l){var f=document.getElementById("sample-permalink");f&&f.offsetHeight>0&&f.childNodes.length&&(l=f.childNodes[0],i=l.nodeValue)}l&&(t.href=i,o(t,d),l.nodeValue=t.href)}else h||(e("#sample-permalink").append('<span id="sample-permalink-lang-query"></span>'),h=e("#sample-permalink-lang-query")),h&&h.text((n.search(/\?/)<0?"/?lang=":"&lang=")+d);s||(s=document.getElementById("wp-admin-bar-view")),s&&s.children.length&&(s.children[0].href=a.href)},g=jQuery("#title"),f=jQuery("#title-prompt-text"),m=function(e){var a=g.attr("value");a?f.addClass("screen-reader-text"):f.removeClass("screen-reader-text")};d.addCustomContentHooks(),c(d.getActiveLanguage()),d.addLanguageSwitchAfterListener(c),f&&g&&d.addLanguageSwitchAfterListener(m)});qtrans_use = function(lang, text) { var result = qtranxj_split(text); return result[lang]; }
    //]]>
</script>
<?php

}
add_action('admin_footer', 'q_Conf',999);

function load_custom_wp_admin_style(){
	wp_enqueue_script( 'qtranslate-admin-options', '/wp-content/plugins/qtranslate-x/admin/js/options.min.js', array(),'1.0', false);
	wp_enqueue_script( 'qtranslate-admin-common', '/wp-content/plugins/qtranslate-x/admin/js/common.min.js', array(),'1.0', true);
}
add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );
}

function client_options_page() {
	global $aAdminCustomFields;

	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}

	$hidden_field_name = 'client_settings';

	$aInputs = array(
		array(
      'client_title', //key
      trans('client_title'), //value
      CLIENT_TITLE, //boolean
      false,
      true
      ),
		array(
			'client_email',
			trans('client_email'),
			CLIENT_EMAIL
			),
		array(
			'client_address',
			trans('client_address'),
			CLIENT_ADRESS
			),
		array(
			'client_phone',
			trans('client_phone'),
			CLIENT_PHONE
			),
		array(
			'client_fax',
			trans('client_fax'),
			CLIENT_FAX
			),
		array(
			'client_vat',
			trans('client_vat'),
			CLIENT_VAT
			),
		array(
			'client_code',
			trans('client_code'),
			CLIENT_CODE
			),
		array(
			'client_ga',
			trans('client_ga'),
			CLIENT_GA,
			'UA-29000000-1'
			)
		);

	if ($aAdminCustomFields) {
		foreach ($aAdminCustomFields as $key => $value) {
			$aInputs[] = $aAdminCustomFields[$key];
		}
	}
  //Social
	$social_inputs = array();
		?>
		<?php if( isset($_POST[ $hidden_field_name ]) && $_POST[ $hidden_field_name ] == 'Y' ) { ?>
		<?php
		if (isset($_POST[ 'client_address' ]) && $_POST[ 'client_address' ] != get_option('client_address')) {
			$aClientMapCoo = parse_map_info($_POST[ 'client_address' ]);
			$sClientMapCoo = implode(', ',(array)$aClientMapCoo);
			update_option( 'client_map_coord', $sClientMapCoo );
		}
		foreach ($aInputs as $aInput) {
			if (!$aInput[2]) {
				continue;
			}
          // Read their posted value
			$opt_val = $_POST[ $aInput[0] ];
          // Save the posted value in the database
			update_option( $aInput[0], htmlspecialchars(stripslashes($opt_val), ENT_QUOTES) );
		}
		if (CLIENT_SOCIAL) {
			$new_array = array();
			foreach ($social_inputs as $input_social) {
				if (!$input_social[2]) {
					continue;
				}
				$new_array[$input_social[0]] = $_POST[ $input_social[0] ];
            // var_dump($_POST[ $input_social[0] ]);
			}

			update_option( 'client_socials', serialize($new_array) );
		}
		?>
		<div class="updated">
			<p>
				<strong>
					<?php _e( 'Settings saved.'); ?>
				</strong>
			</p>
		</div>
		<?php } ?>
		<div class="wrap">
			<h2>
				<span class="dashicons dashicons-id"></span>
				<?php echo trans('client_admin_title'); ?>
			</h2>
			<p>
				<?php echo trans('client_admin_desc'); ?>
			</p>
			<form name="client_settings" method="post" action="">
				<div id="poststuff" class="postbox">
					<h2 class="hndle">
						<span>
							<?php echo trans('client_main'); ?>
						</span>
					</h2>
					<div class="inside">
						<table class="form-table">
							<tbody>
								<?php foreach ($aInputs as $aInput) { ?>
								<?php $opt_val = get_option( $aInput[0] ); ?>
								<?php 
								if (!$aInput[2]) {
									continue;
								}
								?>
								<tr class="user-first-name-wrap">
									<th>
										<label for="<?php echo $aInput[0]; ?>">
											<?php echo $aInput[1]; ?> 
										</label>
									</th>
									<td>
										<input type="text" name="<?php echo $aInput[0]; ?>" id="<?php echo $aInput[0]; ?>" <?php echo (isset($aInput[3]) ? 'placeholder="' . $aInput[3] . '"' : ''); ?> value="<?php echo $opt_val; ?>" class="regular-text<?php echo (isset($aInput[4]) && $aInput[4] ? ' i18n-multilingual' : ''); ?>">
									</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>  
					</div>
				</div>
		
				<input type="hidden" name="<?php echo $hidden_field_name; ?>" value="Y">
				<p class="submit">
					<input type="submit" name="Submit" class="button-primary" value="<?php esc_attr_e('Save Changes') ?>" />
				</p>
			</form>
		</div>
		<?php
	}