<?php
/*
  Template Name: Category
 */
//264 - produktai
//261 - paslaugos
//258 - sprendimai
$id = get_the_ID();
switch ($id) {
    case 258:
        $tax_name = 'solution_categories';
        $cat = get_categories(['taxonomy' => 'solution_categories']);
        $item = 'solutions';
        $l1 = ['url' => get_permalink(264), 'title' => get_the_title(264)];
        $l2 = ['url' => get_permalink(264), 'title' => get_the_title(264)];
         $img = get_theme_url('/assets/images/betonavimoapr.png');
         

        break;
    case 261:
        $tax_name = 'service_categories';
        $cat = get_categories(['taxonomy' => 'service_categories']);
        $l1 = ['url' => get_permalink(258), 'title' => get_the_title(258)];
        $l2 = ['url' => get_permalink(264), 'title' => get_the_title(264)];
        $item = 'services';
        break;
    case 264:
        $tax_name = 'products_categories';
        $cat = get_categories(['taxonomy' => 'products_categories']);
        $l1 = ['url' => get_permalink(258), 'title' => get_the_title(258)];
        $l2 = ['url' => get_permalink(261), 'title' => get_the_title(261)];
        $item = 'products';
        $img = get_theme_url('/assets/images/elektrosprodukt.png');
        
        break;
}
$select = (!empty($_GET['select'])) ? $_GET['select'] : 'noop';
?>
<?php get_header(); ?>
<?php get_part('menu-section'); ?> 

<div class="page-container container-fluid" style="background-image: url(<?php echo $img; ?>)">
    <div class="col-md-offset-2 col-md-8">         
        <div class="info-holder">
            <h1><?php the_title(); ?></h1>
            <div class="desc">
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                        <?php the_content(); ?>
                        <?php
                    endwhile;
                endif;
                ?>
            </div>
        </div>
        <section class="page-content category">

            <div class="controls">
                
                <div class="fr">
                    <a href="<?php echo $l1['url']; ?>" class="next"><?php echo $l1['title']; ?> <span></span></a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="categoryies-holder">
                <?php
                if (!empty($cat)):

                    foreach ($cat as $cat_item):
                        ?>
                        <div class="category-item" data-id="<?php echo $cat_item->category_nicename ?>">
                            <div class="item-img">
                                <img class="img-responsive" src="<?php echo get_field('sub_image', $tax_name . '_' . $cat_item->term_id)['url'] ?>" alt="">
                            </div>
                            <div class="item-name"><?php echo $cat_item->name ?></div>
                        </div>
                        <?php
                    endforeach;
                endif;
                ?>
                <div class="clearfix"></div>
            </div>

            <?php
            if (!empty($cat)):

                $i = 0;
                foreach ($cat as $cat_item):
                    ?>

                    <div class="subcategory" id="<?php echo $cat_item->category_nicename ?>">
                        <div class="container-fluid no-padd">
                            <?php
                            //print_r($cat_item);
                            $args = array(
                                'posts_per_page' => -1,
                                'post_type' => $item,
                                'tax_query' => array(
                                    array(
                                        'taxonomy' => $tax_name,
                                        'field' => 'term_id',
                                        'terms' => $cat_item->term_id,
                                    )
                                )
                            );
                            $postslist = get_posts($args);
                            if (!empty($postslist)):
                                foreach ($postslist as $post_item):

                                    $info = get_field('product_specifications', $post_item->ID);
                                    $images = get_field('item_photos', $post_item->ID);
                                    $image = !empty($images)?reset($images)['item_photo']:'';
                                    ?>
                                    <div class="col-lg-6 col-md-12 no-padd">
                                        <div class="subholder">
                                            <div class="container-fluid no-padd">
                                                <div class="col-lg-6 col-md-12 no-padd">
                                                    <div class="subitem">
                                                        <a href="<?php  echo get_permalink($post_item->ID); ?>" class="sublabel">
                                                            <?php echo $post_item->post_title; ?>
                                                        </a>
                                                        <?php if($tax_name !== 'products_categories'): ?>
                                                        <div class="subdesc"> 
                                                            <?php
                                                            if (!empty($info['privalumai'])) {
                                                                $t_title = strip_tags($info['privalumai']);
                                                                $pos = strpos($t_title, ' ', 60);
                                                                echo substr($t_title, 0, $pos);
                                                            }
                                                            ?>
                                                        </div>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-12 no-padd"> 
                                                    <a href="<?php  echo get_permalink($post_item->ID); ?>"><div class="subimg" style="background-image: url(<?php echo $image; ?>)">
                                                    </div> 
                                                        </a>     
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                endforeach;
                            endif;
                            ?>

                        </div>
                    </div>  
                    <?php
                    $i++;
                endforeach;
            endif;
            ?>

        </section>
    </div>
</div>
<script>
var make_click = '<?php echo $select; ?>';
$('#' + make_click).fadeIn();
</script>

<?php get_footer(); ?>