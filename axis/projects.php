<?php
// let's create the function for the custom type
function projects_cpt() {
	// creating (registering) the custom type
	register_post_type( 'projects', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		[
			'labels'              => [
				'name'               => trans('projects_tab'),
				/* This is the Title of the Group */
				'singular_name'      => __( 'Project', 'axis' ),
				/* This is the individual type */
				'all_items'          => __( 'All projects', 'axis' ),
				/* the all items menu item */
				'add_new'            => __( 'Add New', 'axis' ),
				/* The add new menu item */
				'add_new_item'       => __( 'Add New project', 'axis' ),
				/* Add New Display Title */
				'edit'               => __( 'Edit', 'axis' ),
				/* Edit Dialog */
				'edit_item'          => __( 'Edit Post Types', 'axis' ),
				/* Edit Display Title */
				'new_item'           => __( 'New Post Type', 'axis' ),
				/* New Display Title */
				'view_item'          => __( 'View Post Type', 'axis' ),
				/* View Display Title */
				'search_items'       => __( 'Search Post Type', 'axis' ),
				/* Search Custom Type Title */
				'not_found'          => __( 'Nothing found in the Database.', 'axis' ),
				/* This displays if there are no entries yet */
				'not_found_in_trash' => __( 'Nothing found in Trash', 'axis' ),
				/* This displays if there is nothing in the trash */
				'parent_item_colon'  => ''
			],
			/* end of arrays */
			'description'         => __( 'projects', 'axis' ),
			/* Custom Type Description */
			'public'              => true,
			'publicly_queryable'  => true,
			'exclude_from_search' => false,
			'show_ui'             => true,
			'query_var'           => true,
			'menu_position'       => 6,
			/* this is what order you want it to appear in on the left hand side menu */
			'menu_icon'           => 'dashicons-location-alt',
			/* the icon for the Game type menu */
			'rewrite'             => [ 'slug' => 'projects', 'with_front' => false ],
			/* you can specify its url slug */
			'has_archive'         => 'projects',
			/* you can rename the slug here */
			'capability_type'     => 'post',
			'hierarchical'        => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports'            => [ 'title', 'editor', 'thumbnail', 'template']
		] /* end of options */
	); /* end of register post type */


}

// adding the function to the Wordpress init
add_action( 'init', 'projects_cpt' );
?>
