<?php get_header(); ?>
<div class="page-container container">
  <?php echo breadcrumbs(); ?>
  <?php $iThisID = get_the_ID(); ?>
  <?php if (have_posts()) { ?>
    <?php while (have_posts()) { ?>
      <?php the_post(); ?>
      <article <?php post_class(); ?>>
        <h1>
          <?php echo get_the_title(); ?>
        </h1>
        <?php echo get_the_content(); ?>
        <?php 
          $iImgID = get_post_meta( $iThisID, '_gallery_images', true );
          $aImages = get_posts( array(               
            "showposts"       =>  -1,
            "what_to_show"    =>  "posts",
            "post_status"     =>  "inherit",
            "post_type"       =>  "attachment",
            "post_mime_type"  =>  "image/jpeg,image/gif,image/jpg,image/png",
            'post__in'        => explode( ',', $iImgID ),
            'order'           => 'ASC',
            'meta_key'        => '_gallery_order_' . $iThisID,
            'orderby'         => 'meta_value_num'
          )); 
        ?>
        <div class="gallery-items row">
          <?php foreach ($aImages as $aImage) { ?>
            <?php $aImgMeta = get_post_meta( $aImage -> ID ); ?>
            <div class="col-md-2 col-sm-2">
              <article class="gallery-item">
                <img class="gallery-item-image" src="<?php echo get_thumb_url($aImage -> ID, 'small_news_thumb'); ?>" alt="<?php echo get_the_title($aImage -> ID); ?>">
                <h1 class="gallery-item-title">
                  <?php echo title_filter($aImgMeta['gallery_image_text'][0]); ?>
                </h1>
              </article>
            </div>
          <?php } ?>
        </div>
      </article>
    <?php } ?>
  <?php } ?>
</div>
<?php
get_footer();