<?php get_header(); ?>
<?php get_part('menu-section'); ?>

<div class="page-container container-fluid">
    <div class="col-md-offset-2 col-md-8">       
        <?php echo breadcrumbs(); ?>
        <?php if (have_posts()) { ?>
            <?php while (have_posts()) { ?>
                <?php the_post(); ?>
                <?php
                    $prevpost = get_adjacent_post(false, '', true, 'category');
                    $url = get_permalink($prevpost);
                ?>
                <div class="info-holder news">

                    <h1 class="desc"><?php echo get_the_title(); ?>
                    </h1>

                    <div class="date">
        <?php echo get_the_date('Y F j'); ?> 
                    </div>
                </div>
                <section class="page-content">
                    <div class="controls">
                        <div class="fl"><div class="all-news"><img src="<?php echo get_theme_url('/assets/images/dots.png') ?>" alt=""></div>
                            <a href="<?php echo get_permalink(65); ?>" class="news-list"><?php echo trans('toNews'); ?></a>
                        </div>
                        <?php if($url !== get_permalink()): ?>
                        <div class="fr">
                            <a href="<?php echo $url; ?>" class="next"><?php echo trans('nextNews') ?></a>
                            <div class="all-news">&#xe60a</div>
                            <?php endif; ?>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="post-content">
                        <div class="post-img">
                            <img class="img-responsive" src="<?php echo get_the_post_thumbnail_url(); ?>" alt="post-title"/>
                        </div>
                        <article>
        <?php echo nl2br(get_the_content()); ?>
                        </article>
                    </div>
                </section>
            <?php } ?>
<?php } ?>
    </div>
</div>
<?php
get_footer();
