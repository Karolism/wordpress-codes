<?php get_header(); ?>
<div class="container">
  <h2>
    404: Puslapis nerastas
  </h2>
  <div class="not-found">
    <strong>
      404
    </strong>
    <p>
      Page not found
    </p>
  </div>
</div>
<?php
get_footer();