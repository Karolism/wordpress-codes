<?php /* Template Name: Home */ ?>
<?php get_header(); ?>
<?php get_part('menu-section'); ?> 
<?php get_part('slider-section'); ?> 
<?php get_part('home-news'); ?>
<?php get_part('bio-widget'); ?>
<?php get_part('categories-widget'); ?>
<?php get_part('google-maps'); ?>
<script>
   $(window).scroll(function(){
       var s = $(document).scrollTop();
       if(s>0){
           $('nav.front-page').css("background-color", "rgba(0,0,0,1)");
       } else {
           $('nav.front-page').css("background-color", "rgba(0,0,0,0.5)");
       }
   });
</script>
<!-- Wordpress footer -->
<?php wp_footer(); ?>
<?php get_footer(); ?>