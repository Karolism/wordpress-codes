<?php /* Template Name: News */ ?>
<?php get_header(); ?>
<?php get_part('menu-section'); ?>

<?php
global $post;
$offset = isset($_GET['offset']) ? $_GET['offset'] : 0;
$count_posts = wp_count_posts()->publish;
$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
$wpb_all_query = new WP_Query('cat=1&paged=' . $paged);
?>
<div class="page-container container-fluid">

    <section class="index-news all">
        <div class="container-fluid">
            <h2><?php echo trans('newsSectionTitle'); ?></h2>
            <div class="col-md-10 col-md-offset-1 no-padd">
                <div class="col-md-12 no-padd">
                    <?php
                    $i = 0;
                    if ($wpb_all_query->have_posts()):
                        ?>
                        <?php
                        while ($wpb_all_query->have_posts()):
                            $wpb_all_query->the_post();
                            ?>
                            <div class="col-md-<?php if ($i % 5 == 0) {
                                echo '8';
                            } else {
                                echo '4';
                            } ?> no-padd">
                                <a href="<?php the_permalink(); ?>" class="news-article a1" style="background-image: url(<?php
                                    the_post_thumbnail_url();
                                    ?>)">
                                    <div class="content">
                                        <div class="date">
                                                <?php echo get_the_date('Y F j'); ?>
                                        </div>
                                        <div class="title">
                            <?php  wp_trim_words(the_title(), 10, "..."); ?>
                                        </div>
                                    </div>
                                </a>
                            </div>              
        <?php
        $i++;
    endwhile;
    ?>
                        <?php echo pagination($wpb_all_query->max_num_pages); ?>
                    </div>
                </div>
            </div>

            <?php
            wp_reset_postdata();
            wp_reset_query();
        else:
            ?>
            <p><?php echo trans('no_results');?></p>
<?php endif; ?>
    </section>

</div>
<?php get_footer(); ?>