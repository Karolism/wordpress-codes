<?php

// let's create the function for the custom type
function products_cpt() {
    // creating (registering) the custom type
    register_post_type('products', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
            // let's now add all the options for this post type
            [
        'labels' => [
            'name' => trans('products_tab'),
            /* This is the Title of the Group */
            'singular_name' => __('Products', 'axis'),
            /* This is the individual type */
            'all_items' => __('All products', 'axis'),
            /* the all items menu item */
            'add_new' => __('Add New', 'axis'),
            /* The add new menu item */
            'add_new_item' => __('Add New project', 'axis'),
            /* Add New Display Title */
            'edit' => __('Edit', 'axis'),
            /* Edit Dialog */
            'edit_item' => __('Edit Post Types', 'axis'),
            /* Edit Display Title */
            'new_item' => __('New Post Type', 'axis'),
            /* New Display Title */
            'view_item' => __('View Post Type', 'axis'),
            /* View Display Title */
            'search_items' => __('Search Post Type', 'axis'),
            /* Search Custom Type Title */
            'not_found' => __('Nothing found in the Database.', 'axis'),
            /* This displays if there are no entries yet */
            'not_found_in_trash' => __('Nothing found in Trash', 'axis'),
            /* This displays if there is nothing in the trash */
            'parent_item_colon' => ''
        ],
        /* end of arrays */
        'description' => __('products', 'axis'),
        /* Custom Type Description */
        'public' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'show_ui' => true,
        'query_var' => true,
        'show_in_nav_menus' => false,
        'menu_position' => 7,
        /* this is what order you want it to appear in on the left hand side menu */
        'menu_icon' => 'dashicons-products',
        /* the icon for the Game type menu */
        'rewrite' => true,
        /* you can specify its url slug */
        'has_archive' => 'products',
        /* you can rename the slug here */
        'capability_type' => 'post',
        'hierarchical' => false,
        /* the next one is important, it tells what's enabled in the post editor */
        'supports' => ['title', 'editor', 'thumbnail', 'post_tag'],
        'taxonomies' => ['products_categories', 'post_tag'],
            ] /* end of options */
    ); /* end of register post type */
}

// adding the function to the Wordpress init
add_action('init', 'products_cpt');

function project_categories_init() {
    // create a new taxonomy
    register_taxonomy(
            'products_categories', 'products', array(
        'label' => __('Categories'),
        'rewrite' => array('slug' => 'products_categories'),
        'public' => true,
        'show_admin_column' => true,
        'show_ui' => true,
        'hierarchical' => true,
            )
    );
}

add_action('init', 'project_categories_init');
?>
