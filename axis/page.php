<?php get_header(); ?>
<?php get_part('menu-section'); ?> 
<?php $img = get_field('bg_img');?>
<div class="page-container container-fluid" style="background-image: url(<?php echo $img; ?>)">

    <section class="index-news all" >
        <div class="container-fluid">
            <h2><?php echo get_the_title(); ?></h2>
            <div class="col-md-10 col-md-offset-1 no-padd">
                <div class="col-md-10 col-md-offset-1 col-xs-offset-0 no-padd">
                    <?php echo breadcrumbs(); ?>
                    <?php if (have_posts()) { ?>
                        <?php while (have_posts()) { ?>
                            <?php the_post(); ?>
                            <article <?php post_class(); ?>>
                                <?php echo content(); ?>
                            </article>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
</div>
<?php
get_footer();

