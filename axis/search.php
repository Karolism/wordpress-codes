<?php
/*
Template Name: Search Page
*/  
?>
<?php get_header(); ?>
<?php get_part('menu-section'); ?> 
<div class="page-container container-fluid" style="background-image: url(<?php echo $img; ?>)">

    <section class="index-news all" >
        <div class="container">
            <?php
$s=(isset($_POST['search_text']))?$_POST['search_text']:'';
$args = array(
                's' =>$s
            );
    // The Query
$the_query = new WP_Query( $args );
if ( $the_query->have_posts() ) {
        _e("<h2 style='font-weight:bold;color:#000'>".trans("paieskos_rezultatai").": ".$s."</h2>");
        echo ' <div class="row">';
        while ( $the_query->have_posts() ) {
           $the_query->the_post();
                 ?>

                    <div class="col-md-4">
                        <div class="search-item-holder">
                            <div class="date"><?php echo get_the_date('Y F j'); ?></div>

                            <h3><?php the_title(); ?></h3>
                            
                            <a href="<?php the_permalink(); ?>" class="btn"><?php echo trans('more'); ?></a>
                        </div>
                    </div>

                   
                 <?php
        }
        echo '</div>';
    }else{
?>
            <h2 style='font-weight:bold;color:#000'><?php echo trans('no_results');?></h2>

<?php } ?>
        </div>
    </section>
</div>
<?php wp_footer(); ?>
<?php get_footer();