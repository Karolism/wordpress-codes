<?php /* Template Name: Contacts */ ?>

<?php get_header(); ?>
<?php get_part('menu-section'); ?> 

<?php
$contacts_title = get_field('page_title');
$contacts_description = get_field('page_description');
$main_contacts = get_field('main_contacts');
?>

<div class="page-container container-fluid" style="background-image: url(<?php echo get_theme_url('/assets/images/bg-contact.png'); ?>)">
    <div class="col-md-offset-2 col-md-8">         
        <div class="info-holder">
            <h1><?php echo $contacts_title; ?></h1>
            <div class="desc">
                <?php echo $contacts_description; ?>
            </div>
        </div>
        <section class="page-content">
            <div class="main-contacts">
                <?php echo $main_contacts ?>
                <div class="social">
                    <?php
                    $fb = get_field('facebook', 'option');
                    $linkedin = get_field('linkedin', 'option');
                    if ($linkedin):
                        ?>

                        <a href="<?php echo $linkedin; ?>" target="_blank"><i class="fa fa-linkedin-square"></i></a>

                    <?php endif; ?>
                    <?php if ($fb): ?>

                        <a href="<?php echo $fb; ?>" target="_blank"><i class="fa fa-facebook-official"></i></a>

                    <?php endif; ?>              
                </div>

            </div>
            <div class="live">
                <div class="live-text">
                    <?php echo trans('have_q'); ?>
                    <br>
                    <button class="btn write" data-toggle="modal" data-target="#modal-contact"><?php echo trans('send_request'); ?></button>
                </div>
            </div>
            <div class="clearfix"></div>

            <hr>
            <div class="container-fluid no-padd all-c">
                <div class="row ">
                    <?php if (have_rows('contacts_repeater')): ?>
                        <?php while (have_rows('contacts_repeater')): the_row();
                            ?>
                            <div class="col-md-4 col-sm-6">
                                <div class="contact-items">
                                    <?php the_sub_field('contact_address'); ?>
                                </div>
                            </div>
                            <?php
                        endwhile;
                    endif;
                    ?>
                </div>
            </div>
        </section>
    </div>
</div>

    <div class="modal fade modal-conatct" id="modal-contact" tabindex="-1" role="dialog" aria-labelledby="modalProfile" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" type="button" data-dismiss="modal"></button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="col-md-offset-3 col-md-6">
                            <div class="heading mail"><?php echo trans('contact_with_us') ?></div>
                            <?php
                            include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
                            ?>
                            <form method="post" action="<?php echo admin_url("admin-ajax.php"); ?>" class="contact-form">

                                <input type="text" name="name" class="form-field" placeholder="<?php echo trans('form_name'); ?>" required="true">
                                <input type="email" class="form-field field-email" name="email" placeholder="<?php echo trans('client_email'); ?>" required="true">
                                <input type="number" class="form-field field-phone" name="phone" placeholder="<?php echo trans('form_phone'); ?>" required="true">
                                <textarea class="form-field"  name="message" placeholder="<?php echo trans('form_message') ?>"></textarea>
                                <div class="form-results"></div>
                                <button type="submit" name="form-submit" value="1" class="submit-form btn write"><?php echo trans('field_submit') ?></button>


                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


<!-- Wordpress footer -->
<?php wp_footer(); ?>
<?php get_footer(); ?>