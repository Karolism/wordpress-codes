<?php /* Template Name: Projects */ ?>
<?php get_header(); ?>
<?php get_part('menu-section'); ?> 
<?php
$projects_title = get_field('page_title');
$projects_description = get_field('page_description');
$count_posts = wp_count_posts('projects')->publish;
$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
$wpb_all_query = new WP_Query('post_type=projects&paged=' . $paged);
?>

<div class="page-container container-fluid" style="background-image: url(<?php echo get_theme_url('/assets/images/bg-project.png'); ?>)">
    <div class="col-md-offset-2 col-md-8">         
        <div class="info-holder">
            <h1><?php echo $projects_title; ?></h1>
            <div class="desc">
                <?php echo $projects_description; ?>
            </div>
        </div>
    </div>
    <div class="col-md-offset-2 col-md-8 project-holder">
        <div class="conainer-fluid">
            <?php
            $i = 0;
            if ($wpb_all_query->have_posts()):
                ?>
                <?php
                while ($wpb_all_query->have_posts()):
                    $wpb_all_query->the_post();
                    $spec = get_field('specifikacijos', get_the_ID());
                    $images = get_field('project_photos', get_the_ID());
                    $logo = get_field('produkto_logo', get_the_ID());
                    ?>
                    <?php if ($i % 2 == 0): ?>
                        <div class="project">
                            <div class="col-md-7 no-padd">
                                <div class="container-fluid">

                                    <div class="col-md-5">
                                        <div class="p-img-holder" style="background-image: url(<?php echo $logo; ?>)">
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="p-info">
                                            <div class="p-inner">
                                                <?php
                                                ?>
                                                <div class="p-label"><?php the_title(); ?></div>
                                                <div class="p-text"><?php echo $spec['project_place'] . ' ' . $spec['project_power']; ?></div>
                                                <a href="<?php the_permalink(); ?>"><?php echo trans('watch_project'); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5 no-padd">
                                <div class="p-img-holder pix" style="background-image: url(<?php echo the_post_thumbnail_url(); ?>)">

                                </div>
                            </div>    
                        </div>
                    <?php else: ?>

                        <div class="project">
                            <div class="col-md-5 no-padd">
                                <?php $images = get_field('project_photos', get_the_ID()); ?>
                                <div class="p-img-holder pix" style="background-image: url(<?php echo the_post_thumbnail_url(); ?>)">
                                </div>
                            </div>    
                            <div class="col-md-7 no-padd">
                                <div class="container-fluid">
                                    <div class="col-md-5">
                                        <div class="p-img-holder" style="background-image: url(<?php echo $logo; ?>)">

                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="p-info">
                                            <div class="p-inner">
                                                <?php
                                                $spec = get_field('specifikacijos', get_the_ID());
                                                ?>
                                                <div class="p-label"><?php the_title(); ?></div>
                                                <div class="p-text"><?php echo $spec['project_place'] . ' ' . $spec['project_power']; ?></div>
                                                <a href="<?php the_permalink(); ?>"><?php echo trans('watch_project'); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    <?php
                    endif;
                    $i++;
                endwhile;

                wp_reset_postdata();
                wp_reset_query();
            endif;
            ?>

            <?php echo pagination($wpb_all_query->max_num_pages); ?>
        </div>
    </div>

</div>

<?php get_footer(); ?>