<?php get_header(); ?>
<?php get_part('menu-section'); ?> 
<?php
$about = get_field('about_product');
$info = get_field('product_specifications');
$prevpost = get_adjacent_post(false, '', true, 'category');
$url = get_permalink($prevpost);

$photo = get_field('career_photo');
$person_name = get_field('career_person_name');
$person_position = get_field('career_status');
$phone = get_field('career_phone');
$email = get_field('career_email');
$career_mini = get_field('career_mini');
$is_ac = false;
if(empty($info['attr1']) && empty($info['attr2']) && empty($info['attr3']) && empty($info['attr4']) && empty($info['attr5']) && empty($info['attr6']) && empty($info['attr7']) && empty($info['attr8'])){
    $is_ac = true;
}
?>

<div class="page-container container-fluid">
    <div class="col-md-offset-2 col-md-8">         
        <div class="info-holder">
            <h1 class="preline"><?php the_title(); ?></h1>
            <div class="small-cat">
                <a href="<?php echo get_permalink(264); ?>"><?php
                    $terms = get_terms('products_categories');
                    $subcat = reset($terms);
                    if (!empty($subcat->name)) {
                        echo $subcat->name;
                    }
                    ?></a>
            </div>
        </div>
        <section class="page-content m53 inner">
            <div class="controls">
                <div class="fl"><div class="all-news"><img src="<?php echo get_theme_url('/assets/images/dots.png') ?>" alt=""></div>
                    <a href="<?php echo get_permalink(264); ?>" class="news-list"><?php echo trans('toProduct') ?></a>
                </div>
                 <?php if($url !== get_permalink(264)): ?>
                <div class="fr">
                    <a href="<?php echo $url ?>" class="next"><?php echo trans('nextProduct') ?></a>
                    <div class="all-news">&#xe60a</div>
                </div>
                <?php endif; ?>
                <div class="clearfix"></div>
            </div>

            <div class="product">
                <div class="fl">
                    <div class="owl-carousel" id="product-slider">
                        <?php
                        if (have_rows('item_photos')):
                            while (have_rows('item_photos')): the_row();
                                ?>
                                <div class="item" style="background-image: url(<?php echo get_sub_field('item_photo') ?>)"><img class="img-responsive" src="<?php echo get_sub_field('item_photo') ?>" alt=""></div>
                                <?php
                            endwhile;
                        endif;
                        ?>
                    </div>
                </div>
                <div class="fr">
                    <ul class="bread">     
                        <?php
                        $tags = get_the_tags();
                        if($tags){
                        foreach ($tags as $tag):
                            ?>
                            <li><a href=""><?php echo $tag->name; ?></a></li>
                        <?php endforeach;
                        }?>
                    </ul>
                    <div class="product-description">
                        <?php echo $about['product_description']; ?>
                    </div>
                    <ul class="product-files">
                        <?php
                        if (!empty($about['product_files'])):
                            foreach ($about['product_files'] as $file):
                                ?>
                                <li>
                                    <a href="<?php echo $file['product_file']['url']; ?>" target="_blank" download>
                                        <div class="file" style="background-image: url(<?php echo get_theme_url('/assets/images/pdf.png') ?>)"></div>
                                        <div class="file-name"><?php echo $file['product_file']['title']; ?></div>
                                        <div class="download"><?php echo trans('download'); ?></div>
                                    </a>
                                </li>
                                <?php
                            endforeach;
                        endif;
                        ?>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
        </section>

         <section class="page-content details">
             <?php
             
             $priv = true;
                    $siv=true;
                    if(!empty($info['privalumai'])){
                        $priv = true;
                    } else {
                        $priv = false;
                    }
                    if(!empty($info['attr1']) || !empty($info['attr2']) || !empty($info['attr3']) || !empty($info['attr4']) || !empty($info['attr5']) || !empty($info['attr6']) || !empty($info['attr7']) || !empty($info['attr8'])){
                        $siv = true;
                    } else {
                        $siv = false;
                    }
             ?>
             
<?php if($priv || $siv): ?>
            <div class="tabs-holder">
                <div class="tabs">
                    <?php
                    if($priv == true): ?>
                    <div class="tab<?php if($is_ac == true){ echo ' active'; } ?>" data-id="privalumai"><div class="tab-item"><div class="tab-img"></div><div><?php echo trans('advantages'); ?></div></div></div>
                   <?php
                   endif;
                   if($siv == true): ?>
                    <div class="tab active" data-id="specifikacija"><div class="tab-item"><div class="tab-img"></div><div><?php echo trans('specification'); ?></div></div></div>
                <?php
                endif; ?>
                </div>
            </div>
            
            <div class="content-holder">
                <div class="details-content" id="specifikacija">
                    <div class="container-fluid no-padd">
                        <div class="row no-padd">
                            <?php
                            $tracker = 0;
                            for ($i = 1; $i <= 8; $i++):
                                ?>
                                <?php if (!empty($info['attr' . $i])): ?>
                                    <div class="col-lg-3 col-md-6 col-sm-12 no-padd">
                                        <div class="detail-info">
                                            <div class="detail-label"><?php echo trans('attr' . $i); ?></div>
                                            <div class="detail-parameter"><?php echo $info['attr' . $i]; ?></div>
                                        </div>
                                    </div>
                                    <?php
                                    $tracker++;
                                endif;
                                if ($tracker % 4 == 0 && $i != 8 && !empty($info['attr' . $i])) {
                                    echo '<div class="col-md-12 no-padd">
                                                            <hr>
                                                        </div>';
                                }
                            endfor;
                            ?> 
                        </div>
                    </div>
                </div>
                <div class="details-content<?php if($siv == false){ echo ' active'; } ?>" id="privalumai">
                    <?php echo $info['privalumai']; ?>
                </div>
            </div>

        </section>
        <?php endif; ?>
        <?php if (!empty($email)): ?>
        <div class="manager-block">
            <div class="manager-holder">
                <div class="person-holder">
                    <div class="person-img">
                        <?php if(!empty($photo)): ?><img class="img-responsive" src="<?php echo $photo; ?>" alt="person"><?php endif; ?>
                    </div>
                    <div class="person-info">
                        <div class="person-name"><?php echo $person_name; ?></div>
                        <div class="person-status"><?php echo $person_position; ?></div>
                        <div class="person-contact"><?php echo $phone; ?> <br>
                            <?php echo $email; ?></div>
                    </div>
                </div>
                <div class="quote">
                </div>
                <div class="btn write" data-toggle="modal" data-target=".modal-profile-lg"><?php echo trans('send_request'); ?></div>
            </div>
        <?php endif; ?>
    </div>
</div>

<?php if (!empty($email)): ?>
    <div class="modal fade modal-conatct" id="modal-contact" tabindex="-1" role="dialog" aria-labelledby="modalProfile" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" type="button" data-dismiss="modal"></button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="col-md-offset-3 col-md-6">
                            <div class="heading mail"><?php echo trans('contact_with_us') ?></div>
                            <?php
                            include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
                            ?>
                            <form method="post" action="<?php echo admin_url("admin-ajax.php"); ?>" class="contact-form">

                                <input type="text" name="name" class="form-field" placeholder="<?php echo trans('form_name'); ?>" required="true">
                                <input type="email" class="form-field field-email" name="email" placeholder="<?php echo trans('client_email'); ?>" required="true">
                                <input type="number" class="form-field field-phone" name="phone" placeholder="<?php echo trans('form_phone'); ?>" required="true">
                                <textarea class="form-field"  name="message" placeholder="<?php echo trans('form_message') ?>"></textarea>
                                <input type="hidden" name="sendTo" class="form-field" hidden="" value="<?php echo $email ?>">
                                <div class="form-results"></div>
                                <button type="submit" name="form-submit" value="1" class="submit-form btn write"><?php echo trans('field_submit') ?></button>


                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
<?php endif; ?>

<script>
    var owl = $(".owl-carousel");
    owl.owlCarousel({
        loop: false,
        items: 1,
        margin: 0,
        stagePadding: 0,
        autoplay: false,
        callbacks: true,
        dots: true,
        nav: false,
        responsive: {
            0: {
                nav: false,
            },
            768: {
                nav: false,
            }

        }
    });

    dotcount = 1;

    jQuery('.owl-dot').each(function () {
        jQuery(this).addClass('dotnumber' + dotcount);
        jQuery(this).attr('data-info', dotcount);
        dotcount = dotcount + 1;
    });

    slidecount = 1;

    jQuery('.owl-item').not('.cloned').each(function () {
        jQuery(this).addClass('slidenumber' + slidecount);
        slidecount = slidecount + 1;
    });

    jQuery('.owl-dot').each(function () {
        grab = jQuery(this).data('info');
        slidegrab = jQuery('.slidenumber' + grab + ' img').attr('src');
        jQuery(this).css("background-image", "url(" + slidegrab + ")");
    });

    amount = $('.owl-dot').length;
    gotowidth = 77;
    maxscroll = amount * gotowidth - gotowidth;
    $(".godown").on("click", function () {
        $(".owl-dots").animate({
            scrollTop: gotowidth
        });

    });
    $(".goup").on("click", function () {
        $(".owl-dots").animate({
            scrollTop: -gotowidth
        });

    });
    if (amount > 7) {
        $('.owl-carousel .owl-dots').css('display', 'block');
    }
    if (amount <= 7) {
        $(".godown").addClass("disabled");
        $(".goup").addClass("disabled");
    }
</script>
<!-- Wordpress footer -->
<?php wp_footer(); ?>
<?php get_footer(); ?>