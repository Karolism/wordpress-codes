
<?php
$bio_title = get_field('bio_title');
$bio_text = get_field('bio');
$bio_image = get_field("bio_paveikslas");
?>
<section id="biofuel" style="background-image: url(<?php echo $bio_image; ?>)">
    <h2><?php echo strip_tags($bio_title); ?></h2>
    <div class="text underline">
        <?php echo strip_tags($bio_text); ?>
    </div>
    <div class="arrow"></div>
</section>