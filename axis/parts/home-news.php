<?php
$recent_posts = new WP_Query('cat=1&posts=5');

$recent_postsz = $recent_posts->posts;
$recent_posts = array();
for($i = 0; $i < count($recent_postsz); $i++){
    array_push($recent_posts, (array) $recent_postsz[$i]);
}
?>

<section class="index-news">
    <div class="container-fluid">
        <h2><?php echo trans("newsSectionTitle"); ?></h2>
        <div class="col-md-6 col-md-offset-1 no-padd">
            <div class="col-md-12 no-padd">
                <?php if (isset($recent_posts[0])): ?>
                    <a href="<?php the_permalink($recent_posts[0]['ID']) ?>" class="news-article a1" style="background-image: url(<?php
                    $recent_post_image = wp_get_attachment_image_src(get_post_thumbnail_id($recent_posts[0]['ID']), 'full');
                    echo $recent_post_image[0];
                    ?>)">
                        <div class="content">
                            <div class="date">
                                <?php echo get_the_date('Y F j', $recent_posts[0]['ID']); ?>
                            </div>
                            <div class="title">
                                <?php echo $recent_posts[0]['post_title']; ?>
                            </div>
                        </div>
                    </a>
                <?php endif; ?>
                <?php if (isset($recent_posts[1])): ?>
                    <div class="col-md-6 no-padd">
                        <a href="<?php echo $recent_posts[1]['guid'] ?>" class="news-article a2" style="background-image: url(
                        <?php
                        $recent_post_image = wp_get_attachment_image_src(get_post_thumbnail_id($recent_posts[1]['ID']), 'full');
                        echo $recent_post_image[0];
                        ?>)">
                            <div class="content">
                                <div class="date">
                                    <?php echo get_the_date('Y F j', $recent_posts[1]['ID']); ?>
                                </div>
                                <div class="title">
                                    <?php echo $recent_posts[1]['post_title']; ?>
                                </div>
                            </div>
                        </a>
                    </div>
                <?php endif; ?>
                <?php if (isset($recent_posts[2])): ?>
                    <div class="col-md-6 no-padd">
                        <a href="<?php echo $recent_posts[2]['guid'] ?>" class="news-article a2" style="background-image: url(
                        <?php
                        $recent_post_image = wp_get_attachment_image_src(get_post_thumbnail_id($recent_posts[2]['ID']), 'full');
                        echo $recent_post_image[0];
                        ?>)">
                            <div class="content">
                                <div class="date">
                                    <?php echo get_the_date('Y F j', $recent_posts[2]['ID']); ?>
                                </div>
                                <div class="title">
                                    <?php echo $recent_posts[2]['post_title']; ?>
                                </div>
                            </div>
                        </a>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <?php if (isset($recent_posts[3])): ?>
            <div class="col-md-4 no-padd">
                <a href="<?php echo $recent_posts[3]['guid'] ?>" class="news-article a3" style="background-image: url(
                <?php
                $recent_post_image = wp_get_attachment_image_src(get_post_thumbnail_id($recent_posts[3]['ID']), 'full');
                echo $recent_post_image[0];
                ?>)">
                    <div class="content">
                        <div class="date">
                            <?php echo get_the_date('Y F j', $recent_posts[3]['ID']); ?>
                        </div>
                        <div class="title">
                            <?php echo $recent_posts[3]['post_title']; ?>
                        </div>
                    </div>
                </a>
                <a href="<?php echo get_permalink(65); ?>" class="more-news"><?php echo trans('readmore'); ?>  <div class="round"></div></a>
            </div>
            <?php
            wp_reset_postdata();
            wp_reset_query();
        endif;
        ?>
    </div>
</section>