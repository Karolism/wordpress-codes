<meta http-equiv="content-type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php  wp_title(); ?></title>
<link rel="stylesheet" type="text/css" href="<?php echo get_theme_url('/assets/libs/css/bootstrap.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo get_theme_url('/assets/libs/css/fontawesome.css'); ?>">
<link href="<?php bloginfo('stylesheet_url'); ?>" rel="stylesheet"/>
<!--[if IE]>
<link rel="stylesheet" type="text/css" href="/stylesheets/all-ie-only.css" />
<![endif]-->
<?php
if (is_msie()) {
    echo '<link rel="stylesheet" type="text/css" href="'.get_theme_url('/assets/libs/css/all-ie-only.css"').' />';
}
?>
<script type="text/javascript" src="<?php echo get_theme_url('/assets/libs/js/jquery-3.2.1.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo get_theme_url('/assets/libs/js/bootstrap.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo get_theme_url('/assets/libs/js/jquery.nice-select.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo get_theme_url('/assets/libs/js/jquery.transit.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo get_theme_url('/assets/libs/js/owl.carousel.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo get_theme_url('/assets/libs/js/script.js'); ?>"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&amp;libraries=geometry&amp;v=3.7"></script>