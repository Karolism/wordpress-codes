<?php
$curr_lang = str_replace(' ', '', qtrans_getLanguage());
?>

<nav class="navbar navbar-grey navbar-fixed-top <?php
if (is_front_page()) {
    echo 'front-page';
}
?>" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle x collapsed" data-toggle="collapse" data-target="#navbar-collapse-x">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo get_home_url() ?>"><img src="<?php echo get_theme_url('/assets/images/logo_white.png'); ?>" alt="Axis indurstries logo"></a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse-x">
            <?php
            wp_nav_menu(array(
                'menu' => 'primary',
                'theme_location' => 'primary',
                'depth' => 2,
                'container' => false,
                'menu_class' => 'nav navbar-nav',
                'fallback_cb' => 'wp_bootstrap_navwalker::fallback',
                'walker' => new wp_bootstrap_navwalker())
            );
            ?>

            <ul class="nav navbar-nav navbar-right">
                <li data-toggle="collapse" data-target="#navbar-collapse.in" class="exeptli">
                    <form action="<?php echo get_permalink(581); ?>" method="POST">
                        <div class="s-wrap">
                            <div class="s-rel">
                                <input class="search-input" type="text" name="search_text">
                                <a class="close" href="#"><i class="fa fas fa-times"></i></a>
                            </div>
                        </div>
                        <button type="submit" class="search-nav fa fas fa-search"></button>
                    </form>
                </li>
                <li data-toggle="collapse" data-target="#navbar-collapse.in">
                    <?php
                    $fb = get_field('facebook', 'option');
                    $linkedin = get_field('linkedin', 'option');
                    if ($linkedin):
                        ?>
                        <a class="social ln" href="<?php echo $linkedin; ?>"></a>
                    <?php endif; ?>
                </li>
                <li data-toggle="collapse" data-target="#navbar-collapse.in">
                    <?php if ($fb): ?>
                        <a class="social fb" href="<?php echo $fb; ?>"></a>
                    <?php endif; ?>
                </li>
                <li data-toggle="collapse" data-target="#navbar-collapse.in">
                    <select name="lang" id="language" data-native-menu="true">
                        <?php
                        $langs = qtrans_getSortedLanguages();
                        foreach ($langs as $lang) {
                            $selected = ($curr_lang == $lang) ? 'selected' : '';
                            echo '<option value="' . $lang . '"' . $selected . '>' . trans('in' . strtoupper($lang)) . '</option>';
                        }
                        ?>
                    </select>
                </li>
            </ul>
            <?php echo qtranxf_generateLanguageSelectCode('short'); ?>
        </div><!-- .navbar-collapse -->
    </div>
</nav>
<style>
    #qtranslate-chooser{
        display: none;
    }
</style>