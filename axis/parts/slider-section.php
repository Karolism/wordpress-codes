<div class="slider-holder">
    <div id="main" class="owl-carousel">

        <?php
        $slider = get_field('slider');
        if (have_rows('slider')):
            $i = 1;
            while (have_rows('slider')) : the_row();

                $image = get_sub_field('image');
                $title = get_sub_field('title');
                $description = get_sub_field('description');
                
                ?>
                <div class="holder" style="background-image: url(<?php echo $image; ?>)">
                    <div class="slide item-<?php echo $i; ?>">
                        <div class="container-fluid">
                            <div class="col-md-2 col-md-offset-1 sh"><div class="line"></div></div>
                            <div class="col-md-4 sh slider-content">
                                <div class="slide-title"><?php echo $title; ?></div>
                                <div class="slider-description"><?php echo $description; ?></div>
                            </div>
                            <div class="col-md-4 sh"><div class="line"></div></div>
                            <div class="col-md-1">  <br> </div>
                        </div>
                    </div>
                </div>
            <?php $i++; endwhile; ?>
        <?php  endif; ?>

    </div>
    <div class="more">
        <div class="round"></div>
        <div class="text"><?php echo trans('more_about'); ?></div>
    </div>
    <div class="arrow"></div>
</div>