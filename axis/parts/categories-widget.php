<?php
$tax_name = 'solution_categories';
$terms_solutions = get_terms(array(
    'taxonomy' => 'solution_categories',
    'hide_empty' => false,
        ));
$terms_products = get_terms(array(
    'taxonomy' => 'products_categories',
    'hide_empty' => false,
        ));
$terms_services = get_terms(array(
    'taxonomy' => 'service_categories',
    'hide_empty' => false,
        ));

$args = [
    'post_type' => 'page',
    'fields' => 'post_content',
    'nopaging' => true,
    'meta_key' => '_wp_page_template',
    'meta_value' => 'products_category.php'
];
$pages = get_posts($args);

// reset post data (important!)
wp_reset_postdata();
$solution_desc = '';
$products_desc = '';
$services_desc = '';
foreach ($pages as $page) {
    if ($page->ID == 258)
        $solution_desc = $page->post_content;
    if ($page->ID == 264)
        $products_desc = $page->post_content;
    if ($page->ID == 261)
        $services_desc = $page->post_content;
}
?>


<section id="services">
    <div class="container-fluid">
        <div class="col-md-3 col-md-offset-1 col-sm-offset-0 design-right-no-padd">
            <ul class="tabs">
                <li class="tab-link current" data-tab="tab-2"><span><?php echo trans('products_tab'); ?></span></li>
                <li class="tab-link" data-tab="tab-1"><span><?php echo trans('solutions_tab'); ?></span></li>

            </ul>
        </div>
        <div class="col-md-7 no-padd">
            <div id="tab-1" class="tab-content">
                <div class="col-md-4 no-padd">
                    <ul>
                        <?php
                        if (!empty($terms_solutions)):

                            foreach ($terms_solutions as $term):
                                ?>
                                <li><a href="<?php echo get_permalink(258) . "?select=" . $term->slug ?>"><?php echo $term->name; ?></a></li>
                                <?php
                            endforeach;
                        endif;
                        ?>
                    </ul>
                </div>
                <div class="col-md-8">
                    <div class="cat-info">
                        <div class="cat-title"><?php echo get_the_title(258); ?></div>
                        <div class="cat-desc">
                            <?php
                            echo $solution_desc;
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div id="tab-2" class="tab-content current">
                <div class="col-md-4 no-padd">
                    <ul>
                        <?php
                        if (!empty($terms_products)):

                            foreach ($terms_products as $term):
                                ?>
                                <li><a href="<?php echo get_permalink(264) . "?select=" . $term->slug ?>"><?php echo $term->name; ?></a></li>
                                <?php
                            endforeach;
                        endif;
                        ?>
                    </ul>
                </div>
                <div class="col-md-8">
                    <div class="cat-info">
                        <div class="cat-title"><?php echo get_the_title(264); ?></div>
                        <div class="cat-desc">
                            <?php
                            echo $products_desc;
                            ?>
                        </div>
                    </div>
                </div> 
            </div>

        </div>
    </div>
</section>