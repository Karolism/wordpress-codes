<?php
$location = get_field('map');
$phone = get_field('phone');
$email = get_field('email');
?>

<section>
    <div id="google-map" class="google-map-style2" data-ln="<?php echo $location['lng'] ?>" data-lt="<?php echo $location['lat'] ?>" data-img="<?php echo get_theme_url('/assets/images/marker.png') ?>">
    </div>  
    <div class="contacts">
        <div class="company-title">
            <?php echo get_option("client_title"); ?>
        </div>
        <table border="0">
            <tbody>
                <tr>
                    <td><img src="<?php echo get_theme_url('/assets/images/location.png') ?>" alt="location"></td>
                    <td>
                        <div class="label-c"><?php echo trans('client_address') ?></div>
                        <div class="content-c"><?php echo $location['address'] ?></div>
                    </td>
                </tr>
                <tr>
                    <td><img src="<?php echo get_theme_url('/assets/images/phone.png') ?>" alt="phone"></td>
                    <td>
                        <div class="label-c"><?php echo trans('client_phone'); ?></div>
                        <div class="content-c"><?php echo $phone; ?></div>
                    </td>
                </tr>
                <tr>
                    <td><img src="<?php echo get_theme_url('/assets/images/letter.png') ?>" alt="letter"></td>
                    <td>
                        <div class="label-c"><?php echo trans('client_email'); ?></div>
                        <div class="content-c"><?php echo $email; ?></div>
                    </td>
                </tr>
                <tr>
<?php
$fb = get_field('facebook', 'option');
$linkedin = get_field('linkedin', 'option');
if ($linkedin):
    ?>
                        <td>
                            <a href="<?php echo $linkedin; ?>"><i class="fa fa-linkedin-square"></i></a>
                        </td>
                    <?php endif; ?>
                    <?php if ($fb): ?>
                        <td>
                            <a href="<?php echo $fb; ?>"><i class="fa fa-facebook-official"></i></a>
                        </td>
                    <?php endif; ?>
                </tr>
            </tbody>
        </table>
        <div class="btn write" data-toggle="modal" data-target="#modal-contact"><?php echo trans('send_request'); ?></div>

    </div>
    <div class="modal fade modal-conatct" id="modal-contact" tabindex="-1" role="dialog" aria-labelledby="modal-contact" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button class="close" type="button" data-dismiss="modal"></button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="col-md-offset-3 col-md-6">
                            <div class="heading mail"><?php echo trans('contact_with_us') ?></div>
                            <?php
                            include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
                            ?>
                            <form method="post" action="<?php echo admin_url("admin-ajax.php"); ?>" class="contact-form">

                                <input type="text" name="name" class="form-field" placeholder="<?php echo trans('form_name'); ?>" required="required">
                                <input type="email" class="form-field field-email" name="email" placeholder="<?php echo trans('client_email'); ?>" required="required">
                                <input type="number" class="form-field field-phone" name="phone" placeholder="<?php echo trans('form_phone'); ?>" required="required">
                                <textarea class="form-field"  name="message" placeholder="<?php echo trans('form_message') ?>"></textarea>
                                <div class="form-results"></div>
                                <button type="submit" name="form-submit" value="1" class="submit-form btn write"><?php echo trans('field_submit') ?></button>


                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>