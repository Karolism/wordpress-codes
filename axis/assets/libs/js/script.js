$(document).ready(function () {

    var header_slider = $('#main');



    var career_item = $('#positions > li .fl,#positions > li .fr ');



    var upload_input = $('#upload-cv');



    var upload = $('#upload-cv + .upload');



    var category_item = $('.category-item');



    var detail_tab = $('.details .tabs .tab');



    var more = $('.more');
    
    var search = $('.search-nav');
    
    var close_s = $('.s-rel .close');
    
    
    close_s.on("click", function(e){
        e.preventDefault();
        $(this).parent().parent().fadeOut();
    });
   

    search.on("click", function (e) {
        e.preventDefault();
        var prev = $(this).prev();
        prev.fadeIn();
        if (prev.find('.search-input').val().length) {
            $(this).parent().submit();
            }

    });


    more.on('click', function () {

        $('html, body').animate({

            scrollTop: $(".index-news").offset().top - 85

        }, 900);

    });



    header_slider.owlCarousel({

        items: 1,

        mouseDrag: false,

        nav: true,

        navContainerClass: 'main-nav',

        navText: ['<span class="arrowleft"></span>', '<span class="arrowright"></span>'],

        onInitialized: counter, //When the plugin has initialized.

        onTranslated: counter //When the translation of the stage has finished.

    });



    $('#language').niceSelect();



    // Add slideDown animation to Bootstrap dropdown when expanding.

    $('.dropdown').on('show.bs.dropdown', function () {

        $(this).find('.dropdown-menu').first().stop(true, true).slideDown();

        console.log($(this).find(".caret"));

        $(this).find(".caret").transit({rotate: '-180deg'}, 'fast');

        ;

    });



    // Add slideUp animation to Bootstrap dropdown when collapsing.

    $('.dropdown').on('hide.bs.dropdown', function () {

        $(this).find('.dropdown-menu').first().stop(true, true).slideUp();

        $(this).find(".caret").transit({rotate: '0deg'}, 'fast');



    });



    career_item.on('touchend click', function (e) {

        e.stopPropagation();

        e.preventDefault();

        if (!$(this).parent().hasClass('active')) {

            $(this).parent().addClass('active');

            $(this).parent().find('.expandable-content').stop().slideDown("fast");

            $(this).parent().siblings().removeClass('active');

            $(this).parent().siblings().find('.expandable-content').stop().slideUp("fast");

        } else {

            $(this).parent().removeClass('active');

            $(this).parent().find('.expandable-content').stop().slideUp("fast");

        }



    });



    $('.manager-block .btn').on('click', function (e) {

        $('.modal').modal({show: true});

    });



    $('ul.tabs li').click(function () {

        var tab_id = $(this).attr('data-tab');



        $('ul.tabs li').removeClass('current');

        $('.tab-content').removeClass('current');

        $(this).addClass('current');
        $(this).siblings().removeClass('current');

        $("#" + tab_id).addClass('current');

        if ($(window).width() <= 768) {
$("#" + tab_id).siblings().removeClass(".current").slideUp();
            $("#"+tab_id+'.tab-content.current').slideDown();

            $('html, body').animate({

                scrollTop: $(".tab-link").last().offset().top

            }, 900);
        }



    });







    if ($('#google-map').length > 0) {



        var pos = {

            lat: $('#google-map').data('lt'),

            lng: $('#google-map').data('ln')

        };

        var center = {

            lat: pos.lat,

            lng: pos.lng - 0.007

        }

        var map = new google.maps.Map(document.getElementById('google-map'), {

            zoom: 16,

            center: center,

            scrollwheel: false,

            mapTypeControl: false

        });



        // InfoWindow content

        var image = $('#google-map').data('img');

        var beachMarker = new google.maps.Marker({

            position: pos,

            map: map,

            icon: image,

            title: 'Mus rasite'

        });

        map.set('styles', [

            {

                featureType: 'Greyscale',

                stylers: [

                    {

                        saturation: -90

                    },

                    {

                        gamma: 1.5

                    }]

            }]);

    }





    upload.on('click', function () {



        upload_input.trigger('click');





    });

    upload_input.bind("change", function (e) {

        if (e.target.value != null) {

            var splittedValues = e.target.value.split('\\');

            if (splittedValues.length == 3) {

                var fileName = splittedValues[splittedValues.length - 1];

                if (fileName != null) {

                    upload.find('.form-label span').text(fileName);

                }

            }

        }

    });





    category_item.on('touchend click', function (e) {

        var sub_id = $(this).attr('data-id');

        $('.subcategory').hide();

        $('#' + sub_id).fadeIn();

        if ($(window).width() <= 768) {

            setTimeout(function () {

                $('html, body').animate({

                    scrollTop: $('#' + sub_id).offset().top

                }, 900);

            }, 800);

        }

    });



    $('.details-content').first().show();

    detail_tab.on('click', function (e) {

        var sub_id = $(this).attr('data-id');

        $(this).siblings().removeClass('active');

        $(this).addClass('active');



        $.each($('.details-content'), function (i, val) {

            if ($(this).is('#' + sub_id))

                console.log($(this));

            $(this).animate({opacity: 0}, 'fast').hide();

        });



        $('#' + sub_id).transition({opacity: 1}).show();



    });



    $('#language + .nice-select .list li').on('click', function () {

        var selected_lang = $(this).data('value');

        location.href = window.location.protocol + "//" + window.location.host + "/"+selected_lang;

    });



    $(window).resize(function () {

        if ($(window).width() > 992) {

            header_slider.owlCarousel('destroy');

            header_slider.owlCarousel({

                items: 1,

                mouseDrag: false,

                nav: true,

                navContainerClass: 'main-nav',

                navText: ['<span class="arrowleft"></span>', '<span class="arrowright"></span>'],

                onInitialized: counter, //When the plugin has initialized.

                onTranslated: counter //When the translation of the stage has finished.

            });

        }



    });

});



$.fn.animateRotate = function (startAngle, endAngle, duration, easing, complete) {

    return this.each(function () {

        var elem = $(this);



        $({deg: startAngle}).animate({deg: endAngle}, {

            duration: duration,

            easing: easing,

            step: function (now) {

                elem.css({

                    '-moz-transform': 'rotate(' + now + 'deg)',

                    '-webkit-transform': 'rotate(' + now + 'deg)',

                    '-o-transform': 'rotate(' + now + 'deg)',

                    '-ms-transform': 'rotate(' + now + 'deg)',

                    'transform': 'rotate(' + now + 'deg)'

                });

            },

            complete: complete || $.noop

        });

    });

};



function counter(event) {

    var element = event.target;         // DOM element, in this example .owl-carousel



    var items = event.item.count;     // Number of items

    var item = event.item.index + 1;     // Position of the current item

    $('#counter').html(item + "/" + items);



    var movable = $('.main-nav, #counter');

    if ($('.item-' + item + ' .line').length > 0) {

        movable.animate({top: $('.item-' + item + ' .line').first().offset().top - 9}, 200);

    }

}