<footer>
    <div class="container-fluid">
        <div class="col-md-2 col-md-offset-1">
            <a href=""><img class="img-responsive" src="<?php echo get_theme_url('/assets/images/logo-black.png'); ?>" alt="dark logo"></a>
        </div>
        <div class="col-md-6">
            <div class="footer-nav">
                <?php
                wp_nav_menu(array(
                    'menu'=>'Bottom menu',
                    'theme_location' => 'axis',
                    'container' => false,
                    'items_wrap' => '<ul>%3$s</ul>'
                ));
                ?>
            </div>
        </div>
        <div class="col-md-2">
            <div class="solution">
                <?php echo trans('solution'); ?>: <a href="">Dizaino arkliukas</a>
            </div>

        </div>
    </div>
</footer>
</body>
</html>