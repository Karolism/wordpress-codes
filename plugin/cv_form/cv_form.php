<?php
/*
  Plugin Name: CV Form
  Plugin URI: http://minaja.com
  Description: Just another cv form plugin
  Author: Min dev
  Author URI: http://minaja.com
  Text Domain: cv_form
  Version: 1.0
 */



function cv_form_add_sccss() {
    wp_register_script('cv_form-jquery-form', plugins_url('js/jquery.form.min.js', __FILE__), array(), '1.0');
    wp_register_script('cv_form-js', plugins_url('js/form-scripts.js', __FILE__), array(), '1.0', true);
    wp_register_style('cv_form-style', plugins_url('css/style.css', __FILE__), array(), '1.0');
    wp_enqueue_script('cv_form-jquery-form');
    wp_enqueue_script('cv_form-js');
    wp_enqueue_style('cv_form-style');
    if (RECAPTCHA) {
        wp_register_script('cv_form-g-recaptcha', '//www.google.com/recaptcha/api.js', array(), '1.0', true);
        wp_enqueue_script('cv_form-g-recaptcha');
    }
}

add_action('wp_enqueue_scripts', 'cv_form_add_sccss');
require_once plugin_dir_path(__FILE__) . 'class.simple_mail.php';
$simple_mail = new SimpleMail();

//SUBMITION

add_action('wp_ajax_nopriv_cv_form', 'cv_form_act');
add_action('wp_ajax_cv_form', 'cv_form_act');

function cv_form_act() {
    global $simple_mail;
    $error_msg = "";

    if (isset($_POST['form-submit'])) {
//		if(RECAPTCHA && empty($_POST['g-recaptcha-response'])) {
//			$error_msg = trans('fill_captcha');
//		}

        $req_fields_array = $_POST['req_fields'];

        $req_error = '';
        $i = 0;
        $boolen_req = false;
        foreach ($req_fields_array as $req_field) {
            if (empty($_POST[$req_field])) {
                $i ++;
                if (count($req_fields_array) > 1) {
                    $req_error .= trans('form_' . $req_field) . ', ';
                } else {
                    $req_error .= trans('form_' . $req_field);
                }
                $boolen_req = true;
            }
        }
        if ($boolen_req) {
            $req_trans = $i > 1 ? 'form_required2' : 'form_required';
            $error_msg = $req_error . ' ' . trans($req_trans);
        }

        //$email_to = 'karolis@minaja.com';
        $email_to = (isset($_POST['emailto']))?$_POST['emailto']: get_option('admin_email');



        if (!empty($_POST['subject'])) {
            $subject = $_POST['subject'] . ' (' . site_url() . ')';
        } else {
            $subject = "CV anketa". ': ' . get_bloginfo('name') . ' (' . site_url() . ')';
        }
        $fromName = $_POST['name'];
        $from = $_POST['email'];
        $message = "Vardas, pavardė: " . $_POST['name'] . " " . $_POST['lastname'] 
                . "<br>" . "Miestas: " . $_POST['city'] .
                "<br>Telefonas: " . $_POST['phone'].
                "<br>El. paštas: ".$_POST['email'].
                "<br>Dominanti sritis: ".$_POST['intresting'].
                "<br>Išsilavinimas: ".$_POST['education']; //message

        $file_tmp_name = $_FILES['cvfile']['tmp_name'];
        $file_name = $_FILES['cvfile']['name'];
        $file_size = $_FILES['cvfile']['size'];
        $file_type = $_FILES['cvfile']['type'];
        $file_error = $_FILES['cvfile']['error'];

        if ($file_error > 0) {
            die('Upload error or No files uploaded');
        }
        //read from the uploaded file & base64_encode content for the mail
        $handle = fopen($file_tmp_name, "r");
        $content = fread($handle, $file_size);
        fclose($handle);
        $encoded_content = chunk_split(base64_encode($content));

        $boundary = md5("sanwebe");
        //header
        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "From:" . $from_email . "\r\n";
        $headers .= "Reply-To: " . $reply_to_email . "" . "\r\n";
        $headers .= "Content-Type: multipart/mixed; boundary = $boundary\r\n\r\n";

        //html text 
        $body = "--$boundary\r\n";
        $body .= "Content-Type: text/html; charset=UTF-8\r\n";
        $body .= "Content-Transfer-Encoding: base64\r\n\r\n";
        $body .= chunk_split(base64_encode($message));

        //attachment
        if (!empty($_FILES)) {
            $body .= "--$boundary\r\n";
            $body .= "Content-Type: $file_type; name=" . $file_name . "\r\n";
            $body .= "Content-Disposition: attachment; filename=" . $file_name . "\r\n";
            $body .= "Content-Transfer-Encoding: base64\r\n";
            $body .= "X-Attachment-Id: " . rand(1000, 99999) . "\r\n\r\n";
            $body .= $encoded_content;
        }
$test = 'no';
        if(@mail($email_to, $subject, $body, $headers)){
            $test = 'send';
        }
        
    } else {
        $error_msg = trans('post_error');
    }

    // redirect to success page
    if ($error_msg == "") {
        $response = array(
            'success',
            trans('success'),
            $_FILES
        );
    } else {

        if ($error_msg == "") {
            $error = trans('wrong');
        } else {
            $error = $error_msg;
        }

        $response = array(
            'error',
            
        );
    }

    echo json_encode($response, JSON_UNESCAPED_UNICODE);
    exit;
}
