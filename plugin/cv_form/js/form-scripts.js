//jQuery ajaxform
var required_fields_cv = [];

$('.cv-form .form-field[required=true]').each(function() {
  var this_name = $(this)[0].name;
  required_fields_cv.push(this_name);
});


var form_results_cv = $('.cv-form .form-results');
var cv_form_opt = {
  beforeSubmit: function (formData, jqForm, options) {
    $('.cv-form .field-error').remove();
    var requiem = options.data.req_fields;
    for (var i = 0; i < formData.length; i++ ) {
      if (formData[i].required && !formData[i].value) {
        var thizs = $('.cv-form .form-field[name="' + formData[i].name + '"]');
        thizs.addClass('not-valid');
        $('<div class="field-error">' + trans_fill_field + '</div>').insertAfter(thizs);

        return false;
      }
    }
  },
  data: {
    action : 'cv_form',
    req_fields : required_fields_cv
  },
  success : function(response, statusText, xhr, $form){
    console.log(response);
    var response = JSON.parse(response);
    var output = '';
    if (response[0] == 'success') {
      output = '<div class="form-success">' + response[1] + '</div>';
      //reset values in all input fields
      // $('.contact-form .form-field[required=true]').val('');
      $('.cv-form .form-field').val('');
      console.log(required_fields_cv);
      form_results_cv.slideUp(); //hide form after success
    } else {
      output = '<div class="form-error">' + response[1] + '</div>';
    }
    form_results_cv.hide().html(output).slideDown();
  },
  error : function(data){
    console.log(data);
  }
};

$('.cv-form').ajaxForm(cv_form_opt);
