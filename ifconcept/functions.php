<?php
// Core includes
include 'services.php';
//Translations
// require "core/langs.php";
// Remover Admin menus
require "core/remove-menus.php";
// Template config
require "core/template-config.php";
//Remover WP updates
require "core/remove-updates.php";
//Breadcrumbs
require "core/breadcrumbs.php";
//Post types
require "core/post-types.php";
// Displayng Admin bar
if (!ADMIN_BAR) {
  add_filter('show_admin_bar', '__return_false');
}
// Theme supports
add_theme_support('menus');
add_theme_support('post-thumbnails');
//Remove emoji
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );
//Main functions
//flush_rewrite_rules( false );
require "core/main-functions.php";
//Client information ADMIN page
if (CLIENT_INFO_PAGE) {
  require "core/client-information.php";
}
//Client translations ADMIN page
if (function_exists('qtranxf_getSortedLanguages') && CLIENT_TRANSLATION) {
  require "core/client-translations.php";
}
if (function_exists('acf_add_options_page')) {

    acf_add_options_page(array(
        'page_title' => 'Theme General Settings',
        'menu_title' => 'Theme Settings',
        'menu_slug' => 'theme-general-settings',
        'capability' => 'edit_posts',
        'redirect' => false
    ));

//    acf_add_options_sub_page(array(
//        'page_title' => 'Theme Header Settings',
//        'menu_title' => 'Header',
//        'parent_slug' => 'theme-general-settings',
//    ));
}

function my_acf_init() {
	
	acf_update_setting('google_api_key', get_field('google_maps_api','option'));
}

add_action('acf/init', 'my_acf_init');
