<?php
/**
 * @Author: Dewdrop | Aivaras Čenkus
 * @Date:   2016-10-31 14:33:40
 * @Last Modified by:   Dewdrop | Aivaras Čenkus
 * @Last Modified time: 2016-10-31 14:33:54
 */

include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
?>
<?php if (is_plugin_active( 'dyn_slider_2/dyn_slider-plugin.php')) { ?>
	<div class="slider js_simple_dots simple">
	  <div class="frame js_frame">
	   <?php
	      $slider_args = array(
	        'post_type'       => 'dyn_slider', 
	        'posts_per_page'  => -1
	      );
	    ?>
	    <?php $slider = new WP_Query($slider_args); ?>
	    <?php if ($slider -> have_posts()) { ?>
	    	<ul class="slides js_slides">
		      <?php while ($slider -> have_posts()) { ?>
		        <?php $slider -> the_post(); ?>
		        <li class="js_slide">
		        	<?php echo get_the_post_thumbnail($slider -> ID, 'small_thumb'); ?>
			        <h1>
	              <a href="<?php echo get_the_permalink(); ?>">
	                <?php echo title(); ?>
	              </a>
	            </h1>
	            <p>
	              <?php echo excerpt(); ?>
	            </p>
			      </li>
		      <?php } ?>
	      </ul>
	      <?php wp_reset_postdata(); ?>
	    <?php } ?>   
	  </div>
	  <span class="js_prev prev">
	    <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 501.5 501.5">
	      <g>
	        <path fill="#2E435A" d="M302.67 90.877l55.77 55.508L254.575 250.75 358.44 355.116l-55.77 55.506L143.56 250.75z">
	        </path>
	      </g>
	    </svg>
	  </span>
	  <span class="js_next next">
	    <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 501.5 501.5">
	      <g>
	        <path fill="#2E435A" d="M199.33 410.622l-55.77-55.508L247.425 250.75 143.56 146.384l55.77-55.507L358.44 250.75z">
	        </path>
	      </g>
	    </svg>
	  </span>
	  <ul class="js_dots dots"></ul>
	</div>
<?php } else { ?>
	Išjungtas skydelio modulis.
<?php } ?>