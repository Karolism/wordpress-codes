<?php
/**
* @Author: Dewdrop
* @E-mail:   hello@dewdrop.eu
* @Author URL: http://dewdrop.eu
* @Date:   2016-03-03 11:29:26
* @Last Modified by:   Dewdrop
* @Last Modified time: 2016-03-03 11:30:42
*/
?>
<!-- main things -->
<title><?php wp_title('|', true, 'right'); ?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
<meta charset="<?php bloginfo('charset'); ?>"/>
<meta name="description" content="<?php bloginfo('description'); ?>">
<!-- for smart devices -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="mobile-web-app-capable" content="yes"/>
<!-- theme color -->
<meta name="theme-color" content="#e98124">
<meta name="apple-mobile-web-app-status-bar-style" content="#e98124">
<meta name="msapplication-TileColor" content="#e98124">
<!-- favicons -->
<!--<meta name="msapplication-TileImage" content="<?php echo get_theme_url('/assets/images/favicon/favicon-ms.png'); ?>">
<link rel="icon" sizes="192x192" href="<?php echo get_theme_url('/assets/images/favicon/favicon-android.png'); ?>">
<link rel="icon" sizes="16x16" href="<?php echo get_theme_url('/assets/images/favicon/favicon.ico'); ?>" type="image/x-icon"/>
<link rel="apple-touch-icon" sizes="128x128" href="<?php echo get_theme_url('/assets/images/favicon/logo-ios.png'); ?>">-->
<!-- font -->
<link href='//fonts.googleapis.com/css?family=Ubuntu:400,400italic,500,700,500italic,300&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
<!-- font awesome-->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- helper -->
<link href="<?php echo get_theme_url('/assets/libs/css/bootstrap-edited.css'); ?>" rel="stylesheet"/>
<!-- jQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?php echo get_theme_url('/assets/libs/js/jquery.min.js'); ?>"><\/script>')</script>
<!-- fancybox -->
<link href="<?php echo get_theme_url('/assets/libs/fancybox/jquery.fancybox.css'); ?>" rel="stylesheet"/>
<script src="<?php echo get_theme_url('/assets/libs/fancybox/jquery.fancybox.js'); ?>"></script>
<!-- responsive tables -->
<!--<link href="<?php echo get_theme_url('/assets/libs/responsive-tables/tables.css'); ?>" rel="stylesheet"/>
<script src="<?php echo get_theme_url('/assets/libs/responsive-tables/tables.js'); ?>"></script>-->
<!-- owl-carousel -->
<link href="<?php echo get_theme_url('/assets/libs/css/owl.carousel.min.css'); ?>" rel="stylesheet"/>
<link href="<?php echo get_theme_url('/assets/libs/css/owl.theme.default.min.css'); ?>" rel="stylesheet"/>
<script type="text/javascript" src="<?php echo get_theme_url('/assets/libs/js/owl.carousel.min.js'); ?>"></script>
<!-- owl-carousel -->
<link href="<?php echo get_theme_url('/assets/libs/css/nice-select.css'); ?>" rel="stylesheet"/>
<script type="text/javascript" src="<?php echo get_theme_url('/assets/libs/js/jquery.nice-select.min.js'); ?>"></script>
<!-- feature js -->
<script src="<?php echo get_theme_url('/assets/libs/js/feature.min.js'); ?>"></script>
<!-- google maps -->
<!--//<?php if (get_option('map_api_key')) { ?>
  <script src="//maps.googleapis.com/maps/api/js?key=//<?php echo get_option('map_api_key'); ?>" type="text/javascript"></script>
//<?php } else { ?>
  <script src="//maps.google.com/maps/api/js" type="text/javascript"></script>
//<?php } ?>
main css-->
<link href="<?php bloginfo('stylesheet_url'); ?>?v=<?php echo time(); ?>" rel="stylesheet"/>
<?php if (SOCIAL_META) { ?>
  <!-- head for social networks -->
  <!-- Schema.org markup for Google+ -->
  <meta itemprop="name" content="<?php wp_title('|', true, 'right'); ?>">
  <meta itemprop="description" content="<?php bloginfo('description'); ?>">
  <!-- Twitter Card data -->
  <meta name="twitter:site" content="<?php echo site_url(); ?>">
  <meta name="twitter:title" content="<?php wp_title('|', true, 'right'); ?>">
  <meta name="twitter:description" content="<?php bloginfo('description'); ?>">
  <!-- Open Graph data -->
  <?php if(get_the_title()){ ?>
    <meta property="og:title" content="<?php echo get_the_title(); ?>" />
  <?php } ?>
  <meta property="og:type" content="article" />
  <meta property="og:url" content="<?php echo site_url(); ?>" />
  <meta property="og:description" content="<?php bloginfo('description'); ?>" />
  <meta property="og:site_name" content="<?php wp_title('|', true, 'right'); ?>" />
  <!-- images -->
  <?php if (has_post_thumbnail(get_the_ID())) { ?>
    <meta name="twitter:card" content="<?php echo get_thumb_url(get_the_ID(), 'large'); ?>">
    <!-- Twitter 280x150px -->
    <meta name="twitter:image:src" content="<?php echo get_thumb_url(get_the_ID(), 'large'); ?>">
    <meta itemprop="image" content="<?php echo get_thumb_url(get_the_ID(), 'large'); ?>">
    <meta property="og:image" content="<?php echo get_thumb_url(get_the_ID(), 'large'); ?>" />
  <?php } ?>
<?php } ?>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?key=<?php echo get_field('google_maps_api','option'); ?>&sensor=false&amp;libraries=geometry&amp;v=3.7"></script>