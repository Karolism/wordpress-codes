<?php
/**
 * @Author: Dewdrop | Aivaras Čenkus
 * @Date:   2016-03-15 16:07:54
 * @Last Modified by:   Dewdrop | Aivaras Čenkus
 * @Last Modified time: 2016-10-31 14:33:14
 */
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
?>
<?php if (is_plugin_active('dd_form/dd_form.php')) { ?>
    <form method="post" action="<?php echo admin_url("admin-ajax.php"); ?>" class="contact-form">
        <div class="form-results"></div>
        <div class="form-fields">
            <div class="float-label">
                <input class="form-field" name="name" id="name" type="text" placeholder=" " required="true">
                <label for="name"><?php echo trans('field_name') ?></label>
            </div>
            <div class="pair">
                <div class="float-label">
                    <input class="form-field" name="phone" id="phone" type="text" placeholder=" " required="true">
                    <label for="phone"><?php echo trans('client_phone') ?></label>
                </div>
                <div class="float-label">
                    <input class="form-field" name="email" id="email" type="email" placeholder=" " required="true">
                    <label for="email"><?php echo trans('field_email') ?></label>
                </div>
            </div>
            <div class="float-label">
                <textarea class="form-field" name="message" id="message" placeholder=" " required="true"></textarea>
                <label for="message"><?php echo trans('form_message') ?></label>
            </div>

            <div class="send-wrap">
                <button type="submit" name="form-submit" value="1" class="submit-form btn-form">
	          <?php echo trans('field_submit'); ?>
	        </button>
            </div>
        </div>
    </form>
<?php } else { ?>
    Išjungtas kontaktų formos modulis.
<?php } ?>