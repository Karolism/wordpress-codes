<?php get_header(); ?>
<?php if (have_posts()): ?>
    <?php
    while (have_posts()):
        the_post();
        ?>
        <section class="page-header" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>)">
            <h1> <?php echo get_the_title() ?></h1>
        </section>
        <div class="content">
            <div class="container">
                <div class="text">
                    <?php echo the_content(); ?> 
                </div>
            </div>
        </div>
    <?php endwhile; ?>
<?php endif; ?>
<?php
$services = get_field('paslaugu_sarasas');
if (!empty($services)):
    ?>
    <section class="services">
        <div class="container">
            <div class="row">
                <div id="list-column" class="col-md-4 px-0">
                    <div class="list">
                        <?php
                        if (!empty($services)) {
                            foreach ($services as $key => $service) {
                                if (!empty($service['aprasymas'])) {
                                    echo '<a data-href="tab-' . $key . '" class="tab ' . ($key == 0 ? 'active' : '') . '">' . $service['pavadinimas'] . '</a>';
                                }
                            }
                        }
                        ?>

                    </div>
                </div>
                <div class="col-md-8 px-0">
    
                    <div class="tab-content">
                        <?php
                        
                            foreach ($services as $key => $service) {
                                echo '<div id="tab-' . $key . '" class="' . ($key == 0 ? 'active' : '') . '">' . $service['aprasymas'] . '</div>';
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php
    $files = get_field('failai');

    if (!empty($files)):
//    dump($files);
        ?>
        <section id="service-files">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 px-0">
                        <h3><?php echo trans('docs_and_specs'); ?></h3>
                    </div>
                    <div class="col-md-8 px-0">
                        <?php foreach ($files as $file): ?>
                            <div class="col-md-3 col-sm-6 text-center file-hold">
                                <img src="<?php echo get_theme_url('/assets/images/download-pic.png') ?>" alt="download file">
                                <div class="file-title"><?php
                                    $short_title = wp_trim_words($file['failas']['title'], 5, '...');
                                    echo $short_title;
                                    ?></div>
                                <a href="<?php echo $file['failas']['url']; ?>" download=""><?php echo trans('browser_download'); ?></a>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </section>
        <?php
    endif;
endif;
?>
<?php get_footer(); ?>

