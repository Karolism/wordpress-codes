<?php get_header(); ?>
<div class="page-container container">
  <?php echo breadcrumbs(); ?>
  <?php if (have_posts()) { ?>
    <?php while (have_posts()) { ?>
      <?php the_post(); ?>
      <article <?php post_class(); ?>>
        <?php if (has_post_thumbnail()) { ?>
          <div class="item-image">
            <a href="<?php echo get_the_permalink(); ?>">
              <?php echo get_the_post_thumbnail('small_thumb'); ?>
            </a>
          </div>
        <?php } ?>
        <h1>
        <a href="<?php echo get_the_permalink(); ?>">
          <?php echo get_the_title(); ?>
        </a>
        </h1>
        <?php echo excerpt(); ?>
      </article>
    <?php } ?>
  <?php } ?>
</div>
<?php
get_footer();