<?php
/**
 * @Author: Dewdrop | Aivaras Čenkus
 * @Date:   2016-03-15 16:07:54
 * @Last Modified by:   Dewdrop | Aivaras Čenkus
 * @Last Modified time: 2016-03-23 15:12:58
 */
// Remove updates
if (get_current_user_id() > 1) {
  // Remove updates and other things 
  function remove_core_updates(){
    global $wp_version;

    return(object) array('last_checked'=> time(),'version_checked'=> $wp_version,);
  }
  add_filter('pre_site_transient_update_core','remove_core_updates');
  add_filter('pre_site_transient_update_plugins','remove_core_updates');
  add_filter('pre_site_transient_update_themes','remove_core_updates');

  add_filter('auto_update_plugin', '__return_false');
  add_filter('auto_update_theme', '__return_false');

  remove_action('update-core.php','wp_update_plugins');
  // Remove update icon from admin top bar
  add_action('admin_bar_menu', 'remove_wp_updates', 999);

  function remove_wp_updates($wp_admin_bar) {
    // var_dump($wp_admin_bar); die();
    $wp_admin_bar->remove_node('wp-logo');
    $wp_admin_bar->remove_node('updates');
  }

  // Hide admin footer from admin
  function change_footer_version() {
    return ' ';
  }
  add_filter('update_footer', 'change_footer_version', 9999);

  function edit_admin_menus() {  
    global $submenu;  
    unset($submenu['index.php'][10]);
    return $submenu;
  }  
  add_action( 'admin_menu', 'edit_admin_menus' ); 
}