<?php
/**
 * @Author: Dewdrop | Aivaras Čenkus
 * @Date:   2016-03-15 16:07:54
 * @Last Modified by:   Dewdrop | Aivaras Čenkus
 * @Last Modified time: 2016-06-17 10:32:13
 */
// Removers
function remove_elements ($sHideElements) {
  
  $sStyle = '<style type="text/css">';
    if ($sHideElements) {
       $sStyle .= ''.$sHideElements.' {
        display: none !important;
      }';
    }
    $sStyle .= '
    #toplevel_page_edit-post_type-acf,
    .profile-php .user-description-wrap,
    .profile-php .user-display-name-wrap,
    #menu-settings .wp-submenu li:nth-child(4),
    #menu-settings .wp-submenu li:nth-child(5),
    #menu-settings .wp-submenu li:nth-child(6),
    #menu-settings .wp-submenu li:nth-child(7),
    #menu-settings .wp-submenu li:nth-child(8),
    #menu-appearance .wp-submenu li:nth-child(5),
    .options-general-php .form-table tr:nth-child(3),
    .options-general-php .form-table tr:nth-child(4),
    .options-general-php .form-table tr:nth-child(5),
    .options-general-php .form-table tr:nth-child(6),
    .options-general-php .form-table tr:nth-child(7) {
      display: none !important;
    }

    #toplevel_page_client_options a {
      background: #ccc;
    }
  </style>';
  if (get_current_user_id() > 1) {
    echo $sStyle;
  }
  //custom menu style
  echo '<style type="text/css">
    #toplevel_page_client_options a {
      border-top: 1px solid #60626B;
      border-bottom: 1px solid #60626B;
      font-weight: 600 !important;
      background: #404146;
    }
    #toplevel_page_client_options div.wp-menu-image {
      width: 28px;
    }
  </style>';
}

add_action('admin_head', 'remove_elements');

if (get_current_user_id() > 1) {
  add_action('admin_menu', 'remove_menus');
}