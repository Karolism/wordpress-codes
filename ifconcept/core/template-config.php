<?php
/**
 * @Author: Dewdrop | Aivaras Čenkus
 * @Date:   2016-03-15 16:07:54
 * @Last Modified by:   Dewdrop | Aivaras Čenkus
 * @Last Modified time: 2016-11-17 13:01:18
 */
// Remove WP Updates notices: TRUE/FALSE
define('REMOVE_UPDATES', false);
// Breadcrumbs: TRUE/FALSE
define('BREADCRUMBS', false);
// Social share META tags: TRUE/FALSE 
define('SOCIAL_META', true);
// Dashboard bar: TRUE/FALSE
define('ADMIN_BAR', false);
// Client information
// Client fields
define('CLIENT_TITLE', true);
define('CLIENT_EMAIL', true);
define('CLIENT_ADRESS', true);
define('CLIENT_PHONE', true);
define('CLIENT_FAX', true);
define('CLIENT_CODE', true);
define('CLIENT_VAT', false);
define('CLIENT_GA', true);
define('MAP_API_KEY', true);
// Client Social
define('CLIENT_SOCIAL', true);
define('CLIENT_FB', true);
define('CLIENT_LINKEDIN', true);
define('CLIENT_TWITTER', true);
define('CLIENT_YOUTUBE', true);
define('CLIENT_INSTAGRAM', true);
define('CLIENT_GPLUS', true);
define('CLIENT_PINTEREST', true);
define('CLIENT_SKYPE', true);
// Contact form
define('RECAPTCHA_KEY', '6Lf13CQTAAAAABfh8sz44JMOIXlhxwkzcHbCro87');
define('RECAPTCHA', true);
define('CLIENT_INFO_PAGE', true);
// Client translation
define('CLIENT_TRANSLATION', true);
//Admin CLIENT information page custom fields
// $aAdminCustomFields = array(
//   array(
//     'client_custom', //key
//     'Client custom', //value
//     true //boolean
//   ),
//    array(
//     'client_custom', //key
//     'Client custom', //value
//     true //boolean
//   ),
// );

// Menu registrations
register_nav_menus(
  array(
    'header-menu' => 'Top menu'
  )
);
// Thumbnails size registering
add_image_size('thumb_news', 540, 355, true);
add_image_size('thumb_img', 350, 260, true);
add_image_size('small_thumb', 350, 230, true);
// Admin MENU remove by CSS elements
// $sHideElements = '.menu-icon-media';
// Admin MENU remove by url
function remove_menus() {
  // Debuging
  // echo '<pre>' . var_dump( $GLOBALS[ 'menu' ]) . '</pre>'; die();
  // Dashboard
  // remove_menu_page( 'index.php' );
  // Posts
  // remove_menu_page('edit.php');
  // Media
  // remove_menu_page('upload.php');
  // Pages
  // remove_menu_page('edit.php?post_type=page');
  // Comments
  remove_menu_page('edit-comments.php');
  // Appearance
  // remove_menu_page( 'themes.php' );
  // Plugins
  remove_menu_page('plugins.php');
  // Tools
  remove_menu_page('tools.php');
  // Settings
  // remove_menu_page('options-general.php');
  // Users
  remove_menu_page('users.php');
  // remove_all_filters('users.php');
  // Advanced custom fields
  // remove_menu_page('edit.php?post_type=acf');
  // Contact forms 7
  // remove_menu_page( 'wpcf7' );
}