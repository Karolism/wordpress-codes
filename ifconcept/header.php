<!DOCTYPE HTML>
<html <?php language_attributes(); ?><?php echo (SOCIAL_META ? ' itemscope itemtype="http://schema.org/Article"' : ''); ?>>

    <head>
        <?php get_part('header', 'head'); ?>
        <!-- wordpress head -->
        <?php wp_head(); ?>
    </head>
    <body <?php body_class() ?> >
        <!-- header -->
        <?php
        /**
         * @Author: Dewdrop | Aivaras Čenkus
         * @Date:   2016-03-15 16:07:54
         * @Last Modified by:   Dewdrop | Aivaras Čenkus
         * @Last Modified time: 2016-10-31 14:33:14
         */
        include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
        ?>
        <?php if (is_plugin_active('dd_form/dd_form.php') && is_page_template('page-contacts.php') !== true): ?>
        <?php
        $args = array(
	'posts_per_page'   => -1,
	'orderby'          => 'date',
	'order'            => 'DESC',
	'post_type'        => 'services',
	'post_status'      => 'publish'
);
$posts_array = get_posts( $args );
        ?>
            <div class="fixed-form">
                <div class="button-wrap">
                    <div class="open-button">
                        <?php echo trans('expand_form'); ?>
                    </div>
                </div>
                <div class="close-form">
                </div>
                <form method="post" action="<?php echo admin_url("admin-ajax.php"); ?>" class="contact-form">
                    <div class="form-results"></div>
                    <select name="service-select" id="service-select" class="form-field" required="true">
                        <option value="pasirinktie" selected="" hidden disabled=""><?php echo trans('select_service'); ?>...</option>
                        <?php
                        foreach($posts_array as $post){
                            echo '<option value="'.$post->post_title.'">'.$post->post_title.'</option>';
                        }
                        ?>
                    </select>
                    <input name="name" id="name" class="form-field" type="text" placeholder="<?php echo trans('field_name') ?>" required="true">
                    <input name="phone" id="phone" class="form-field" type="text" placeholder="<?php echo trans('client_phone') ?>" required="true">
                    <input name="email" id="email" class="form-field" type="email" placeholder="<?php echo trans('client_email') ?>" required="true">
                    <div class="send-wrap">
                        <button type="submit" name="form-submit" value="1" class="submit-form btn-form">
                            <?php echo trans('field_submit'); ?>
                        </button>
                    </div>
                </form>
            </div>
        <?php endif; ?>
        <header class="header">
            <div class="container">
                <div class="row">
                    <div class="col-xs-3">
                        <a href="<?php echo site_url(); ?>" class="logo"><img src="<?php echo get_field('logo', "option") ?>" alt="IF Consept logo"></a>
                    </div>
                    <div class="col-xs-9 static">
                        <div class="header-menu-hamburger">
                            <span></span>
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                        <?php echo get_menu('header-menu'); ?>
                    </div>
                </div>
            </div>
        </header>
        <!-- main container begin -->
        <main class="main-container">
            <?php get_part('header', 'cookies'); ?>