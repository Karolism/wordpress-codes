</main>
<footer class="footer">
  <div class="container">
      <?php $footer = get_field('footer', 'option');
      ?>
  	<div class="row">
            <?php foreach ($footer as $item): ?>
            <div class="col-md-4">
                <?php echo $item; ?>
            </div>
            <?php endforeach;?>
  	</div>
      <?php
      $socials = get_field('socials', 'option');
      if(!empty($socials)):
      ?>
      <div class="socials text-center">
          <?php 
          if(!empty($socials['facebook'])){
              echo '<a class="fa fab fa-facebook-f" href="'.$socials['facebook'].'" target="_blank"></a>';
          }
          if(!empty($socials['linkedin'])){
              echo '<a class="fa fa-linkedin" href="'.$socials['linkedin'].'" target="_blank"></a>';
          }
          ?>
      </div>
      <?php endif; ?>
  </div>
</footer>
<!-- Wordpress footer -->
<?php wp_footer(); ?>
<!-- main JS -->
<script type="text/javascript">
  var templateDir = '<?php echo get_theme_url(); ?>';
</script>
<script src="<?php echo get_theme_url('/assets/js/script.js'); ?>"></script>
<?php if(is_home() && !empty(get_map_coord('lat'))) { ?>
  <script>
    var map_coords = {
      lat: <?php echo get_map_coord('lat'); ?>,
      lng: <?php echo get_map_coord('lng'); ?>
    };
  </script>
  <script src="<?php echo get_theme_url('/assets/js/contact.js'); ?>"></script>
<?php } ?>
</body>
</html>