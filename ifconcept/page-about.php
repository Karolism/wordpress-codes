<?php
/* Template Name: Page: About us */

get_header();
?>
<?php if (have_posts()): ?>
    <?php
    while (have_posts()):
        the_post();
        ?>
        <section class="page-header" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>)">
            <h1> <?php echo get_the_title() ?></h1>
        </section>
        <section class="group">
            <div class="white">
                <div class="content">
                    <div class="container">
                        <h2 class="abouth2"><?php echo get_field('antraste'); ?></h2>
                        <div class="two-text">
                            <?php echo the_content(); ?>                    
                        </div>
                        <div class="video-block">
                            <?php
                            $video = get_field('video', false, false);
                            $v_img = get_youtube_img_url($video);
                            ?>
                            <div class="v-img" style="background-image: url(<?php echo $v_img; ?>)">
                                <div class="player">
                                    <div class="player-items">
                                        <img src="<?php echo get_theme_url('/assets/images/play.png'); ?>" alt="play">
                                        <div><?php echo trans('watch_video'); ?></div>
                                    </div>

                                </div>
                            </div>  

                            <?php echo get_field('video'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="blue">
                <div class="container">
                    <div class="text">
                        <?php echo get_field('tekstinis_blokas');
                        ?>
                    </div>
                </div>
            </div>
        </section>
        <section class="colide">
            <div class="container">
                <div class="proccess">
                    <h2><?php echo get_field('block_title'); ?></h2>
                    <?php
                    $proccess = get_field('procesai');
                    $text_block = get_field('text_block');
                    ?>
                    <div class="row">
                        <?php foreach ($proccess as $key => $proc): ?>
                            <div class="col-md-2 col-sm-6 p-item tab <?php echo ($key == 0 ? 'active' : ''); ?>" data-href="tab-<?php echo $key; ?>">
                                <div class="icon-wrap">
                                    <img src="<?php echo $proc['ikona']; ?>" alt="">
                                </div>
                                <div class="tab-title">
                                    <?php echo $proc['pavadinimas']; ?>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="tab-content">
                    <div class="active">
                        <div class="row">
                            <div class="col-md-6 fir">
                                <?php echo $text_block['tekstas']; ?>
                            </div>
                            <div class="col-md-6 px-0 text-center img-area">
                                <img src="<?php echo $text_block['paveikslas']; ?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php
        $quality = get_field('imones');
        $companies = $quality['imones'];
        ?>
        <section class="grey">
            <div class="container">
                <div class="text">
                    <?php echo $quality['tekstas'] ?>
                </div>
                <div id="companies" class="owl-carousel">
                    <?php foreach ($companies as $company): ?>
                        <div class="item">
                            <a href="<?php if (!empty($company['adresas'])) {
                echo $company['adresas'];
            } else {
                echo '#companies';
            } ?>">
                                <img src="<?php echo $company['logo']; ?>" alt="">
                            </a>
                        </div>
        <?php endforeach; ?>
                </div>
            </div>
        </section>

    <?php endwhile; ?>
<?php endif; ?>
<?php get_footer(); ?>