<?php get_header();
?>
<?php if (have_posts()): ?>
    <?php
    while (have_posts()):
        the_post();
        ?>
        <section class="page-header" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>)">
            <h1> <?php echo get_the_title() ?></h1>
        </section>
        <div class="content">
            <div class="container">
                <div class="text">
                    <?php echo the_content(); ?> 
                </div>
            </div>
        </div>
        <?php
    endwhile;
endif;
?>
<?php
get_footer();

