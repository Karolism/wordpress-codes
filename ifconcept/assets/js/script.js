$(document).ready(function () {

    var home_slider = $('#home_slider');
    var select_service = $('select');
    var open_form = $(".fixed-form .button-wrap");
    var close_form = $(".fixed-form .close-form");
    var tab = $('.tab');
    var comp = $('#companies');


    home_slider.on('initialized.owl.carousel', function (property) {
        var current = property.item.index;
        var vid = $(property.target).find(".owl-item").eq(current).find('.video > iframe');
        var other = $(property.target).find(".owl-item").find('.video > iframe');
        console.log(other);
        console.log(vid);
        $.each(other, function () {
            var vid_url = $(this).prop('src').replace("&autoplay=1&amp;showinfo=0&amp;controls=0&amp;modestbranding=0&amp;rel=0", " ");
            $(this).prop('src', '');
            $(this).prop('src', vid_url);
        });
        if (vid) {
            vid.prop('src', vid.prop('src') + '&autoplay=1&amp;showinfo=0&amp;controls=0&amp;modestbranding=0&amp;rel=0');
        }

    });

    home_slider.owlCarousel({
        items: 1,
        dots: false
    });


    home_slider.on('changed.owl.carousel', function (property) {
        var current = property.item.index;
        var vid = $(property.target).find(".owl-item").eq(current).find('.video > iframe');
        var other = $(property.target).find(".owl-item").find('.video > iframe');
        console.log(other);
        console.log(vid);
        $.each(other, function () {
            var vid_url = $(this).prop('src').replace("&autoplay=1&amp;showinfo=0&amp;controls=0&amp;modestbranding=0&amp;rel=0", " ");
            $(this).prop('src', '');
            $(this).prop('src', vid_url);
        });
        if (vid) {
            vid.prop('src', vid.prop('src') + '&autoplay=1&amp;showinfo=0&amp;controls=0&amp;modestbranding=0&amp;rel=0');
        }

    });
//    home_slider.on('change.owl.carousel', function (property) {
//        var current = property.item.index;
//        var vid = $(property.target).find(".owl-item").eq(current).find('.video > iframe');
//        if (vid) {
//            vid_url = vid[0].src.replace("&autoplay=1", "");
//            vid.prop('src', '');
//            vid.prop('src', vid_url);
//        }
//    });

    select_service.niceSelect();

    open_form.on('click', function () {
        $(this).parent().css("right", "0");
        $(this).parent().find('.close-form').css("top", "200px");
    });

    close_form.on('click', function () {
        $(this).parent().css("right", "-360px");
        $(this).css("top", "120px");
    });
    //pagrindinio daugiau mygtuko scroll
    $('.more').on('click', function () {
        $('html, body').animate({
            scrollTop: $("#more").offset().top
        }, 900);
    });
    //visi tabai
    tab.on('click', function (e) {
        e.preventDefault();

        var clicked = $(this).data('href');

        $(this).siblings().removeClass('active');
        $(this).addClass('active');
        $('.tab-content').find('#' + clicked).siblings().removeClass('active').hide('slow');
        $('.tab-content').find('#' + clicked).addClass('active').show('slow');
    });

    if ($('#google-map').length > 0) {



        var pos = {

            lat: $('#google-map').data('lt'),

            lng: $('#google-map').data('ln')

        };

        var center = {

            lat: pos.lat,

            lng: pos.lng - 0.007

        }

        var map = new google.maps.Map(document.getElementById('google-map'), {

            zoom: 16,

            center: center,

            scrollwheel: false,

            mapTypeControl: false

        });



        // InfoWindow content

        var image = $('#google-map').data('img');

        var beachMarker = new google.maps.Marker({

            position: pos,

            map: map,

            icon: image,

            title: 'Mus rasite'

        });

        map.set('styles', [

            {

                featureType: 'Greyscale',

                stylers: [

                    {

                        saturation: -90

                    },

                    {

                        gamma: 1.5

                    }]

            }]);

    }
$(window).scroll(function() {
   var hT = $('.v-img').offset().top,
       hH = $('.v-img').outerHeight(),
       wH = $(window).height(),
       wS = $(this).scrollTop();
   if (wS > (hT+hH-wH)){
       if($('.v-img').is(':visible'))
            $('.v-img').click();
   }
});
    // apie mus video on click
    var vclicked = false;
    $('.v-img').on('click', function (e) {
        var iframe = $(this).parent().find('iframe');
        if (vclicked == false) {
            $(this).animate({
                opacity: 0,
            }, 600, function () {
// Animation complete.
            });
            iframe[0].src += "&autoplay=1&amp;showinfo=0&amp;controls=0&amp;modestbranding=0&amp;rel=0";
            vclicked = true;
        } else {
            iframe[0].click();
        }
        e.preventDefault();
    });

    //companies init apie mus page
    comp.owlCarousel({
        items: 7,
        dots: false,
        nav: false,
        loop: true,
        autoplay: true,
        margin: 70,
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 2
            },
            992: {
                items: 4
            },
            1200: {
                items: 7
            }
        }
    });

    $(window).on('load', function (e) {
        $('.services .tab-content').css("min-height", $('#list-column').outerHeight(true) + "px");
    });
});
$(window).scroll(function () {
    var scrollPosition = $(window).scrollTop();
    if (scrollPosition == 0) {
        $('.header').css("background-color", "rgba(0,0,0,.0)");
        $('.header .logo img').css("top", "35px");
        $('.header .logo img').css("transform", "scale(1)");
        $('.header .logo').removeClass("scrolled");
    } else {
        $('.header').css("background-color", "rgba(0,0,0,.6)");
        $('.header .logo img').css("top", "-10px");
        $('.header .logo img').css("transform", "scale(0.6)");
        $('.header .logo').addClass("scrolled");
    }
    if ($(window).width() >= 768 && $(window).width() <= 992 && scrollPosition !== 0) {
        $('.header .logo img').css("top", "-19px");
    }

});


// functions
function create_cookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    } else {
        var expires = "";
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

function read_cookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ')
            c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0)
            return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function erase_cookie(name) {
    createCookie(name, "", -1);
}
//check feuture
function check_feature() {
//historyAPI, placeholder (from IE10)
//webGL (from IE11)
    if (!feature.historyAPI || !feature.placeholder) {
        $('.check_feature-container').fadeIn();
    }
}
//document
//check feature
check_feature();

$('.check_feature-close').on('click', function (e) {
    e.preventDefault();

    create_cookie('check_feature', 1, 7);
    $('.check_feature-container').fadeOut();
})
// moblie menu
$('.header-menu-hamburger').on('click', function () {
    $(this).toggleClass('open');
    $('body').toggleClass('no-scroll');
    $('.header-menu').toggleClass('open');
});
//usecookie
var check_usecookie = function () {
    var exsisting_cookie = read_cookie('usecookie');

    if (exsisting_cookie === '1') {
        return true;
    }
    return false;
};

$('a.set-usecookie').on('click', function (e) {
    e.preventDefault();

    create_cookie('usecookie', 1, 365);

    if (check_usecookie()) {
        $('.usecookie-container').fadeOut('slow');
    }
});