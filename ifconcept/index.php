<?php get_header(); ?>
<section class="header-slider">
    <div id="home_slider" class="owl-carousel">
        <div class="item" style="background-image: url('<?php echo get_theme_url('/assets/images/h1.png'); ?>')">
            <div class="item-wrapper">
                some radnom bshit text<br>
                <strong>twest</strong>
            </div>
        </div>
        <div class="item" style="background-image: url('<?php echo get_theme_url('/assets/images/h1.png'); ?>')">
            <div class="item-wrapper">
                asd
            </div>
        </div>
    </div>
    <span class="more">
        <?php echo trans("learn_more") ?>
    </span>
</section>
<?php
$args = array(
    'posts_per_page' => 3,
    'post_type' => 'services',
    'post_status' => 'publish'
);
$services_array = get_posts($args);
?>
<section id="more" class="group">
    <?php if (!empty($services_array)): ?>
        <div class="white">
            <h2><?php echo trans('services_tab'); ?></h2>

            <div id="home-services" class="container">
                <div class="row">
                    <?php
                    foreach ($services_array as $service):

                        $services = get_field('paslaugu_sarasas', $service->ID);
                        ?>
                        <div class="col-md-4">
                            <div class="service-item">
                                <h3><?php echo $service->post_title; ?></h3>
                                <ul>
                                    <?php
                                    if (!empty($services)) {
                                        foreach ($services as $item) {
                                            echo '<li>' . $item['pavadinimas'] . '</li>';
                                        }
                                    }
                                    ?>
                                </ul>
                                <a href="<?php echo $service->guid; ?>" class="button"><?php echo trans('details_label'); ?></a>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div class="blue">
        <h2><?php echo trans('meet_us'); ?></h2>
    </div>
</section>
<?php
get_footer();
