<?php
/* Template Name: Page: Contacts */

get_header();
?>
<?php if (have_posts()): ?>
    <?php
    while (have_posts()):
        the_post();
        ?>
        <section class="page-header" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>)">
            <h1> <?php echo get_the_title() ?></h1>
        </section>
        <div class="content">
            <div class="container">
                <div class="text">
                    <?php echo the_content(); ?> 
                </div>
                <div class="row">
                    <?php
                    $team = get_field('personalas');
                    foreach ($team as $member):
                        $img = get_theme_url('/assets/images/nophoto.png');
                        if ($member['nuotrauka']) {
                            $img = $member['nuotrauka']["sizes"]["thumbnail"];
                        }
                        ?>
                        <div class="col-md-3 col-sm-6 text-center member">
                            <img src="<?php echo $img ?>" alt="<?php echo $member['vardas']; ?>">
                            <div class="duty"><?php echo $member['pareigos']; ?></div>
                            <div class="name"><?php echo $member['vardas']; ?></div>
                            <div class="phone"><?php echo $member['telefonas']; ?></div>
                            <div class="email"><?php echo $member['el_pastas']; ?></div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <section class="contacts">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 px-0 form">
                        <h2><?php echo trans('form_label'); ?></h2>
                       <?php get_part('contact-form'); ?>
                    </div>
                    <div class="col-md-6 px-0 info">
                        <h2><?php echo trans('contacts_label'); ?></h2>
                        <div class="text">
                            <?php echo get_field('tekstas'); ?>
                        </div>
                        <?php
                        $socials = get_field('socials', 'option');
                        if (!empty($socials)):
                            ?>
                            <div class="socials">
                                <div class="label">
                                    <strong><?php echo trans('follow_us'); ?></strong>
                                </div>
                                <?php
                                if (!empty($socials['facebook'])) {
                                    echo '<a class="fa fab fa-facebook-f" href="' . $socials['facebook'] . '" target="_blank"></a>';
                                }
                                if (!empty($socials['linkedin'])) {
                                    echo '<a class="fa fa-linkedin" href="' . $socials['linkedin'] . '" target="_blank"></a>';
                                }
                                ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </section>
<?php
$location = get_field('map');
?>
        <section>
            <div id="google-map" class="google-map-style2" data-ln="<?php echo $location['lng'] ?>" data-lt="<?php echo $location['lat'] ?>" data-img="<?php echo get_theme_url('/assets/images/marker.png') ?>">
            </div>  
        </section>

    <?php endwhile; ?>
<?php endif; ?>
<?php get_footer(); ?>